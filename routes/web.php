<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\SambutanController;
use App\Http\Controllers\SliderController;
use App\Http\Controllers\CalonSiswaController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\EkstrakurikulerController;
use App\Http\Controllers\LogoController;
use App\Http\Controllers\PrestasiController;
use App\Http\Controllers\SaranaPrasaranaController;
use App\Http\Controllers\SejarahController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\TujuanSloganController;
use App\Http\Controllers\VisiMisiController;
use App\Http\Controllers\MediaSosialController;
use App\Http\Controllers\Api\Lokasi;
use App\Http\Controllers\BudayaController;
use App\Http\Controllers\JurusanController;
use App\Http\Controllers\KetenagaanController;
use App\Http\Controllers\LokasiKontakController;
use App\Http\Controllers\PertanyaanController;
use App\Http\Controllers\StrukturKurikulumController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('home');
Route::get('home', function () {
    return view('index');
})->name('home');
Route::get('/profile', function () {
    return view('profile');
})->name('profile');
Route::get('/akademik', function () {
    return view('akademik');
})->name('akademik');
Route::get('/ketenagaan', function () {
    return view('ketenagaan');
})->name('ketenagaan');
Route::get('/ketenagaan/{ketenagaan}', function () {
    return view('ketenagaan-detail');
})->name('ketenagaan.detail');
Route::get('/lokasi', function () {
    return view('lokasi');
})->name('lokasi');
Route::get('/faq', function () {
    return view('faq');
})->name('faq');
Route::get('/siswa/diterima', function () {
    return view('siswa-diterima');
})->name('siswa.diterima');

Route::get('/chat', [LokasiKontakController::class, 'chat'])->name('chat');
Route::post('/chat', [LokasiKontakController::class, 'chat_whatsapp'])->name('chat.post');
Route::get('/blog', [BlogController::class, 'blog'])->name('blog');
Route::get('/blog/{blog}', [BlogController::class, 'blog_detail'])->name('blog-detail');
Route::get('/ketenagaan/{ketenagaan}', [KetenagaanController::class, 'ketenagaan_detail'])->name('ketenagaan-detail');
Route::get('/ekstrakurikuler/{ekstrakurikuler}', [EkstrakurikulerController::class, 'ekstrakurikuler_detail'])->name('ekstrakurikuler-detail');
Route::get('/prestasi/{prestasi}', [PrestasiController::class, 'prestasi_detail'])->name('prestasi-detail');
Route::get('/saranaprasarana/{saranaprasarana}', [SaranaPrasaranaController::class, 'saranaprasarana_detail'])->name('saranaprasarana-detail');
Route::get('/jurusan/{jurusan}', [JurusanController::class, 'jurusan_detail'])->name('jurusan-detail');
Route::prefix('daftar')->group(function () {
    Route::get('hal-satu', [CalonSiswaController::class, 'createStepOne'])->name('daftar.hal.satu');
    Route::post('hal-satu', [CalonSiswaController::class, 'postCreateStepOne'])->name('daftar.hal.satu.post');
    Route::get('hal-dua', [CalonSiswaController::class, 'createStepTwo'])->name('daftar.hal.dua');
    Route::post('hal-dua', [CalonSiswaController::class, 'postCreateStepTwo'])->name('daftar.hal.dua.post');
    Route::get('hal-tiga', [CalonSiswaController::class, 'createStepThree'])->name('daftar.hal.tiga');
    Route::post('hal-tiga', [CalonSiswaController::class, 'postCreateStepThree'])->name('daftar.hal.tiga.post');
    Route::get('hal-empat', [CalonSiswaController::class, 'createStepFour'])->name('daftar.hal.empat');
    Route::post('hal-empat', [CalonSiswaController::class, 'postCreateStepFour'])->name('daftar.hal.empat.post');
    Route::get('hal-lima', [CalonSiswaController::class, 'createStepFive'])->name('daftar.hal.lima');
    Route::post('hal-lima', [CalonSiswaController::class, 'postCreateStepFive'])->name('daftar.hal.lima.post');
    Route::get('hal-enam', [CalonSiswaController::class, 'createStepSix'])->name('daftar.hal.enam');
    Route::post('hal-enam', [CalonSiswaController::class, 'postCreateStepSix'])->name('daftar.hal.enam.post');
});

Route::prefix('api')->group(function () {
    Route::post('provinsis', [Lokasi::class, 'getProvinsis'])->name('api.provinsis');
    Route::post('provinsi', [Lokasi::class, 'getProvinsi'])->name('api.provinsi');
    Route::post('domisili', [Lokasi::class, 'getDomisili'])->name('api.domisili');
});

Auth::routes();

Route::prefix('dashboard')->middleware('auth')->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('logo', LogoController::class);
    Route::resource('slider', SliderController::class);
    Route::resource('sambutan', SambutanController::class);
    Route::resource('sejarah', SejarahController::class);
    Route::resource('ekstrakurikuler', EkstrakurikulerController::class);
    Route::resource('prestasi', PrestasiController::class);
    Route::resource('ketenagaan', KetenagaanController::class);
    Route::resource('budaya', BudayaController::class);
    Route::resource('tujuan-slogan', TujuanSloganController::class);
    Route::resource('visi-misi', VisiMisiController::class);
    Route::get('calonsiswa/print/{calonsiswa}', [CalonSiswaController::class, 'print'])->name('print');
    Route::get('calonsiswa/print', [CalonSiswaController::class, 'print_calonsiswa'])->name('print.calonsiswa');
    Route::get('pendaftar-ditolak', [CalonSiswaController::class, 'indexDitolak'])->name('pendaftar.ditolak');
    Route::resource('calonsiswa', CalonSiswaController::class);
    Route::resource('siswa', SiswaController::class);
    Route::resource('sarana-prasarana', SaranaPrasaranaController::class);
    Route::resource('blog', BlogController::class);
    Route::resource('mediasosial', MediaSosialController::class);
    Route::resource('lokasikontak', LokasiKontakController::class);
    Route::resource('jurusan', JurusanController::class);
    Route::resource('pertanyaan', PertanyaanController::class);
    Route::resource('strukturkurikulum', StrukturKurikulumController::class);
});

Route::get('install', function () {
    // shell_exec('composer install');
    Artisan::call('migrate:fresh');
    Artisan::call('db:seed');
    Artisan::call('key:generate');
});
