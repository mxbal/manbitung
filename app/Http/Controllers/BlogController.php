<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::get();

        return view('blog.index', compact('blogs'));
    }

    public function create()
    {
        $blog = new Blog();
        $action = route('blog.store');
        $method = 'POST';

        return view('blog.form', compact('blog', 'action', 'method'));
    }

    public function store(Request $request)
    {
        $attr = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'required|mimes:jpg,png,jpeg,gif'
        ]);

        try {
            DB::beginTransaction();

            $image = $request->file('gambar');
            $attr['gambar'] = $image->storeAs('images/blog', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());

            Blog::create($attr);

            DB::commit();

            return redirect()->route('blog.index')->with('success', 'Blog berhasil dibuat');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function show(Blog $blog)
    {
        return view('blog.show', compact('blog'));
    }

    public function edit(Blog $blog)
    {
        $action = route('blog.update', $blog->id);
        $method = 'PATCH';

        return view('blog.form', compact('blog', 'action', 'method'));
    }

    public function update(Request $request, Blog $blog)
    {
        $attr = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'mimes:jpg,png,jpeg,gif'
        ]);

        try {
            DB::beginTransaction();

            if ($request->file('gambar')) {
                Storage::delete($blog->gambar);
                $image = $request->file('gambar');
                $attr['gambar'] = $image->storeAs('images/blog', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());
            } else {
                $attr['gambar'] = $blog->gambar;
            }

            $blog->update($attr);

            DB::commit();

            return redirect()->route('blog.index')->with('success', 'Blog berhasil diubah');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function destroy(Blog $blog)
    {
        try {
            DB::beginTransaction();

            Storage::delete($blog->gambar);
            $blog->delete();

            DB::commit();

            return redirect()->route('blog.index')->with('success', 'Blog berhasil dihapus');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function blog()
    {
        return view('blog');
    }

    public function blog_detail(Blog $blog)
    {
        return view('blog-detail', compact('blog'));
    }
}
