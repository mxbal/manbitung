<?php

namespace App\Http\Controllers;

use App\Models\Prestasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PrestasiController extends Controller
{
    public function index()
    {
        $prestasis = Prestasi::get();

        return view('prestasi.index', compact('prestasis'));
    }

    public function create()
    {
        $prestasi = new Prestasi();
        $action = route('prestasi.store');
        $method = 'POST';

        return view('prestasi.form', compact('prestasi', 'action', 'method'));
    }

    public function store(Request $request)
    {
        $attr = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'required|mimes:jpg,png,jpeg,gif'
        ]);

        try {
            DB::beginTransaction();

            $image = $request->file('gambar');
            $attr['gambar'] = $image->storeAs('images/prestasi', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());

            Prestasi::create($attr);

            DB::commit();

            return redirect()->route('prestasi.index')->with('success', 'Prestasi berhasil dihapus');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function show(Prestasi $prestasi)
    {
        return view('prestasi.show', compact('prestasi'));
    }

    public function edit(Prestasi $prestasi)
    {
        $action = route('prestasi.update', $prestasi->id);
        $method = 'PATCH';

        return view('prestasi.form', compact('prestasi', 'action', 'method'));
    }

    public function update(Request $request, Prestasi $prestasi)
    {
        $attr = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'mimes:jpg,png,jpeg,gif'
        ]);

        try {
            DB::beginTransaction();

            if ($request->file('gambar')) {
                Storage::delete($prestasi->gambar);
                $image = $request->file('gambar');
                $attr['gambar'] = $image->storeAs('images/prestasi', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());
            } else {
                $attr['gambar'] = $prestasi->gambar;
            }

            $prestasi->update($attr);

            DB::commit();

            return redirect()->route('prestasi.index')->with('success', 'Prestasi berhasil diubah');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function destroy(Prestasi $prestasi)
    {
        try {
            DB::beginTransaction();

            Storage::delete($prestasi->gambar);
            $prestasi->delete();
            DB::commit();

            return redirect()->route('prestasi.index')->with('success', 'Prestasi berhasil dihapus');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function prestasi_detail(Prestasi $prestasi)
    {
        return view('prestasi-detail', compact('prestasi'));
    }
}
