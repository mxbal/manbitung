<?php

namespace App\Http\Controllers;

use App\Models\Budaya;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BudayaController extends Controller
{
    public function index()
    {
        $budaya = Budaya::find(1);

        return view('budaya.index', compact('budaya'));
    }

    public function update(Request $request, Budaya $budaya)
    {
        $attr = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'mimes:jpg,png,jpeg',
        ]);

        try {
            DB::beginTransaction();
            if ($request->file('gambar')) {
                Storage::delete($budaya->gambar);
                $image = $request->file('gambar');
                $attr['gambar'] = $image->storeAs('images/budaya', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());
            } else {
                $attr['gambar'] = $budaya->gambar;
            }

            $budaya->update($attr);

            DB::commit();

            return back()->with('success', 'Budaya berhasil diubah');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
}
