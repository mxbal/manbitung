<?php

namespace App\Http\Controllers;

use App\Models\Logo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class LogoController extends Controller
{
    public function index()
    {
        $logo = Logo::find(1);

        return view('logo.index', compact('logo'));
    }

    public function update(Request $request, Logo $logo)
    {
        $attr = $request->validate([
            'label' => 'required',
            'gambar' => 'mimes:jpg,png,jpeg',
            'deskripsi' => 'required',
        ]);

        try {
            DB::beginTransaction();
            if ($request->file('gambar')) {
                Storage::delete($logo->gambar);
                $image = $request->file('gambar');
                $attr['gambar'] = $image->storeAs('images/logo', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());
            } else {
                $attr['gambar'] = $logo->gambar;
            }

            $logo->update($attr);

            DB::commit();

            return back()->with('success', 'Logo berhasil diubah');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
}
