<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Lokasi extends Controller
{
    public function getDomisili(Request $request)
    {    
        $curl = curl_init();
        $id_provinsi = explode('-',$request->provinsi)[1];
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.rajaongkir.com/starter/city?id=" . $request->id_district . "province=". $id_provinsi,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key: fe0d4585b425a9d02f8dff4e402e0215"
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $array_response = json_decode($response, false);
        $data_district = (object)$array_response->rajaongkir->results;
        return response()->json($data_district);
    }

    public function getProvinsi(Request $request)
    {    
        $curl = curl_init();
        $id_provinsi = explode('-',$request->provinsi);
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.rajaongkir.com/starter/city?province=". $id_provinsi[1],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "key: fe0d4585b425a9d02f8dff4e402e0215"
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $array_response = json_decode($response, false);
        $data_district = (object)$array_response->rajaongkir->results;
        foreach($data_district as $key)
        {
            echo "<option value='$key->type $key->city_name' kota='$key->city_name' tipe='$key->type' kode-pos='$key->postal_code' >$key->type $key->city_name</option>";
        }
    }

    public function getProvinsis()
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
                "key: fe0d4585b425a9d02f8dff4e402e0215"
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $array_response = json_decode($response, false);
        return (object)$array_response->rajaongkir->results;
    }
}
