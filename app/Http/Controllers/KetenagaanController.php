<?php

namespace App\Http\Controllers;

use App\Models\Ketenagaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class KetenagaanController extends Controller
{
    public function index()
    {
        $ketenagaans = Ketenagaan::get();

        return view('ketenagaan.index', compact('ketenagaans'));
    }

    public function create()
    {
        $ketenagaan = new Ketenagaan();
        $action = route('ketenagaan.store');
        $method = 'POST';

        return view('ketenagaan.form', compact('ketenagaan', 'action', 'method'));
    }

    public function store(Request $request)
    {
        $attr = $request->validate([
            'nama' => 'required',
            'kategori' => 'required',
            'gambar' => 'required|mimes:jpg,png,jpeg,gif'
        ]);
        $attr['nip'] = $request->nip;
        $attr['nuptk'] = $request->nuptk;
        $attr['status'] = $request->status;
        $attr['pendidikan'] = $request->pendidikan;
        $attr['jabatan'] = $request->jabatan;
        $attr['jenis_kelamin'] = $request->jenis_kelamin;
        $attr['agama'] = $request->agama;
        $attr['tempat_lahir'] = $request->tempat_lahir;
        $attr['tanggal_lahir'] = $request->tanggal_lahir;
        $attr['kode_pos'] = $request->kode_pos;
        $attr['telepon'] = $request->telepon;
        $attr['instansi'] = $request->instansi;
        $attr['keterangan'] = $request->keterangan;
        $attr['alamat'] = $request->alamat;

        try {
            DB::beginTransaction();

            $image = $request->file('gambar');
            $attr['gambar'] = $image->storeAs('images/ketenagaan', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());

            Ketenagaan::create($attr);

            DB::commit();

            return redirect()->route('ketenagaan.index')->with('success', 'Ketenagaan has been created');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function show(Ketenagaan $ketenagaan)
    {
        return view('ketenagaan.show', compact('ketenagaan'));
    }

    public function edit(Ketenagaan $ketenagaan)
    {
        $action = route('ketenagaan.update', $ketenagaan->id);
        $method = 'PATCH';

        return view('ketenagaan.form', compact('ketenagaan', 'action', 'method'));
    }

    public function update(Request $request, Ketenagaan $ketenagaan)
    {
        $attr = $request->validate([
            'nama' => 'required',
            'kategori' => 'required',
            'gambar' => 'required|mimes:jpg,png,jpeg,gif'
        ]);


        $attr['nip'] = $request->nip;
        $attr['nuptk'] = $request->nuptk;
        $attr['status'] = $request->status;
        $attr['pendidikan'] = $request->pendidikan;
        $attr['jabatan'] = $request->jabatan;
        $attr['jenis_kelamin'] = $request->jenis_kelamin;
        $attr['agama'] = $request->agama;
        $attr['tempat_lahir'] = $request->tempat_lahir;
        $attr['tanggal_lahir'] = $request->tanggal_lahir;
        $attr['kode_pos'] = $request->kode_pos;
        $attr['telepon'] = $request->telepon;
        $attr['instansi'] = $request->instansi;
        $attr['keterangan'] = $request->keterangan;
        $attr['alamat'] = $request->alamat;

        try {
            DB::beginTransaction();

            if ($request->file('gambar')) {
                Storage::delete($ketenagaan->gambar);
                $image = $request->file('gambar');
                $attr['gambar'] = $image->storeAs('images/ketenagaan', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());
            } else {
                $attr['gambar'] = $ketenagaan->gambar;
            }

            $ketenagaan->update($attr);

            DB::commit();

            return redirect()->route('ketenagaan.index')->with('success', 'Ketenagaan has been updated');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function destroy(Ketenagaan $ketenagaan)
    {
        try {
            DB::beginTransaction();

            Storage::delete($ketenagaan->image);
            $ketenagaan->delete();

            DB::commit();

            return redirect()->route('ketenagaan.index')->with('success', 'Ketenagaan has been deleted');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function ketenagaan_detail(Ketenagaan $ketenagaan)
    {
        return view('ketenagaan-detail', compact('ketenagaan'));
    }

}
