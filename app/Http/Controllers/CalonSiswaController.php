<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\CalonSiswa;
use App\Http\Controllers\Api\Lokasi;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Storage;

class CalonSiswaController extends Controller
{
 
    public function createStepOne(Request $request)
    {
        $siswa = $request->session()->get('siswa');
        $years = range(2000, 2099);
        return view('pendaftaran.create-step-one',compact('siswa', 'years'));
    }
  
 
    public function postCreateStepOne(Request $request)
    {
        $validatedData = $request->validate([
            'jalur_ppdb' => 'required',
            'gelombang_pendaftaran' => 'required',
            'tanggal_daftar' => 'required',
            'nama_panggilan' => 'required',
            'bahasa' => 'required',
            'alasan' => 'required',
            'hobi' => 'required',
            'cita_cita' => 'required',
            'nama' => 'required',
            'nomor_kk' => 'required|numeric',
            'nisn' => 'required|unique:calon_siswas|numeric',
            'no_telepon' => 'required',
            'kewarganegaraan' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'penerima_pkh' => 'required',
            'asal_sekolah' => 'required',
        ]);
        $validatedData['email'] = $request->email;
        $validatedData['tahun_lulus'] = $request->tahun_lulus;
        $validatedData['tanggal_daftar'] = $request->tanggal_daftar;
        $validatedData['status_siswa'] = 'Pendaftar';
        $validatedData['jurusan'] = implode("-", [$request->jurusan1, $request->jurusan2, $request->jurusan3]);
        if(empty($request->session()->get('siswa'))){
            $siswa = new CalonSiswa();
            $siswa->fill($validatedData);
            $request->session()->put('siswa', $siswa);
        }else{
            $siswa = $request->session()->get('siswa');
            $siswa->fill($validatedData);
            $request->session()->put('siswa', $siswa);
        }
        $request->session()->put('form-1', true);

        return redirect()->route('daftar.hal.dua');
    }
  
 
    public function createStepTwo(Request $request)
    {
        if (!$request->session()->get('form-1')) {
            return redirect()->route('daftar.hal.satu');
        }   
        $siswa = $request->session()->get('siswa');
  
        return view('pendaftaran.create-step-two',compact('siswa'));
    }
  
 
    public function postCreateStepTwo(Request $request)
    {
        $validatedData = $request->validate([
            'jumlah_saudara' => 'required',
            'jumlah_saudara_kandung' => 'required',
            'jumlah_saudara_tiri' => 'required',
            'jumlah_saudara_angkat' => 'required',
            'anak_ke' => 'required',
            'status_anak' => 'required',
        ]);
       
        if(empty($request->session()->get('siswa'))){
            $siswa = new CalonSiswa();
            $siswa->fill($validatedData);
            $request->session()->put('siswa', $siswa);
        }else{
            $siswa = $request->session()->get('siswa');
            $siswa->fill($validatedData);
            $request->session()->put('siswa', $siswa);
        }
        $request->session()->put('form-2', true);
        $request->session()->forget('form-1');


        return redirect()->route('daftar.hal.tiga');
    }

    public function createStepThree(Request $request)
    {
        if (!$request->session()->get('form-2')) {
            return redirect()->route('daftar.hal.satu');
        }  
        $siswa = $request->session()->get('siswa');
  
        return view('pendaftaran.create-step-three',compact('siswa'));
    }
  
 
    public function postCreateStepThree(Request $request)
    {
        $validatedData = $request->validate([
            'tinggi_badan' => 'required|numeric',
            'berat_badan' => 'required|numeric',
            'golongan_darah' => 'required',
            'cacat_badan' => 'required',
            'penyakit_bawaan' => 'required',
            'pernah_sakit' => 'required',
            'nama_penyakit' => 'required',
            'tanggal_sakit' => 'required',
            'lama_sakit' => 'required',
        ]);
       
        if(empty($request->session()->get('siswa'))){
            $siswa = new CalonSiswa();
            $siswa->fill($validatedData);
            $request->session()->put('siswa', $siswa);
        }else{
            $siswa = $request->session()->get('siswa');
            $siswa->fill($validatedData);
            $request->session()->put('siswa', $siswa);
        }
        $request->session()->put('form-3', true);
        $request->session()->forget('form-2');

        return redirect()->route('daftar.hal.empat');
    }
    
    public function createStepFour(Request $request)
    {
        if (!$request->session()->get('form-3')) {
            return redirect()->route('daftar.hal.satu');
        }  
        $siswa = $request->session()->get('siswa');
        $lokasi = new Lokasi();
        $provinsis = $lokasi->getProvinsis();
        return view('pendaftaran.create-step-four',compact('siswa', 'provinsis'));
    }
 
    public function postCreateStepFour(Request $request)
    {
        $validatedData = $request->validate([
            'alamat' => 'required',
            'rw' => 'required|numeric',
            'rt' => 'required|numeric',
            'kelurahan' => 'required',
            'kecamatan' => 'required',
            'domisili' => 'required',
            'kode_pos' => 'required|numeric',
            'jarak_kesekolah' => 'required',
            'transportasi_kesekolah' => 'required',
            'alamat_sekolah_asal' => 'required',
        ]);
        $validatedData['provinsi'] = explode('-',$request->provinsi)[0];
       
        if(empty($request->session()->get('siswa'))){
            $siswa = new CalonSiswa();
            $siswa->fill($validatedData);
            $request->session()->put('siswa', $siswa);
        }else{
            $siswa = $request->session()->get('siswa');
            $siswa->fill($validatedData);
            $request->session()->put('siswa', $siswa);
        }

        $request->session()->put('form-4', true);
        $request->session()->forget('form-3');

        return redirect()->route('daftar.hal.lima');
    }

    public function createStepFive(Request $request)
    {
        if (!$request->session()->get('form-4')) {
            return redirect()->route('daftar.hal.satu');
        }  
        $siswa = $request->session()->get('siswa');
  
        return view('pendaftaran.create-step-five',compact('siswa'));
    }
 
    public function postCreateStepFive(Request $request)
    {
        $validatedData = $request->validate([
            'nama_ayah' => 'required',
            'nik_ayah' => 'required|numeric',
            'alamat_ayah' => 'required',
            'tempat_lahir_ayah' => 'required',
            'tanggal_lahir_ayah' => 'required',
            'agama_ayah' => 'required',
            'pendidikan_ayah' => 'required',
            'pekerjaan_ayah' => 'required',
            'penghasilan_ayah' => 'required|numeric',
            'telp_ayah' => 'required',
            'nama_ibu' => 'required',
            'nik_ibu' => 'required|numeric',
            'alamat_ibu' => 'required',
            'tempat_lahir_ibu' => 'required',
            'tanggal_lahir_ibu' => 'required',
            'agama_ibu' => 'required',
            'pendidikan_ibu' => 'required',
            'pekerjaan_ibu' => 'required',
            'penghasilan_ibu' => 'required|numeric',
            'telp_ibu' => 'required',
        ]);
        $validatedData['nama_wali'] = $request->nama_wali;
        $validatedData['nik_wali'] = $request->nik_wali;
        $validatedData['nama_wali'] = $request->nama_wali;
        $validatedData['alamat_wali'] = $request->alamat_wali;
        $validatedData['tempat_lahir_wali'] = $request->tempat_lahir_wali;
        $validatedData['tanggal_lahir_wali'] = $request->tanggal_lahir_wali;
        $validatedData['agama_wali'] = $request->agama_wali;
        $validatedData['pendidikan_wali'] = $request->pendidikan_wali;
        $validatedData['pekerjaan_wali'] = $request->pekerjaan_wali;
        $validatedData['penghasilan_wali'] = $request->penghasilan_wali;
        $validatedData['telp_wali'] = $request->telp_wali;
       
        if(empty($request->session()->get('siswa'))){
            $siswa = new CalonSiswa();
            $siswa->fill($validatedData);
            $request->session()->put('siswa', $siswa);
        }else{
            $siswa = $request->session()->get('siswa');
            $siswa->fill($validatedData);
            $request->session()->put('siswa', $siswa);
        }
        $request->session()->forget('form-4');
        $request->session()->put('form-5', true);

        return redirect()->route('daftar.hal.enam');
    }
  

    public function createStepSix(Request $request)
    {
        if (!$request->session()->get('form-5')) {
            return redirect()->route('daftar.hal.satu'
            );
        }  
        $siswa = $request->session()->get('siswa');
  
        return view('pendaftaran.create-step-six',compact('siswa'));
    }
 
    public function postCreateStepSix(Request $request)
    {
        $siswa = $request->validate([
            'image' => 'required',
        ]);
        $siswa = $request->session()->get('siswa');

        $image = $request->file('image');
        $siswa['image'] = $image->storeAs('images/calonsiswa', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());
        $siswa->save();
  
        $request->session()->forget('siswa');
  
        return redirect()->route('daftar.hal.satu');
    }

    public function index()
    {
        $calonsiswas = CalonSiswa::where('status_siswa', '=', 'Pendaftar')->get();

        return view('calonsiswa.index', compact('calonsiswas'));
    }

    public function indexDitolak()
    {
        $calonsiswas = CalonSiswa::where('status_siswa', '=', 'ditolak')->get();

        return view('calonsiswa.index-ditolak', compact('calonsiswas'));
    }

    public function create()
    {
        $calonsiswa = new CalonSiswa();
        $action = route('calonsiswa.store');
        $method = 'POST';
        $years = range(2000, 2099);
        return view('calonsiswa.form', compact('calonsiswa', 'years', 'action', 'method'));
    }

    public function store(Request $request)
    {
        if ($request->oldNisn !== null) {
            $nisnValidate = 'required|numeric';
        } else {
            $nisnValidate = 'required|numeric|unique:calon_siswas';
        }

        $attr = $request->validate([
            'jalur_ppdb' => 'required',
            'nama' => 'required',
            'nomor_kk' => 'required|numeric',
            'nisn' => $nisnValidate,
            'no_telepon' => 'required',
            'kewarganegaraan' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'penerima_pkh' => 'required',
            'asal_sekolah' => 'required',
            'penerima_pkh' => 'required',
            'status_anak' => 'required',
            'anak_ke' => 'required',
            'jumlah_saudara' => 'required',
            'tinggi_badan' => 'required',
            'asal_sekolah' => 'required',
            'tahun_lulus' => 'required',
            'golongan_darah' => 'required',
            'berat_badan' => 'required',
            'alamat' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'kecamatan' => 'required',
            'kelurahan' => 'required',
            'provinsi' => 'required',
            'kode_pos' => 'required',
            'domisili' => 'required',
            'jarak_kesekolah' => 'required',
            'transportasi_kesekolah' => 'required',
            'kode_pos' => 'required',
            'kode_pos' => 'required',
            'alamat_sekolah_asal' => 'required',
            'image' => 'required',
            'gelombang_pendaftaran' => 'required',
            'nama_panggilan' => 'required',
            'bahasa' => 'required',
            'alasan' => 'required',
            'hobi' => 'required',
            'cita_cita' => 'required',
            'jumlah_saudara_kandung' => 'required',
            'jumlah_saudara_tiri' => 'required',
            'jumlah_saudara_angkat' => 'required',
            'cacat_badan' => 'required',
            'penyakit_bawaan' => 'required',
            'pernah_sakit' => 'required',
            'nama_penyakit' => 'required',
            'tanggal_sakit' => 'required',
            'lama_sakit' => 'required',
        ]);
        $attr['email'] = $request->email;
        $attr['tahun_lulus'] = (int)$request->tahun_lulus;
        $attr['tanggal_daftar'] = now();
        $attr['status_siswa'] = 'Pendaftar';
        $attr['jurusan'] = implode("-", [$request->jurusan1, $request->jurusan2, $request->jurusan3]);
        $attr['nama_ayah'] = $request->nama_ayah;
        $attr['nik_ayah'] = $request->nik_ayah;
        $attr['alamat_ayah'] = $request->alamat_ayah;
        $attr['tanggal_lahir_ayah'] = $request->tanggal_lahir_ayah;
        $attr['tempat_lahir_ayah'] = $request->tempat_lahir_ayah;
        $attr['agama_ayah'] = $request->agama_ayah;
        $attr['pendidikan_ayah'] = $request->pendidikan_ayah;
        $attr['pekerjaan_ayah'] = $request->pekerjaan_ayah;
        $attr['penghasilan_ayah'] = $request->penghasilan_ayah;
        $attr['telp_ayah'] = $request->telp_ayah;
        $attr['nama_ibu'] = $request->nama_ibu;
        $attr['nik_ibu'] = $request->nik_ibu;
        $attr['alamat_ibu'] = $request->alamat_ibu;
        $attr['tanggal_lahir_ibu'] = $request->tanggal_lahir_ibu;
        $attr['tempat_lahir_ibu'] = $request->tempat_lahir_ibu;
        $attr['agama_ibu'] = $request->agama_ibu;
        $attr['pendidikan_ibu'] = $request->pendidikan_ibu;
        $attr['pekerjaan_ibu'] = $request->pekerjaan_ibu;
        $attr['penghasilan_ibu'] = $request->penghasilan_ibu;
        $attr['telp_ibu'] = $request->telp_ibu;
        $attr['nama_wali'] = $request->nama_wali;
        $attr['nik_wali'] = $request->nik_wali;
        $attr['alamat_wali'] = $request->alamat_wali;
        $attr['tanggal_lahir_wali'] = $request->tanggal_lahir_wali;
        $attr['tempat_lahir_wali'] = $request->tempat_lahir_wali;
        $attr['agama_wali'] = $request->agama_wali;
        $attr['pendidikan_wali'] = $request->pendidikan_wali;
        $attr['pekerjaan_wali'] = $request->pekerjaan_wali;
        $attr['penghasilan_wali'] = $request->penghasilan_wali;
        $attr['telp_wali'] = $request->telp_wali;

        try {
            DB::beginTransaction();
            $image = $request->file('image');
            $attr['image'] = $image->storeAs('images/calonsiswa', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());
            CalonSiswa::create($attr);

            DB::commit();

            return redirect()->route('calonsiswa.index')->with('success', 'Calon Siswa has been created');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function show(CalonSiswa $calonsiswa)
    {
        return view('calonsiswa.show', compact('calonsiswa'));
    }

    public function edit(CalonSiswa $calonsiswa)
    {
        $action = route('calonsiswa.update', $calonsiswa->id);
        $method = 'PATCH';
        $years = range(2000, 2099);

        return view('calonsiswa.form', compact('calonsiswa', 'years', 'action', 'method'));
    }

    public function update(Request $request, CalonSiswa $calonsiswa)
    {
        if (!$request->status_siswa) {
            if ($request->oldNisn !== null) {
                $nisnValidate = 'required|numeric';
            } else {
                $nisnValidate = 'required|numeric|unique:calon_siswas';
            }
            $attr = $request->validate([
                'jalur_ppdb' => 'required',
                'nama' => 'required',
                'nomor_kk' => 'required|numeric',
                'nisn' => $nisnValidate,
                'no_telepon' => 'required',
                'kewarganegaraan' => 'required',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required',
                'jenis_kelamin' => 'required',
                'agama' => 'required',
                'penerima_pkh' => 'required',
                'asal_sekolah' => 'required',
                'penerima_pkh' => 'required',
                'status_anak' => 'required',
                'anak_ke' => 'required',
                'jumlah_saudara' => 'required',
                'tinggi_badan' => 'required',
                'asal_sekolah' => 'required',
                'tahun_lulus' => 'required',
                'golongan_darah' => 'required',
                'berat_badan' => 'required',
                'alamat' => 'required',
                'rt' => 'required',
                'rw' => 'required',
                'kecamatan' => 'required',
                'kelurahan' => 'required',
                'provinsi' => 'required',
                'kode_pos' => 'required',
                'domisili' => 'required',
                'jarak_kesekolah' => 'required',
                'transportasi_kesekolah' => 'required',
                'kode_pos' => 'required',
                'kode_pos' => 'required',
                'alamat_sekolah_asal' => 'required',
            ]);
            $attr['email'] = $request->email;
            $attr['tahun_lulus'] = (int)$request->tahun_lulus;
            $attr['tanggal_daftar'] = now();
            $attr['status_siswa'] = 'Pendaftar';
            $attr['jurusan'] = implode("-", [$request->jurusan1, $request->jurusan2, $request->jurusan3]);
            $attr['nama_ayah'] = $request->nama_ayah;
            $attr['nik_ayah'] = $request->nik_ayah;
            $attr['alamat_ayah'] = $request->alamat_ayah;
            $attr['tanggal_lahir_ayah'] = $request->tanggal_lahir_ayah;
            $attr['tempat_lahir_ayah'] = $request->tempat_lahir_ayah;
            $attr['agama_ayah'] = $request->agama_ayah;
            $attr['pendidikan_ayah'] = $request->pendidikan_ayah;
            $attr['pekerjaan_ayah'] = $request->pekerjaan_ayah;
            $attr['penghasilan_ayah'] = $request->penghasilan_ayah;
            $attr['telp_ayah'] = $request->telp_ayah;
            $attr['nama_ibu'] = $request->nama_ibu;
            $attr['nik_ibu'] = $request->nik_ibu;
            $attr['alamat_ibu'] = $request->alamat_ibu;
            $attr['tanggal_lahir_ibu'] = $request->tanggal_lahir_ibu;
            $attr['tempat_lahir_ibu'] = $request->tempat_lahir_ibu;
            $attr['agama_ibu'] = $request->agama_ibu;
            $attr['pendidikan_ibu'] = $request->pendidikan_ibu;
            $attr['pekerjaan_ibu'] = $request->pekerjaan_ibu;
            $attr['penghasilan_ibu'] = $request->penghasilan_ibu;
            $attr['telp_ibu'] = $request->telp_ibu;
            $attr['nama_wali'] = $request->nama_wali;
            $attr['nik_wali'] = $request->nik_wali;
            $attr['alamat_wali'] = $request->alamat_wali;
            $attr['tanggal_lahir_wali'] = $request->tanggal_lahir_wali;
            $attr['tempat_lahir_wali'] = $request->tempat_lahir_wali;
            $attr['agama_wali'] = $request->agama_wali;
            $attr['pendidikan_wali'] = $request->pendidikan_wali;
            $attr['pekerjaan_wali'] = $request->pekerjaan_wali;
            $attr['penghasilan_wali'] = $request->penghasilan_wali;
            $attr['telp_wali'] = $request->telp_wali;
        } else {
            $attr['status_siswa'] = $request->status_siswa;
        }

        try {
            DB::beginTransaction();

            if ($request->file('image')) {
                Storage::delete($calonsiswa->image);
                $image = $request->file('image');
                $attr['image'] = $image->storeAs('images/calonsiswa', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());
            } else {
                $attr['image'] = $calonsiswa->image;
            }

            $calonsiswa->update($attr);

            DB::commit();

            return redirect()->route('calonsiswa.index')->with('success', 'CalonSiswa has been updated');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function destroy(CalonSiswa $calonsiswa)
    {
        try {
            DB::beginTransaction();

            $calonsiswa->delete();

            DB::commit();

            return redirect()->route('calonsiswa.index')->with('success', 'CalonSiswa has been deleted');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function print(CalonSiswa $calonsiswa)
    {
        $date = date('m/d/Y');

        $pdf = PDF::loadView('calonsiswa.siswa-laporan', compact('calonsiswa', 'date'));
        return $pdf->download("laporan-". $calonsiswa->nama .".pdf");


    }

    public function print_calonsiswa()
    {
        $calonsiswas = CalonSiswa::where('status_siswa', '=', 'Pendaftar')->get();
        // return view('calonsiswa.laporan',compact('calonsiswas'));

        $pdf = PDF::loadView('calonsiswa.laporan', compact('calonsiswas'));
        return $pdf->stream('laporan.pdf');
        
        // return $pdf->download("laporan.pdf");
    }

}