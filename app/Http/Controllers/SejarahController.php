<?php

namespace App\Http\Controllers;

use App\Models\Sejarah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SejarahController extends Controller
{
    public function index()
    {
        $sejarah = Sejarah::find(1);

        return view('sejarah.index', compact('sejarah'));
    }

    public function update(Request $request, Sejarah $sejarah)
    {
        $attr = $request->validate([
            'deskripsi' => 'required',
            'gambar' => 'mimes:jpg,png,jpeg',
        ]);
        $attr['judul'] = $request->judul;

        try {
            DB::beginTransaction();
            if ($request->file('gambar')) {
                Storage::delete($sejarah->gambar);
                $image = $request->file('gambar');
                $attr['gambar'] = $image->storeAs('images/sejarah', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());
            } else {
                $attr['gambar'] = $sejarah->image;
            }

            $sejarah->update($attr);

            DB::commit();

            return back()->with('success', 'Sejarah berhasil  dibuat');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
}
