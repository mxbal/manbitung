<?php

namespace App\Http\Controllers;

use App\Models\TujuanSlogan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TujuanSloganController extends Controller
{
    public function index()
    {
        $tujuanslogan = TujuanSlogan::find(1);

        return view('tujuanslogan.index', compact('tujuanslogan'));
    }

    public function update(Request $request, TujuanSlogan $tujuan_slogan)
    {
        $attr = $request->validate([
            'judul_tujuan' => 'required',
            'judul_slogan' => 'required',
            'tujuan' => 'required',
            'slogan' => 'required',
        ]);
        
        try {
            DB::beginTransaction();

            $tujuan_slogan->update($attr);

            DB::commit();

            return back()->with('success', 'Tujuan Slogan berhasil diubah');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
}
