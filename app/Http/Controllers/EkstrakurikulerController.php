<?php

namespace App\Http\Controllers;

use App\Models\Ekstrakurikuler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class EkstrakurikulerController extends Controller
{
    public function index()
    {
        $ekstrakurikulers = Ekstrakurikuler::get();

        return view('ekstrakurikuler.index', compact('ekstrakurikulers'));
    }

    public function create()
    {
        $ekstrakurikuler = new Ekstrakurikuler();
        $action = route('ekstrakurikuler.store');
        $method = 'POST';

        return view('ekstrakurikuler.form', compact('ekstrakurikuler', 'action', 'method'));
    }

    public function store(Request $request)
    {
        $attr = $request->validate([
            'judul' => 'required',
            'gambar' => 'required|mimes:jpg,png,jpeg,gif'
        ]);
        $attr['deskripsi'] = $request->deskripsi;

        try {
            DB::beginTransaction();

            $image = $request->file('gambar');
            $attr['gambar'] = $image->storeAs('images/ekstrakurikuler', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());

            Ekstrakurikuler::create($attr);

            DB::commit();

            return redirect()->route('ekstrakurikuler.index')->with('success', 'Ekstrakurikuler berhasil dibuat');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
  
    public function show(Ekstrakurikuler $ekstrakurikuler)
    {
        return view('ekstrakurikuler.show', compact('ekstrakurikuler'));
    }

    public function edit(Ekstrakurikuler $ekstrakurikuler)
    {
        $action = route('ekstrakurikuler.update', $ekstrakurikuler->id);
        $method = 'PATCH';

        return view('ekstrakurikuler.form', compact('ekstrakurikuler', 'action', 'method'));
    }

    public function update(Request $request, Ekstrakurikuler $ekstrakurikuler)
    {
        $attr = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'mimes:jpg,png,jpeg,gif'
        ]);

        try {
            DB::beginTransaction();

            if ($request->file('gambar')) {
                Storage::delete($ekstrakurikuler->gambar);
                $image = $request->file('gambar');
                $attr['gambar'] = $image->storeAs('images/ekstrakurikuler', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());
            } else {
                $attr['gambar'] = $ekstrakurikuler->gambar;
            }

            $ekstrakurikuler->update($attr);

            DB::commit();

            return redirect()->route('ekstrakurikuler.index')->with('success', 'Ekstrakurikuler berhasil diubah');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function destroy(Ekstrakurikuler $ekstrakurikuler)
    {
        try {
            DB::beginTransaction();

            Storage::delete($ekstrakurikuler->gambar);
            $ekstrakurikuler->delete();

            DB::commit();

            return redirect()->route('ekstrakurikuler.index')->with('success', 'Ekstrakurikuler berhasil dihapus');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
    
    public function ekstrakurikuler_detail(Ekstrakurikuler $ekstrakurikuler)
    {
        return view('ekstrakurikuler-detail', compact('ekstrakurikuler'));
    }
}
