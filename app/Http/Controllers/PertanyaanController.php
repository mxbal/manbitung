<?php

namespace App\Http\Controllers;

use App\Models\Pertanyaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PertanyaanController extends Controller
{
    public function index()
    {
        $pertanyaans = Pertanyaan::get();

        return view('pertanyaan.index', compact('pertanyaans'));
    }

    public function create()
    {
        $pertanyaan = new Pertanyaan();
        $action = route('pertanyaan.store');
        $method = 'POST';

        return view('pertanyaan.form', compact('pertanyaan', 'action', 'method'));
    }

    public function store(Request $request)
    {
        $attr = $request->validate([
            'pertanyaan' => 'required',
            'jawaban' => 'required',
        ]);

        try {
            DB::beginTransaction();

            Pertanyaan::create($attr);

            DB::commit();

            return redirect()->route('pertanyaan.index')->with('success', 'Pertanyaan berhasil dibuat');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function show(Pertanyaan $pertanyaan)
    {
        //
    }

    public function edit(Pertanyaan $pertanyaan)
    {
        $action = route('pertanyaan.update', $pertanyaan->id);
        $method = 'PATCH';

        return view('pertanyaan.form', compact('pertanyaan', 'action', 'method'));
    }

    public function update(Request $request, Pertanyaan $pertanyaan)
    {
        $attr = $request->validate([
            'pertanyaan' => 'required',
            'jawaban' => 'required',
        ]);

        try {
            DB::beginTransaction();

            $pertanyaan->update($attr);

            DB::commit();

            return redirect()->route('pertanyaan.index')->with('success', 'Pertanyaan has been updated');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function destroy(Pertanyaan $pertanyaan)
    {
        try {
            DB::beginTransaction();

            Storage::delete($pertanyaan->image);
            $pertanyaan->delete();

            DB::commit();

            return redirect()->route('pertanyaan.index')->with('success', 'Pertanyaan has been deleted');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
}
