<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class JurusanController extends Controller
{
    public function index()
    {
        $jurusans = Jurusan::get();

        return view('jurusan.index', compact('jurusans'));
    }

    public function create()
    {
        $jurusan = new Jurusan();
        $action = route('jurusan.store');
        $method = 'POST';

        return view('jurusan.form', compact('jurusan', 'action', 'method'));
    }

    public function store(Request $request)
    {
        $attr = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'required|mimes:jpg,png,jpeg,gif'
        ]);
        $attr['gambar'] = $request->gambar;

        try {
            DB::beginTransaction();

            $image = $request->file('gambar');
            $attr['gambar'] = $image->storeAs('images/jurusan', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());

            Jurusan::create($attr);

            DB::commit();

            return redirect()->route('jurusan.index')->with('success', 'Jurusan berhasil dibuat');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }


    public function show(Jurusan $jurusan)
    {
        return view('jurusan.show', compact('jurusan'));
    }

    public function edit(Jurusan $jurusan)
    {
        $action = route('jurusan.update', $jurusan->id);
        $method = 'PATCH';

        return view('jurusan.form', compact('jurusan', 'action', 'method'));
    }

    public function update(Request $request, Jurusan $jurusan)
    {
        $attr = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'mimes:jpg,png,jpeg,gif'
        ]);

        try {
            DB::beginTransaction();

            if ($request->file('gambar')) {
                Storage::delete($jurusan->gambar);
                $image = $request->file('gambar');
                $attr['gambar'] = $image->storeAs('images/jurusan', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());
            } else {
                $attr['gambar'] = $jurusan->gambar;
            }

            $jurusan->update($attr);

            DB::commit();

            return redirect()->route('jurusan.index')->with('success', 'Jurusan berhasil diubah');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function destroy(Jurusan $jurusan)
    {
        try {
            DB::beginTransaction();

            Storage::delete($jurusan->gambar);
            $jurusan->delete();

            DB::commit();

            return redirect()->route('jurusan.index')->with('success', 'Jurusan berhasil dihapus');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function jurusan_detail(Jurusan $jurusan)
    {
        return view('jurusan-detail', compact('jurusan'));
    }
}
