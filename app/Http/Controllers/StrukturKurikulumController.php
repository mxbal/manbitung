<?php

namespace App\Http\Controllers;

use App\Models\StrukturKurikulum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class StrukturKurikulumController extends Controller
{
    public function index()
    {
        $strukturkurikulum = StrukturKurikulum::find(1);

        return view('strukturkurikulum.index', compact('strukturkurikulum'));
    }

    public function update(Request $request, StrukturKurikulum $strukturkurikulum)
    {
        $attr = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'mimes:jpg,png,jpeg',
        ]);

        try {
            DB::beginTransaction();
            if ($request->file('gambar')) {
                Storage::delete($strukturkurikulum->gambar);
                $image = $request->file('gambar');
                $attr['gambar'] = $image->storeAs('images/strukturkurikulum', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());
            } else {
                $attr['gambar'] = $strukturkurikulum->image;
            }

            $strukturkurikulum->update($attr);

            DB::commit();

            return back()->with('success', 'Struktur Kurikulum berhasil diubah');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
}
