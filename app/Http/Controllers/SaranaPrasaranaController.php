<?php

namespace App\Http\Controllers;

use App\Models\SaranaPrasarana;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SaranaPrasaranaController extends Controller
{
    public function index()
    {
        $saranaprasaranas = SaranaPrasarana::get();

        return view('saranaprasarana.index', compact('saranaprasaranas'));
    }

    public function create()
    {
        $sarana_prasarana = new SaranaPrasarana();
        $action = route('sarana-prasarana.store');
        $method = 'POST';

        return view('saranaprasarana.form', compact('sarana_prasarana', 'action', 'method'));
    }

    public function store(Request $request)
    {
        $attr = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'required|mimes:jpg,png,jpeg,gif'
        ]);
        try {
            DB::beginTransaction();

            $image = $request->file('gambar');
            $attr['gambar'] = $image->storeAs('images/saranaprasarana', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());

            SaranaPrasarana::create($attr);

            DB::commit();

            return redirect()->route('sarana-prasarana.index')->with('success', 'Sarana Prasarana berhasil dibuat');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function show(SaranaPrasarana $saranaprasarana)
    {
        return view('saranaprasarana.show', compact('saranaprasarana'));
    }

    public function edit(SaranaPrasarana $sarana_prasarana)
    {
        $action = route('sarana-prasarana.update', $sarana_prasarana->id);
        $method = 'PATCH';

        return view('saranaprasarana.form', compact('sarana_prasarana', 'action', 'method'));
    }

    public function update(Request $request, SaranaPrasarana $sarana_prasarana)
    {
        $attr = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'image' => 'mimes:jpg,png,jpeg,gif'
        ]);

        try {
            DB::beginTransaction();

            if ($request->file('gambar')) {
                Storage::delete($sarana_prasarana->gambar);
                $image = $request->file('gambar');
                $attr['gambar'] = $image->storeAs('images/saranaprasarana', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());
            } else {
                $attr['gambar'] = $sarana_prasarana->gambar;
            }

            $sarana_prasarana->update($attr);

            DB::commit();

            return redirect()->route('sarana-prasarana.index')->with('success', 'Sarana Prasarana berhasil diubah');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function destroy(SaranaPrasarana $sarana_prasarana)
    {
        try {
            DB::beginTransaction();

            Storage::delete($sarana_prasarana->gambar);
            $sarana_prasarana->delete();

            DB::commit();

            return redirect()->route('sarana-prasarana.index')->with('success', 'Sarana Prasarana berhasil dihapus');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function saranaprasarana_detail(SaranaPrasarana $saranaprasarana)
    {
        return view('saranaprasarana-detail', compact('saranaprasarana'));
    }
}
