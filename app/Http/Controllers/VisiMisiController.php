<?php

namespace App\Http\Controllers;

use App\Models\VisiMisi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class VisiMisiController extends Controller
{
    public function index()
    {
        $visimisi = VisiMisi::find(1);

        return view('visimisi.index', compact('visimisi'));
    }

    public function update(Request $request, VisiMisi $visi_misi)
    {
        $attr = $request->validate([
            'judul_visi' => 'required',
            'judul_misi' => 'required',
            'visi' => 'required',
            'misi' => 'required',
        ]);

        try {
            DB::beginTransaction();

            $visi_misi->update($attr);

            DB::commit();

            return back()->with('success', 'Visi Misi berhasil diubah');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
}
