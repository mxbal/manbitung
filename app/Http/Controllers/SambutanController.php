<?php

namespace App\Http\Controllers;

use App\Models\Sambutan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SambutanController extends Controller
{
    public function index()
    {
        $sambutan = Sambutan::find(1);

        return view('sambutan.index', compact('sambutan'));
    }

    public function update(Request $request, Sambutan $sambutan)
    {
        $attr = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'mimes:jpg,png,jpeg',
        ]);

        try {
            DB::beginTransaction();
            if ($request->file('gambar')) {
                Storage::delete($sambutan->gambar);
                $image = $request->file('gambar');
                $attr['gambar'] = $image->storeAs('images/sambutan', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());
            } else {
                $attr['gambar'] = $sambutan->gambar;
            }

            $sambutan->update($attr);

            DB::commit();

            return back()->with('success', 'Sambutan berhasil diubah');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
}
