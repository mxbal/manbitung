<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::get();

        return view('slider.index', compact('sliders'));
    }

    public function create()
    {
        $slider = new Slider();
        $action = route('slider.store');
        $method = 'POST';

        return view('slider.form', compact('slider', 'action', 'method'));
    }

    public function store(Request $request)
    {
        $attr = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'gambar' => 'required|mimes:jpg,png,jpeg,gif'
        ]);

        try {
            DB::beginTransaction();

            $image = $request->file('gambar');
            $attr['gambar'] = $image->storeAs('images/slider', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());

            Slider::create($attr);

            DB::commit();

            return redirect()->route('slider.index')->with('success', 'Slider Berhasil Dibuat');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function show(Slider $slider)
    {
        //
    }

    public function edit(Slider $slider)
    {
        $action = route('slider.update', $slider->id);
        $method = 'PATCH';

        return view('slider.form', compact('slider', 'action', 'method'));
    }

    public function update(Request $request, Slider $slider)
    {
        $attr = $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'image' => 'mimes:jpg,png,jpeg,gif'
        ]);

        try {
            DB::beginTransaction();

            if ($request->file('gambar')) {
                Storage::delete($slider->gambar);
                $image = $request->file('gambar');
                $attr['gambar'] = $image->storeAs('images/slider', date('Ymd') . rand(1000, 9999) . '.' . $image->extension());
            } else {
                $attr['gambar'] = $slider->gambar;
            }

            $slider->update($attr);

            DB::commit();

            return redirect()->route('slider.index')->with('success', 'Slider berhasil diubah');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function destroy(Slider $slider)
    {
        try {
            DB::beginTransaction();

            Storage::delete($slider->gambar);
            $slider->delete();

            DB::commit();

            return redirect()->route('slider.index')->with('success', 'Slider berhasil dihapus');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
}
