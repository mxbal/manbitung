<?php

namespace App\Http\Controllers;

use App\Models\MediaSosial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class MediaSosialController extends Controller
{
    public function index()
    {
        $mediasosials = MediaSosial::get();

        return view('mediasosial.index', compact('mediasosials'));
    }

    public function create()
    {
        $mediasosial = new MediaSosial();
        $action = route('mediasosial.store');
        $method = 'POST';

        return view('mediasosial.form', compact('mediasosial', 'action', 'method'));
    }

    public function store(Request $request)
    {
        $attr = $request->validate([
            'label' => 'required',
            'tautan' => 'required',
            'ikon' => 'required',
        ]);
        $attr['tampil'] = 'Aktif';
        try {
            DB::beginTransaction();

            MediaSosial::create($attr);

            DB::commit();

            return redirect()->route('mediasosial.index')->with('success', 'Media Sosial berhasil dibuat');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function show(MediaSosial $mediasosial)
    {
        //
    }

    public function edit(MediaSosial $mediasosial)
    {
        $action = route('mediasosial.update', $mediasosial->id);
        $method = 'PATCH';

        return view('mediasosial.form', compact('mediasosial', 'action', 'method'));
    }

    public function update(Request $request, MediaSosial $mediasosial)
    {
        if (!$request->status_tampil) {
            $attr = $request->validate([
                'label' => 'required',
                'tautan' => 'required',
                'ikon' => 'required',
            ]);
        } else {
            $attr['tampil'] = $request->status_tampil;
        }
        try {
            DB::beginTransaction();
            $mediasosial->update($attr);

            DB::commit();

            return redirect()->route('mediasosial.index')->with('success', 'Media Sosial berhasil dibuat');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function destroy(MediaSosial $mediasosial)
    {
        try {
            DB::beginTransaction();

            $mediasosial->delete();

            DB::commit();

            return redirect()->route('mediasosial.index')->with('success', 'Media Sosial berhasil dihapus');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }
}
