<?php

namespace App\Http\Controllers;

use App\Models\CalonSiswa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiswaController extends Controller
{
    public function index()
    {
        $siswas = CalonSiswa::where('status_siswa', 'Diterima')->get();
        return view('siswa.index', compact('siswas'));
    }

    public function show(CalonSiswa $siswa)
    {
        return view('siswa.show', compact('siswa'));
    }

}
