<?php

namespace App\Http\Controllers;

use App\Models\LokasiKontak;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LokasiKontakController extends Controller
{
    public function index()
    {
        $lokasikontak = LokasiKontak::find(1);

        return view('lokasikontak.index', compact('lokasikontak'));
    }

    public function update(Request $request, LokasiKontak $lokasikontak)
    {
        $attr = $request->validate([
            'telepon' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'lokasi_map' => 'required',
        ]);

        try {
            DB::beginTransaction();
           
            $lokasikontak->update($attr);

            DB::commit();

            return back()->with('success', 'Lokasi Kontak berhasil dibuat');
        } catch (\Throwable $th) {
            return back()->with('error', $th->getMessage());
        }
    }

    public function chat() {
        return view('chat');
    }

    public function chat_whatsapp(Request $request) {
        $attr = $request->validate([
            'nama' => 'required',
            'pesan' => 'required',
        ]);
        $lokasikontak = LokasiKontak::find(1);
        
        return redirect()->to("https://api.whatsapp.com/send?phone=+62".$lokasikontak['telepon'] ."&text=Hallo, saya ". $attr['nama'] ." mau bertanya ". $attr['pesan'])->send();
    }
}
