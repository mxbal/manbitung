<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TujuanSlogan extends Model
{
    use HasFactory;
    protected $guarded = [];
}
