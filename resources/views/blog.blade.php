@extends('template.main')

@section('content')

<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="#">Beranda</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <span class="current">Berita</span>
  </div>
  <div class="site-section">
      <div class="container">
          <div class="row">
            @foreach (App\Models\Blog::get() as $blog)
              <div class="col-lg-4 col-md-6 mb-4">
                  <div class="course-1-item">
                    <figure class="thumnail">
                      <a href="{{ route('blog-detail', $blog->id) }}" style="max-height:200px; overflow:hidden"><img src="{{ asset('storage/' . $blog->gambar) ?? '' }}" alt="Image" class="img-fluid"></a>
                      <div class="category"><h6 class="text-white">Dibuat Pada : {{ $blog->created_at->diffForHumans() }}</h6></div>  
                      </figure>
                      <div class="course-1-content pb-4">
                      <h2 style="height:20px" >{{ Str::limit(strip_tags($blog->judul), 50) }}</h2>
                      <p><a href="{{ route('blog-detail', $blog->id) }}" class="btn btn-primary text-white rounded-0 px-4">Baca</a></p>
                      </div>
                  </div>
              </div>
            @endforeach
          </div>
      </div>
  </div>
</div>
@endsection