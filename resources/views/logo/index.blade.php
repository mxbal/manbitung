@extends('layouts.master', ['title' => 'Logo', 'first' => 'Logo'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Logo</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <form action="{{ route('logo.update', $logo->id) }}" method="post" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="media">
                <a class="media-left" href="javascript:;">
                    <img src="{{ asset('storage/' . $logo->gambar ?? '') }}" alt="" class="media-object rounded mb-3" id="preview"><br>
                    <input type="file" name="gambar" id="gambar" onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])">

                    @error('gambar')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </a>
                <div class="media-body">
                    <div class="form-group">
                        <label for="label">Label</label>
                        <input type="text" name="label" id="label" class="form-control" value="{{ $logo->label ?? old('label') }}">

                        @error('label')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <input type="text" name="deskripsi" id="deskripsi" class="form-control" value="{{ $logo->deskripsi ?? old('deskripsi') }}">

                        @error('deskripsi')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>


                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop