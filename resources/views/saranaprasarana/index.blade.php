@extends('layouts.master', ['title' => 'Sarana Prasarana', 'first' => 'Sarana Prasarana'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Sarana Prasarana</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <a href="{{ route('sarana-prasarana.create') }}" class="btn btn-primary mb-3">Buat Sarana Prasarana
        </a>
        <table id="data-table-default" class="table table-striped table-bordered table-td-valign-middle">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Gambar</th>
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>
                @foreach($saranaprasaranas as $saranaprasarana)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $saranaprasarana->judul }}</td>
                    <td>
                        <img src="{{ asset('storage/' . $saranaprasarana->gambar) }}" alt="{{ $saranaprasarana->gambar }}" height="100">
                    </td>
                    <td class="d-flex">
                        <a href="{{ route('sarana-prasarana.edit', $saranaprasarana->id) }}" class="btn btn-sm btn-success mr-1"><i class="fas fa-edit"></i></a>
                        <a href="{{ route('sarana-prasarana.show', $saranaprasarana->id) }}" class="btn btn-sm btn-info mr-1"><i class="fas fa-eye"></i></a>
                        <form action="{{ route('sarana-prasarana.destroy', $saranaprasarana->id) }}" method="post">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger mr-1" onclick="return confirm('Apakah anda yakin ingin menghapus data ini?')"><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop