@extends('layouts.master', ['title' => 'Sarana Prasarana', 'first' => 'Sarana Prasarana', 'second' => 'Buat Sarana Prasarana'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Sarana Prasarana</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <form action="{{ $action }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="{{ $method }}">
            @csrf
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" name="judul" id="judul" class="form-control" value="{{ $sarana_prasarana->judul ?? old('judul') }}">

                @error('judul')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="deskripsi">Deskripsi</label>
                <textarea id="editor" name="deskripsi">{{ $sarana_prasarana->deskripsi ?? old('deskripsi') }}</textarea>

                @error('deskripsi')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>


            <div class="form-group">
                <label for="gambar">Gambar</label>
                <input type="file" name="gambar" id="gambar" class="form-control" onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])">
                <br>
                <img id="preview" alt="your gambar" width="100" src="{{ asset('storage/' . $sarana_prasarana->gambar) ?? '' }}" />

                @error('gambar')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Kirim</button>
            </div>
        </form>
    </div>
</div>
@stop