@extends('layouts.master', ['title' => 'Sarana Prasarana', 'first' => 'Detail Sarana Prasarana'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Sarana Prasarana</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <div>
            <div class="text-center">
                <h3>{{ $saranaprasarana->judul }}</h3>
                <img class="mb-4" src="{{ asset('storage/' . $saranaprasarana->gambar) }}" height="200">
            </div>
          <div>
              {!! $saranaprasarana->deskripsi !!}}
          </div>
        </div>
    </div>
</div>
@stop