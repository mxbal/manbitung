<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{ App\Models\Logo::find(1)->nama }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/') }}front/fonts/icomoon/style.css">

    <link rel="stylesheet" href="{{ asset('/') }}front/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}front/css/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('/') }}front/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}front/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}front/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="{{ asset('/') }}front/css/jquery.fancybox.min.css">

    <link rel="stylesheet" href="{{ asset('/') }}front/css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="{{ asset('/') }}front/fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="{{ asset('/') }}front/css/aos.css">
    <link href="{{ asset('/') }}front/css/jquery.mb.YTPlayer.min.css" media="all" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{ asset('/') }}front/css/style.css">



</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

    <div class="site-wrap">
        <div class="site-section">
            <div class="container">
                <h2 class="text-center">Log In</h2>
                <div class="row justify-content-center">
                    <div class="col-md-5">
                        <form action="{{ route('login') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label for="username">Username</label>
                                    <input type="text" id="username" name="username" class="form-control form-control-lg">

                                    @error('username')
                                    <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="pword">Password</label>
                                    <input type="password" id="pword" name="password" class="form-control form-control-lg">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <input type="submit" value="Log In" class="btn btn-primary btn-lg px-5">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>



            </div>
        </div>
    </div>
    <!-- .site-wrap -->

    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#51be78" />
        </svg>
    </div>

    <script src="{{ asset('/') }}front/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('/') }}front/js/jquery-migrate-3.0.1.min.js"></script>
    <script src="{{ asset('/') }}front/js/jquery-ui.js"></script>
    <script src="{{ asset('/') }}front/js/popper.min.js"></script>
    <script src="{{ asset('/') }}front/js/bootstrap.min.js"></script>
    <script src="{{ asset('/') }}front/js/owl.carousel.min.js"></script>
    <script src="{{ asset('/') }}front/js/jquery.stellar.min.js"></script>
    <script src="{{ asset('/') }}front/js/jquery.countdown.min.js"></script>
    <script src="{{ asset('/') }}front/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ asset('/') }}front/js/jquery.easing.1.3.js"></script>
    <script src="{{ asset('/') }}front/js/aos.js"></script>
    <script src="{{ asset('/') }}front/js/jquery.fancybox.min.js"></script>
    <script src="{{ asset('/') }}front/js/jquery.sticky.js"></script>
    <script src="{{ asset('/') }}front/js/jquery.mb.YTPlayer.min.js"></script>




    <script src="{{ asset('/') }}front/js/main.js"></script>

</body>

</html>