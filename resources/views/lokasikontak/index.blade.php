@extends('layouts.master', ['title' => 'Lokasi | Kontak', 'first' => 'Lokasi | Kontak'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Lokasi | Kontak</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <form action="{{ route('lokasikontak.update', $lokasikontak->id) }}" method="post" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="media">
                <div class="media-body">
                    <div class="form-group">
                        <iframe width="100%" height="300" src="https://maps.google.com/maps?q={{  $lokasikontak->lokasi_map ?? '' }}&output=embed" class="media-left"></iframe>
                    </div>
                    <div class="form-group">
                        <label for="telepon">Nomor Telepon</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text" id="basic-addon1">+62</span>
                            </div>
                            <input type="text" class="form-control" value="{{ $lokasikontak->telepon ?? old('telepon') }}" name="telepon" id="telepon" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                        @error('telepon')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Alamat Email</label>
                        <input type="text" name="email" id="email" class="form-control" value="{{ $lokasikontak->email ?? old('email') }}">

                        @error('email')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="lokasi_map">Lokasi Map</label>
                        <input type="text" name="lokasi_map" id="lokasi_map" class="form-control" value="{{ $lokasikontak->lokasi_map ?? old('lokasi_map') }}">

                        @error('lokasi_map')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea id="editor" name="alamat">{{ $lokasikontak->alamat ?? old('alamat') }}</textarea>

                        @error('alamat')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop