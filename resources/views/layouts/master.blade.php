<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>{{ $title }} | {{ App\Models\Logo::find(1)->label }}</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />

    <meta name="nama" content="MAN 1 Bitung">
    <meta name="nama" content="Madrasah Aliyah 1 Bitung">
    <meta name="nama" content="MAN Bitung">
    <meta name="nama" content="MAN Satu Bitung">
    <meta name="nama" content="Madrasah aliyah negeri Bitung">
    <meta name="nama" content="Madrasah aliyah Bitung">
    <meta name="nama" content="Madrasah Bitung">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="{{ asset('/') }}css/default/app.min.css" rel="stylesheet" />
    <link rel="icon" href="{{ asset('storage/' . App\Models\Logo::find(1)->gambar ?? '') }}" type = "image/x-icon">
    <link href="{{ asset('/') }}plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <link href="{{ asset('/') }}plugins/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css" integrity="sha512-O03ntXoVqaGUTAeAmvQ2YSzkCvclZEcPQu1eqloPaHfJ5RuNGiS4l+3duaidD801P50J28EHyonCV06CUlTSag==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://kit.fontawesome.com/yourcode.js" crossorigin="anonymous"></script>

     {{-- Tiny Editor --}}
     <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>    
    <script>
         tinymce.init({
           selector: 'textarea#editor', 
        });
    </script>

    @stack('style')
</head>

<body>
    <div id="page-loader" class="fade show">
        <span class="spinner"></span>
    </div>

    <div id="page-container" class="page-container fade page-sidebar-fixed page-header-fixed">
        <div id="header" class="header navbar-default">
            <div class="navbar-header">
                <a href="/dashboard" class="navbar-brand"><span class="navbar-logo"></span> <b>Man 1 </b> Bitung</a>
                <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <ul class="navbar-nav navbar-right">
                <li class="dropdown navbar-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <div class="image image-icon bg-black text-grey-darker">
                            <i class="fa fa-user"></i>
                        </div>
                        <span class="d-none d-md-inline">{{ auth()->user()->name }}</span> <b class="caret"></b>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        {{-- <a href="javascript:;" class="dropdown-item">Edit Profile</a> --}}
                        {{-- <a href="javascript:;" class="dropdown-item"><span class="badge badge-danger pull-right">0</span> Inbox</a>
                        <a href="javascript:;" class="dropdown-item">Calendar</a> --}}
                        {{-- <a href="javascript:;" class="dropdown-item">Setting</a> --}}
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Log Out
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </a>
                    </div>
                </li>
            </ul>
        </div>

        <div id="sidebar" class="sidebar">
            <div data-scrollbar="true" data-height="100%">
                <ul class="nav">
                    <li class="nav-profile">
                        <a href="javascript:;" data-toggle="nav-profile">
                            <div class="cover with-shadow"></div>
                            <div class="image image-icon bg-black text-grey-darker">
                                <i class="fa fa-user"></i>
                            </div>
                            <div class="info">
                                <b class="caret pull-right"></b>{{ auth()->user()->name }}
                                <small>{{ auth()->user()->level }}</small>
                            </div>
                        </a>
                    </li>
                    <li>
                        {{-- <ul class="nav nav-profile"> --}}
                            {{-- <li><a href="javascript:;"><i class="fa fa-cog"></i> Settings</a></li> --}}
                            {{-- <li><a href="javascript:;"><i class="fa fa-pencil-alt"></i> Send Feedback</a></li> --}}
                            {{-- <li><a href="javascript:;"><i class="fa fa-question-circle"></i> Helps</a></li> --}}
                        {{-- </ul> --}}
                    </li>
                </ul>
                <ul class="nav">
                    <li class="nav-header">Menu</li>
                    <li class="{{ request()->is('dashboard') ? 'active' : '' }}">
                        <a href="/dashboard">
                            <i class="fa fa-th-large"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="has-sub">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fa fa-cog"></i>
                            <span>Tampilan</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ request()->is('dashboard/logo*') ? 'active' : '' }}">
                                <a href="{{ route('logo.index') }}">
                                    <i class="fas fa-circle"></i>
                                    <span>Logo</span>
                                </a>
                            </li>
                            <li class="{{ request()->is('dashboard/slider*') ? 'active' : '' }}">
                                <a href="{{ route('slider.index') }}">
                                    <i class="fa fa-sliders-h"></i>
                                    <span>Slider</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fas fa-folder-open"></i>
                            <span>Data</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ request()->is('dashboard/ketenagaan*') ? 'active' : '' }}">
                                <a href="{{ route('ketenagaan.index') }}">
                                    <i class="fas fa-user-alt"></i>
                                    <span>Ketenagaan</span>
                                </a>
                            </li>
                            <li class="{{ request()->is('dashboard/calonsiswa*') ? 'active' : '' }}">
                                <a href="{{ route('calonsiswa.index') }}">
                                    <i class="fas fa-user-alt"></i>
                                    <span>Calon Siswa</span>
                                </a>
                            </li>
                            <li class="{{ request()->is('dashboard/siswa*') ? 'active' : '' }}">
                                <a href="{{ route('siswa.index') }}">
                                    <i class="fas fa-user-alt"></i>
                                    <span>Siswa</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fas fa-bookmark"></i>
                            <span>Informasi</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ request()->is('dashboard/lokasikontak*') ? 'active' : '' }}">
                                <a href="{{ route('lokasikontak.index') }}">
                                    <i class="fa fa-address-book"></i>
                                    <span>Lokasi & Kontak</span>
                                </a>
                            </li>
                            <li class="{{ request()->is('dashboard/mediasosial*') ? 'active' : '' }}">
                                <a href="{{ route('mediasosial.index') }}">
                                    <i class="fas fa-comment-alt"></i>
                                    <span>Media Sosial</span>
                                </a>
                            </li>
                            <li class="{{ request()->is('dashboard/pertanyaan*') ? 'active' : '' }}">
                                <a href="{{ route('pertanyaan.index') }}">
                                    <i class="fa fa-question-circle"></i>
                                    <span>Pertanyaan</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fas fa-address-card"></i>
                            <span>Profile</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ request()->is('dashboard/tujuan-slogan*') ? 'active' : '' }}">
                                <a href="{{ route('tujuan-slogan.index') }}">
                                    <i class="fas fa-star"></i>
                                    <span>Tujuan & Slogan</span>
                                </a>
                            </li>
                            <li class="{{ request()->is('dashboard/sambutan*') ? 'active' : '' }}">
                                <a href="{{ route('sambutan.index') }}">
                                    <i class="fas fa-handshake"></i>
                                    <span>Sambutan</span>
                                </a>
                            </li>
                            <li class="{{ request()->is('dashboard/visi-misi*') ? 'active' : '' }}">
                                <a href="{{ route('visi-misi.index') }}">
                                    <i class="fas fa-globe"></i>
                                    <span>Visi & Misi</span>
                                </a>
                            </li>
                            <li class="{{ request()->is('dashboard/sejarah*') ? 'active' : '' }}">
                                <a href="{{ route('sejarah.index') }}">
                                    <i class="far fa-hourglass"></i>
                                    <span>Sejarah</span>
                                </a>
                            </li>
                            <li class="{{ request()->is('dashboard/budaya*') ? 'active' : '' }}">
                                <a href="{{ route('budaya.index') }}">
                                    <i class="fas fa-user-alt"></i>
                                    <span>Budaya</span>
                                </a>
                            </li>
                            <li class="{{ request()->is('dashboard/prestasi*') ? 'active' : '' }}">
                                <a href="{{ route('prestasi.index') }}">
                                    <i class="fas fa-award"></i>
                                    <span>Prestasi</span>
                                </a>
                            </li>
                            <li class="{{ request()->is('dashboard/ekstrakurikuler*') ? 'active' : '' }}">
                                <a href="{{ route('ekstrakurikuler.index') }}">
                                    <i class="fas fa-basketball-ball"></i>
                                    <span>Ekstrakurikuler</span>
                                </a>
                            </li>
                            <li class="{{ request()->is('dashboard/jurusan*') ? 'active' : '' }}">
                                <a href="{{ route('jurusan.index') }}">
                                    <i class="fas fa-brain"></i>
                                    <span>Jurusan</span>
                                </a>
                            </li>
                            <li class="{{ request()->is('dashboard/sarana-prasarana*') ? 'active' : '' }}">
                                <a href="{{ route('sarana-prasarana.index') }}">
                                    <i class="fas fa-door-closed"></i>
                                    <span>Sarana Prasarana</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="has-sub">
                        <a href="javascript:;">
                            <b class="caret"></b>
                            <i class="fa fa-align-left"></i>
                            <span>Akademik</span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ request()->is('dashboard/strukturkurikulum*') ? 'active' : '' }}">
                                <a href="{{ route('strukturkurikulum.index') }}">
                                    <i class="fas fa-tasks"></i>
                                    <span>Struktur Kurikulum</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="{{ request()->is('dashboard/blog*') ? 'active' : '' }}">
                        <a href="{{ route('blog.index') }}">
                            <i class="fas fa-file-alt"></i>
                            <span>Blog</span>
                        </a>
                    </li>
                    <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
                </ul>
            </div>

        </div>
        <div class="sidebar-bg"></div>

        <div id="content" class="content">
            <ol class="breadcrumb float-xl-right">
                <li class="breadcrumb-item"><a href="javascript:;">{{ $first ?? '' }}</a></li>
                <li class="breadcrumb-item"><a href="javascript:;">{{ $second ?? '' }}</a></li>
                <li class="breadcrumb-item active"><a href="javascript:;">{{ $third ?? '' }}</a></li>
            </ol>

            <h1 class="page-header">{{ $title }}</h1>

            @yield('content')


        </div>

        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>

    </div>
    {{-- Tiny Editor --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.6.0/umd/popper.min.js" integrity="sha512-BmM0/BQlqh02wuK5Gz9yrbe7VyIVwOzD1o40yi1IsTjriX/NGF37NyXHfmFzIlMmoSIBXgqDiG1VNU6kB5dBbA==" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    {{-- tiny --}}

    <script src="{{ asset('/') }}js/app.min.js"></script>
    <script src="{{ asset('/') }}js/theme/default.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js" integrity="sha512-Zq9o+E00xhhR/7vJ49mxFNJ0KQw1E1TMWkPTxrWcnpfEFDEXgUiwJHIKit93EW/XxE31HSI5GEOW06G6BF1AtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script src="{{ asset('/') }}plugins/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('/') }}plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('/') }}plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('/') }}plugins/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <script src="{{ asset('/') }}js/demo/table-manage-default.demo.js"></script>

    <script src="{{ asset('/') }}plugins/sweetalert/dist/sweetalert.min.js"></script>
    @stack('script')



    @if(session('success'))
    <script>
        iziToast.success({
            title: 'Success',
            position: 'topRight',
            message: '{{ session("success") }}',
        });
    </script>
    @endif

    @if(session('error'))
    <script>
        iziToast.error({
            title: 'Error',
            position: 'topRight',
            message: '{{ session("error") }}',
        });
    </script>
    @endif

    
</body>

</html>