@extends('layouts.master', ['title' => 'Visi | Misi', 'first' => 'Visi | Misi'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Visi | Misi</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <form action="{{ route('visi-misi.update', $visimisi->id) }}" method="post" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="media">
                <div class="media-body">
                    <div class="form-group">
                        <label for="judul_visi">Judul Visi</label>
                        <input type="text" name="judul_visi" id="judul_visi" class="form-control" value="{{ $visimisi->judul_visi ?? old('judul_visi') }}">

                        @error('judul_visi')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="visi">Visi</label>
                        <textarea id="editor" name="visi">{{ $visimisi->visi ?? old('visi') }}</textarea>
        
                        @error('visi')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="judul_misi">Judul Misi</label>
                        <input type="text" name="judul_misi" id="judul_misi" class="form-control" value="{{ $visimisi->judul_misi ?? old('judul_misi') }}">

                        @error('judul_misi')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="misi">Misi</label>
                        <textarea id="editor" name="misi">{{ $visimisi->misi ?? old('misi') }}</textarea>
        
                        @error('misi')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop