@extends('layouts.master', ['title' => 'Pertanyaan', 'first' => 'Pertanyaan', 'second' => 'Form Pertanyaan'])])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Pertanyaan</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <form action="{{ $action }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="{{ $method }}">
            @csrf
            <div class="form-group">
                <label for="pertanyaan">Pertanyaan</label>
                <input type="text" name="pertanyaan" id="pertanyaan" class="form-control" value="{{ $pertanyaan->pertanyaan ?? old('pertanyaan') }}">

                @error('pertanyaan')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="jawaban">Jawaban</label>
                <input type="text" name="jawaban" id="jawaban" class="form-control" value="{{ $pertanyaan->jawaban ?? old('jawaban') }}">

                @error('jawaban')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Kirim</button>
            </div>
        </form>
    </div>
</div>
@stop