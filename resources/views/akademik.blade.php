@extends('template.main')

@section('content')

<div class="custom-breadcrumns border-bottom">
    <div class="container">
      <a href="{{ route('home') }}">Beranda</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <span class="current">Akademik</span>
    </div>
  </div>

  <div class="site-section" id="struktur-kurikulum">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="mb-5">
                <h2 class="section-title-underline mb-5">
                    <span>{{ App\Models\StrukturKurikulum::find(1)->judul ?? ''  }}</span>
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 mb-lg-0">
                <div class="feature-1">
                    <div style="max-height:400px; overflow:hidden">
                        <img src="{{ asset('storage/' . App\Models\StrukturKurikulum::find(1)->gambar) ?? '' }}" alt="" class="media-object rounded" width="300">
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-6 mb-lg-0">
                <div class="feature-1">
                    <div class="feature-1-content">
                        {!! App\Models\StrukturKurikulum::find(1)->deskripsi !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</div>
@endsection