@extends('layouts.master', ['title' => 'Calon Siswa', 'first' => 'Calon Siswa', 'second' => 'Detail Calon Siswa'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Calon Siswa</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
    <table>
        <tr>
            <td>Jalur PPDB</td>
            <td>:</td>
            <td>{{ $calonsiswa->jalur_ppdb }}</td>
        </tr>
        <tr>
            <td>Gelombang Pendaftaran</td>
            <td>:</td>
            <td>{{ $calonsiswa->gelombang_pendaftaran }}</td>
        </tr>
        <tr>
            <td>Tanggal Daftar</td>
            <td>:</td>
            <td>{{ $calonsiswa->tanggal_daftar }}</td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{ $calonsiswa->nama }}</td>
        </tr>
        <tr>
            <td>Nama Panggilan</td>
            <td>:</td>
            <td>{{ $calonsiswa->nama_panggilan }}</td>
        </tr>
        <tr>
            <td>Kewarganegaraan</td>
            <td>:</td>
            <td>{{ $calonsiswa->kewarganegaraan }}</td>
        </tr>
        <tr>
            <td>Bahasa</td>
            <td>:</td>
            <td>{{ $calonsiswa->bahasa }}</td>
        </tr>
        <tr>
            <td>Alasan</td>
            <td>:</td>
            <td>{{ $calonsiswa->alasan }}</td>
        </tr>
        <tr>
            <td>Hobi</td>
            <td>:</td>
            <td>{{ $calonsiswa->hobi }}</td>
        </tr>
        <tr>
            <td>Cita - Cita</td>
            <td>:</td>
            <td>{{ $calonsiswa->cita_cita }}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>:</td>
            <td>{{ $calonsiswa->email }}</td>
        </tr>
        <tr>
            <td>Nomor KK</td>
            <td>:</td>
            <td>{{ $calonsiswa->nomor_kk }}</td>
        </tr>
        <tr>
            <td>Jurusan 1</td>
            <td>:</td>
            <td>{{ explode('-',$calonsiswa->jurusan)[0] }}</td>
        </tr>
        <tr>
            <td>Jurusan 2</td>
            <td>:</td>
            <td>{{ explode('-',$calonsiswa->jurusan)[1] }}</td>
        </tr>
        <tr>
            <td>Jurusan 3</td>
            <td>:</td>
            <td>{{ explode('-',$calonsiswa->jurusan)[2] }}</td>
        </tr>
        <tr>
            <td>Tempat Lahir</td>
            <td>:</td>
            <td>{{ $calonsiswa->tempat_lahir }}</td>
        </tr>
        <tr>
            <td>Tanggal Lahir</td>
            <td>:</td>
            <td>{{ $calonsiswa->tanggal_lahir }}</td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>:</td>
            <td>{{ $calonsiswa->jenis_kelamin }}</td>
        </tr>
        <tr>
            <td>Agama</td>
            <td>:</td>
            <td>{{ $calonsiswa->agama }}</td>
        </tr>
        <tr>
            <td>Status Anak</td>
            <td>:</td>
            <td>{{ $calonsiswa->status_anak }}</td>
        </tr>
        <tr>
            <td>Anak Ke</td>
            <td>:</td>
            <td>{{ $calonsiswa->anak_ke }}</td>
        </tr>
        <tr>
            <td>Jumlah Saudara</td>
            <td>:</td>
            <td>{{ $calonsiswa->jumlah_saudara }}</td>
        </tr>
        <tr>
            <td>Jumlah Saudara Kandung</td>
            <td>:</td>
            <td>{{ $calonsiswa->jumlah_saudara_kandung }}</td>
        </tr>
        <tr>
            <td>Jumlah Saudara Tiri</td>
            <td>:</td>
            <td>{{ $calonsiswa->jumlah_saudara_tiri }}</td>
        </tr>
        <tr>
            <td>Jumlah Saudara Angkat</td>
            <td>:</td>
            <td>{{ $calonsiswa->jumlah_saudara_angkat }}</td>
        </tr>
        <tr>
            <td>Tinggi Badan</td>
            <td>:</td>
            <td>{{ $calonsiswa->tinggi_badan }}</td>
        </tr>
        <tr>
            <td>Berat badan</td>
            <td>:</td>
            <td>{{ $calonsiswa->berat_badan }}</td>
        </tr>
        <tr>
            <td>Golongan darah</td>
            <td>:</td>
            <td>{{ $calonsiswa->golongan_darah }}</td>
        </tr>
        <tr>
            <td>Cacat Badan</td>
            <td>:</td>
            <td>{{ $calonsiswa->cacat_badan }}</td>
        </tr>
        <tr>
            <td>Penyakit Bawaaan</td>
            <td>:</td>
            <td>{{ $calonsiswa->penyakit_bawaan }}</td>
        </tr>
        <tr>
            <td>Pernah Sakit</td>
            <td>:</td>
            <td>{{ $calonsiswa->pernah_sakit }}</td>
        </tr>
        <tr>
            <td>Nama Penyakit</td>
            <td>:</td>
            <td>{{ $calonsiswa->nama_penyakit }}</td>
        </tr>
        <tr>
            <td>Tanggal Sakit</td>
            <td>:</td>
            <td>{{ $calonsiswa->tanggal_sakit }}</td>
        </tr>
        <tr>
            <td>Lama Sakit</td>
            <td>:</td>
            <td>{{ $calonsiswa->lama_sakit }}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td>{{ $calonsiswa->alamat }}</td>
        </tr>
        <tr>
            <td>RT</td>
            <td>:</td>
            <td>{{ $calonsiswa->rt }}</td>
        </tr>
        <tr>
            <td>RW</td>
            <td>:</td>
            <td>{{ $calonsiswa->rw }}</td>
        </tr>
        <tr>
            <td>Kelurahan</td>
            <td>:</td>
            <td>{{ $calonsiswa->kelurahan }}</td>
        </tr>
        <tr>
            <td>Kecamatan</td>
            <td>:</td>
            <td>{{ $calonsiswa->kecamatan }}</td>
        </tr>
        <tr>
            <td>Domisili</td>
            <td>:</td>
            <td>{{ $calonsiswa->domisili }}</td>
        </tr>
        <tr>
            <td>Provinsi</td>
            <td>:</td>
            <td>{{ $calonsiswa->provinsi }}</td>
        </tr>
        <tr>
            <td>Kode Pos</td>
            <td>:</td>
            <td>{{ $calonsiswa->kode_pos }}</td>
        </tr>
        <tr>
            <td>Jarak Kesekolah</td>
            <td>:</td>
            <td>{{ $calonsiswa->jarak_kesekolah }}</td>
        </tr>
        <tr>
            <td>Transportasi Kesekolah</td>
            <td>:</td>
            <td>{{ $calonsiswa->transportasi_kesekolah }}</td>
        </tr>
        <tr>
            <td>Handphone</td>
            <td>:</td>
            <td>{{ $calonsiswa->no_telepon }}</td>
        </tr>
        <tr>
            <td>Penerima PKH</td>
            <td>:</td>
            <td>{{ $calonsiswa->penerima_pkh }}</td>
        </tr>
        <tr>
            <td>Asal Sekolah</td>
            <td>:</td>
            <td>{{ $calonsiswa->asal_sekolah }}</td>
        </tr>
        <tr>
            <td>Tahun Lulus</td>
            <td>:</td>
            <td>{{ $calonsiswa->tahun_lulus }}</td>
        </tr>
        <tr>
            <td>NISN</td>
            <td>:</td>
            <td>{{ $calonsiswa->nisn }}</td>
        </tr>
        <tr>
            <td>Nama Ayah</td>
            <td>:</td>
            <td>{{ $calonsiswa->nama_ayah }}</td>
        </tr>
        <tr>
            <td>NIK Ayah</td>
            <td>:</td>
            <td>{{ $calonsiswa->nik_ayah }}</td>
        </tr>
        <tr>
            <td>Alamat Ayah</td>
            <td>:</td>
            <td>{{ $calonsiswa->alamat_ayah }}</td>
        </tr>
        <tr>
            <td>Tempat Lahir Ayah</td>
            <td>:</td>
            <td>{{ $calonsiswa->tempat_lahir_ayah }}</td>
        </tr>
        <tr>
            <td>Tanggal Lahir Ayah</td>
            <td>:</td>
            <td>{{ $calonsiswa->tanggal_lahir_ayah }}</td>
        </tr>
        <tr>
            <td>Agama Ayah</td>
            <td>:</td>
            <td>{{ $calonsiswa->agama_ayah }}</td>
        </tr>
        <tr>
            <td>Pendidikan Ayah</td>
            <td>:</td>
            <td>{{ $calonsiswa->pendidikan_ayah }}</td>
        </tr>
        <tr>
            <td>Pekerjaan Ayah</td>
            <td>:</td>
            <td>{{ $calonsiswa->pekerjaan_ayah }}</td>
        </tr>
        <tr>
            <td>Penghasilan Ayah</td>
            <td>:</td>
            <td>{{ $calonsiswa->penghasilan_ayah }}</td>
        </tr>
        <tr>
            <td>Telepon Ayah</td>
            <td>:</td>
            <td>{{ $calonsiswa->telp_ayah }}</td>
        </tr>
        <tr>
            <td>Nama Ibu</td>
            <td>:</td>
            <td>{{ $calonsiswa->nama_ibu }}</td>
        </tr>
        <tr>
            <td>NIK Ibu</td>
            <td>:</td>
            <td>{{ $calonsiswa->nik_ibu }}</td>
        </tr>
        <tr>
            <td>Alamat Ibu</td>
            <td>:</td>
            <td>{{ $calonsiswa->alamat_ibu }}</td>
        </tr>

        <tr>
            <td>Tempat Lahir Ibu</td>
            <td>:</td>
            <td>{{ $calonsiswa->tempat_lahir_ibu }}</td>
        </tr>
        <tr>
            <td>Tanggal Lahir Ibu</td>
            <td>:</td>
            <td>{{ $calonsiswa->tanggal_lahir_ibu }}</td>
        </tr>
        <tr>
            <td>Agama Ibu</td>
            <td>:</td>
            <td>{{ $calonsiswa->agama_ibu }}</td>
        </tr>
        <tr>
            <td>Pendidikan Ibu</td>
            <td>:</td>
            <td>{{ $calonsiswa->pendidikan_ibu }}</td>
        </tr>
        <tr>
            <td>Pekerjaan Ibu</td>
            <td>:</td>
            <td>{{ $calonsiswa->pekerjaan_ibu }}</td>
        </tr>
        <tr>
            <td>Penghasilan Ibu</td>
            <td>:</td>
            <td>{{ $calonsiswa->penghasilan_ibu }}</td>
        </tr>
        <tr>
            <td>Telepon Ibu</td>
            <td>:</td>
            <td>{{ $calonsiswa->telp_ibu }}</td>
        </tr>
        <tr>
            <td>Nama Wali</td>
            <td>:</td>
            <td>{{ $calonsiswa->nama_wali }}</td>
        </tr>
        <tr>
            <td>NIK Wali</td>
            <td>:</td>
            <td>{{ $calonsiswa->nik_wali }}</td>
        </tr>
        <tr>
            <td>Alamat Wali</td>
            <td>:</td>
            <td>{{ $calonsiswa->alamat_wali }}</td>
        </tr>
        <tr>
            <td>Tempat Lahir Wali</td>
            <td>:</td>
            <td>{{ $calonsiswa->tempat_lahir_wali }}</td>
        </tr>
        <tr>
            <td>Tanggal Lahir Wali</td>
            <td>:</td>
            <td>{{ $calonsiswa->tanggal_lahir_wali }}</td>
        </tr>
        <tr>
            <td>Agama Wali</td>
            <td>:</td>
            <td>{{ $calonsiswa->agama_wali }}</td>
        </tr>
        <tr>
            <td>Pendidikan Wali</td>
            <td>:</td>
            <td>{{ $calonsiswa->pendidikan_wali }}</td>
        </tr>
        <tr>
            <td>Pekerjaan Wali</td>
            <td>:</td>
            <td>{{ $calonsiswa->pekerjaan_wali }}</td>
        </tr>
        <tr>
            <td>Penghasilan Wali</td>
            <td>:</td>
            <td>{{ $calonsiswa->penghasilan_wali }}</td>
        </tr>
        <tr>
            <td>Telepon Wali</td>
            <td>:</td>
            <td>{{ $calonsiswa->telp_wali }}</td>
        </tr>
     </table>
    </div>
</div>
@stop