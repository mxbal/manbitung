@extends('layouts.master', ['title' => 'Calon Siswa', 'first' => 'Calon Siswa', 'second' => 'Buat Calon Siswa'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Calon Siswa</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
        <form action="{{ $action }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="{{ $method }}">
            @csrf

            <div class="form-group">
                <input type="hidden" name="oldNisn" id="oldNisn" class="form-control" value="{{ $calonsiswa->nisn ?? old('nisn') }}">
            </div>

            <div class="form-group">
                <label for="image">Gambar</label>
                <input type="file" name="image" id="image" class="form-control" onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])">
                <br>
                <img id="preview" alt="your image" width="100" src="{{ asset('storage/' . $calonsiswa->image) ?? '' }}" />

                @error('image')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
        
            <div class="form-group">
                <label for="jalur_ppdb">Jalur PPDB</label>
                <select  class="form-control"  name="jalur_ppdb" id="">
                  <option value="" disabled selected>-- Pilih Jalur PPDB --</option>
                  <option value="Afirmasi" @if ($calonsiswa->jalur_ppdb ?? old('jalur_ppdb') =='Afirmasi'){{ 'selected' }}@endif>Afirmasi / Keluarga Ekonomi Tidak Mampu</option>
                  <option value="Afirmasi Disabilitas"  @if ($calonsiswa->jalur_ppdb ?? old('jalur_ppdb') =='Afirmasi Disabilitas') {{ 'selected' }}@endif>Afirmasi Disabilitas</option>
                  <option value="Perpindahan Tugas Orangtua" @if ($calonsiswa->jalur_ppdb ?? old('jalur_ppdb') =='Perpindahan Tugas Orangtua') {{ 'selected' }}@endif>Perpindahan Tugas Orangtua</option>
                  <option value="Prestasi" @if ($calonsiswa->jalur_ppdb ?? old('jalur_ppdb') =='Prestasi') {{ 'selected' }}@endif>Prestasi</option>
                  <option value="Zonasi" @if ($calonsiswa->jalur_ppdb ?? old('jalur_ppdb') =='Zonasi') {{ 'selected' }}@endif>Zonasi</option>
                </select>
                @error('jalur_ppdb')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="gelombang_pendaftaran">Gelombang Pendaftaran</label>
                <select  class="form-control"  name="gelombang_pendaftaran" id="">
                  <option value="" disabled selected>-- Pilih Gelombang Pendaftaran --</option>
                  <option value="1" @if($calonsiswa->gelombang_pendaftaran ?? old('gelombang_pendaftaran') =='1') {{ 'selected' }}@endif>1</option>
                  <option value="2" @if($calonsiswa->gelombang_pendaftaran ?? old('gelombang_pendaftaran') =='2') {{ 'selected' }}@endif>2</option>
                  <option value="3" @if($calonsiswa->gelombang_pendaftaran ?? old('gelombang_pendaftaran') =='3') {{ 'selected' }}@endif>3</option>
                </select>
                @error('gelombang_pendaftaran')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" name="nama" id="nama" class="form-control" value="{{ $calonsiswa->nama ?? old('nama') }}">
                @error('nama')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama_panggilan">Nama Panggilan</label>
                <input type="text" name="nama_panggilan" id="nama_panggilan" class="form-control" value="{{ $calonsiswa->nama_panggilan ?? old('nama_panggilan') }}">
                @error('nama_panggilan')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
         
            <div class="form-group">
                <label for="kewarganegaraan">Kewarganegaraan</label>
                <input type="text" name="kewarganegaraan" id="kewarganegaraan" class="form-control" value="{{ $calonsiswa->kewarganegaraan ?? old('kewarganegaraan') }}">
                @error('kewarganegaraan')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="bahasa">Bahasa</label>
                <input type="text" name="bahasa" id="bahasa" class="form-control" value="{{ $calonsiswa->bahasa ?? old('bahasa') }}">
                @error('bahasa')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
         
            <div class="form-group">
                <label for="alasan">Alasan</label>
                <input type="text" name="alasan" id="alasan" class="form-control" value="{{ $calonsiswa->alasan ?? old('alasan') }}">
                @error('alasan')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="hobi">Hobi</label>
                <input type="text" name="hobi" id="hobi" class="form-control" value="{{ $calonsiswa->hobi ?? old('hobi') }}">
                @error('hobi')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="cita_cita">Cita cita</label>
                <input type="text" name="cita_cita" id="cita_cita" class="form-control" value="{{ $calonsiswa->cita_cita ?? old('cita_cita') }}">
                @error('cita_cita')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="email">Email</label>
                        <input type="text" name="email" id="email" class="form-control" value="{{ $calonsiswa->email ?? old('email') }}">
                        @error('email')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col">
                        <label for="no_telepon">No Telepon</label>
                        <input type="number" name="no_telepon" id="no_telepon" class="form-control" value="{{ $calonsiswa->no_telepon ?? old('no_telepon') }}">
                        @error('no_telepon')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>
               
            <div class="form-group">
                <label for="tanggal_daftar">Tanggal Daftar</label>
                <input type="date" name="tanggal_daftar" id="tanggal_daftar" class="form-control" value="{{ $calonsiswa->tanggal_daftar ?? old('tanggal_daftar') }}">
                @error('tanggal_daftar')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="nomor_kk">Nomor Kartu Keluarga</label>
                <input type="number" name="nomor_kk" id="nomor_kk" class="form-control" value="{{ $calonsiswa->nomor_kk ?? old('nomor_kk') }}">
                @error('nomor_kk')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            
            <div class="form-group">
                <label for="jurusan1">Jurusan Pertama</label>
                <select  class="form-control"  name="jurusan1" id="jurusan1">
                  <option value="" disabled selected>Pilih Jurusan Pertama</option>
                    @foreach(App\Models\Jurusan::get() as $jurusan)
                      <option value="{{ $jurusan->judul }}"  @if(explode('-', $calonsiswa->jurusan)[0] ?? old('jurusan1') == $jurusan->judul) {{ 'selected' }}@endif>{{ $jurusan->judul }}</option>
                    @endforeach
                </select>
                @error('jurusan1')
                <small class="text-danger">{{ $message }}</small>
                @enderror
                <label for="jurusan2">Jurusan Kedua</label>
                <select  class="form-control"  name="jurusan2" id="jurusan2">
                  <option value="" disabled selected>Pilih Jurusan Kedua</option>
                  @foreach(App\Models\Jurusan::get() as $jurusan)
                  <option value="{{ $jurusan->judul }}" @if(explode('-', $calonsiswa->jurusan)[1] ?? old('jurusan2') == $jurusan->judul) {{ 'selected' }}@endif>{{ $jurusan->judul }}</option>
                  @endforeach
                </select>
                @error('jurusan2')
                <small class="text-danger">{{ $message }}</small>
                @enderror
                <label for="jurusan3">Jurusan Ketiga</label>
                <select  class="form-control"  name="jurusan3" id="jurusan3">
                  <option value="" disabled selected>Pilih Jurusan Ketiga</option>
                  @foreach(App\Models\Jurusan::get() as $jurusan)
                  <option value="{{ $jurusan->judul }}" @if(explode('-', $calonsiswa->jurusan)[2] ?? old('jurusan3') == $jurusan->judul) {{ 'selected' }}@endif>{{ $jurusan->judul }}</option>
                  @endforeach
                </select>
                @error('jurusan3')
                <small class="text-danger">{{ $message }}</small>
                @enderror
              </div>
              <div class="form-group">
                <label for="asal_sekolah">Asal Sekolah</label>
                <input type="text" name="asal_sekolah" id="asal_sekolah" class="form-control" value="{{ $calonsiswa->asal_sekolah ?? old('asal_sekolah') }}">

                @error('asal_sekolah')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="tempat_lahir">Tempat Lahir</label>
                <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" value="{{ $calonsiswa->tempat_lahir ?? old('tempat_lahir') }}">
                @error('tempat_lahir')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="tanggal_lahir">Tanggal Lahir</label>
                <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control" value="{{ $calonsiswa->tanggal_lahir ?? old('tanggal_lahir') }}">
                @error('tanggal_lahir')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="description">Jenis Kelamin</label><br/>
                <label class="radio-inline"><input type="radio" name="jenis_kelamin" value="L" @if($calonsiswa->jenis_kelamin ?? old('jenis_kelamin') =='L') {{ 'checked' }}@endif> Laki - Laki</label>
                <label class="radio-inline"><input type="radio" name="jenis_kelamin" value="P" @if($calonsiswa->jenis_kelamin ?? old('jenis_kelamin') =='P') {{ 'checked' }}@endif> Perempuan</label>
                @error('jenis_kelamin')
                <small class="text-danger">{{ $message }}</small>
                @enderror
              </div>
    
            <div class="form-group">
                <label for="penerima_pkh">Penerima PKH</label>
                <select  class="form-control"  name="penerima_pkh" id="">
                  <option value="" disabled selected>-- Penerima PKH --</option>
                  <option value="Ya" @if($calonsiswa->penerima_pkh ?? old('penerima_pkh') == 'Ya') {{ 'selected' }}@endif>Ya</option>
                  <option value="Tidak" @if($calonsiswa->penerima_pkh ?? old('penerima_pkh') == 'Tidak') {{ 'selected' }}@endif>Tidak</option>
                </select>
                @error('penerima_pkh')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
    
            <div class="form-group">
                <label for="nisn">NISN</label>
                <input type="number" name="nisn" id="nisn" class="form-control" value="{{ $calonsiswa->nisn ?? old('nisn') }}">

                @error('nisn')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="agama">Agama</label>
                <input type="text" name="agama" id="agama" class="form-control" value="{{ $calonsiswa->agama ?? old('agama') }}">

                @error('agama')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="status_anak">Status Anak</label>
                <input type="text" name="status_anak" id="status_anak" class="form-control" value="{{ $calonsiswa->status_anak ?? old('status_anak') }}">
                @error('status_anak')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="anak_ke">Anak Ke</label>
                <input type="number" name="anak_ke" id="anak_ke" class="form-control" value="{{ $calonsiswa->anak_ke ?? old('anak_ke') }}">

                @error('anak_ke')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="jumlah_saudara">Jumlah Saudara</label>
                <input type="number" name="jumlah_saudara" id="jumlah_saudara" class="form-control" value="{{ $calonsiswa->jumlah_saudara ?? old('jumlah_saudara') }}">

                @error('jumlah_saudara')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="jumlah_saudara_kandung">Jumlah Saudara kandung</label>
                <input type="number" name="jumlah_saudara_kandung" id="jumlah_saudara_kandung" class="form-control" value="{{ $calonsiswa->jumlah_saudara_kandung ?? old('jumlah_saudara_kandung') }}">

                @error('jumlah_saudara_kandung')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="jumlah_saudara_tiri">Jumlah Saudara Tiri</label>
                <input type="number" name="jumlah_saudara_tiri" id="jumlah_saudara_tiri" class="form-control" value="{{ $calonsiswa->jumlah_saudara_tiri ?? old('jumlah_saudara_tiri') }}">

                @error('jumlah_saudara_tiri')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="jumlah_saudara_angkat">Jumlah Saudara Angkat</label>
                <input type="number" name="jumlah_saudara_angkat" id="jumlah_saudara_angkat" class="form-control" value="{{ $calonsiswa->jumlah_saudara_angkat ?? old('jumlah_saudara_angkat') }}">

                @error('jumlah_saudara_angkat')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="cacat_badan">Cacat Badan</label>
                <input type="text" name="cacat_badan" id="cacat_badan" class="form-control" value="{{ $calonsiswa->cacat_badan ?? old('cacat_badan') }}">

                @error('cacat_badan')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="penyakit_bawaan">Penyakit Bawaan</label>
                <input type="text" name="penyakit_bawaan" id="penyakit_bawaan" class="form-control" value="{{ $calonsiswa->penyakit_bawaan ?? old('penyakit_bawaan') }}">

                @error('penyakit_bawaan')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="pernah_sakit">Pernah Sakit</label>
                <input type="text" name="pernah_sakit" id="pernah_sakit" class="form-control" value="{{ $calonsiswa->pernah_sakit ?? old('pernah_sakit') }}">

                @error('pernah_sakit')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama_penyakit">Nama Penyakit</label>
                <input type="text" name="nama_penyakit" id="nama_penyakit" class="form-control" value="{{ $calonsiswa->nama_penyakit ?? old('nama_penyakit') }}">
                @error('nama_penyakit')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="tanggal_sakit">Tanggal Sakit</label>
                <input type="date" name="tanggal_sakit" id="tanggal_sakit" class="form-control" value="{{ $calonsiswa->tanggal_sakit ?? old('tanggal_sakit') }}">
                @error('tanggal_sakit')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="lama_sakit">Lama Sakit</label>
                <input type="text" name="lama_sakit" id="lama_sakit" class="form-control" value="{{ $calonsiswa->lama_sakit ?? old('lama_sakit') }}">
                @error('lama_sakit')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="tinggi_badan">Tinggi Badan (CM)</label>
                <input type="number" name="tinggi_badan" id="tinggi_badan" class="form-control" value="{{ $calonsiswa->tinggi_badan ?? old('tinggi_badan') }}">

                @error('tinggi_badan')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
          
            <div class="form-group">
                <label for="tahun_lulus">Tahun Lulus</label>
                <select  class="form-control"  name="tahun_lulus" id="">
                  <option value="" disabled selected>-- Tahun Lulus --</option>
                  @foreach ($years as $year)
                  <option value="{{ $year }}" @if($calonsiswa->tahun_lulus ?? old('tahun_lulus') == $year) {{ 'selected' }}@endif>{{ $year }}</option>
                  @endforeach
                </select>
                @error('tahun_lulus')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
              
            <div class="form-group">
                <label for="berat_badan">Berat Badan (KG)</label>
                <input type="number" name="berat_badan" id="berat_badan" class="form-control" value="{{ $calonsiswa->berat_badan ?? old('berat_badan') }}">

                @error('berat_badan')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="golongan_darah">Golongan Darah</label>
                <select  class="form-control"  name="golongan_darah" id="">
                  <option value="" disabled selected>-- Pilih Golongan Darah --</option>
                  <option value="A" @if($calonsiswa->golongan_darah ?? old('golongan_darah') =='A') {{ 'selected' }}@endif>A</option>
                  <option value="B" @if($calonsiswa->golongan_darah ?? old('golongan_darah') =='B') {{ 'selected' }}@endif>B</option>
                  <option value="AB" @if($calonsiswa->golongan_darah ?? old('golongan_darah') =='AB') {{ 'selected' }}@endif>AB</option>
                  <option value="O" @if($calonsiswa->golongan_darah ?? old('golongan_darah')=='O') {{ 'selected' }}@endif>O</option>
                  <option value="lainnya">lainnya</option>
                </select>
                @error('golongan_darah')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea type="text"  class="form-control" id="taskDescription" name="alamat">{{ $calonsiswa->alamat ?? old('alamat') }}</textarea>
                @error('alamat')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="provinsi">Provinsi</label>
                <input type="text" name="provinsi" id="provinsi" class="form-control" value="{{ $calonsiswa->provinsi ?? old('provinsi') }}">
                @error('provinsi')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="kecamatan">Kecamatan</label>
                        <input type="text" value="{{ $calonsiswa->kecamatan ?? old('kecamatan') }}" class="form-control"  name="kecamatan">
                        @error('kecamatan')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col">
                        <label for="kelurahan">Kelurahan</label>
                        <input type="text" value="{{ $calonsiswa->kelurahan ?? old('kelurahan') }}" class="form-control"  name="kelurahan">
                        @error('kelurahan')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="rt">RT</label>
                        <input type="number" value="{{ $calonsiswa->rt ?? old('rt') }}" class="form-control"  name="rt">
                        @error('rt')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col">
                        <label for="rw">RW</label>
                        <input type="number" value="{{ $calonsiswa->rw ?? old('rw') }}" class="form-control"  name="rw">
                        @error('rw')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="kode_pos">Kode Pos</label>
                <input type="text" name="kode_pos" id="kode_pos" class="form-control" value="{{ $calonsiswa->kode_pos ?? old('kode_pos') }}">
                @error('kode_pos')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="domisili">Domisili</label>
                <input type="text" name="domisili" id="domisili" class="form-control" value="{{ $calonsiswa->domisili ?? old('domisili') }}">
                @error('domisili')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="jarak_kesekolah">Jarak Ke-Sekolah (KM)</label>
                <input type="number" name="jarak_kesekolah" id="jarak_kesekolah" class="form-control" value="{{ $calonsiswa->jarak_kesekolah ?? old('jarak_kesekolah') }}">

                @error('jarak_kesekolah')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="transportasi_kesekolah">Transportasi Ke-Sekolah</label>
                <input type="text" name="transportasi_kesekolah" id="transportasi_kesekolah" class="form-control" value="{{ $calonsiswa->transportasi_kesekolah ?? old('transportasi_kesekolah') }}">

                @error('transportasi_kesekolah')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="alamat_sekolah_asal">Alamat Asal Sekolah</label>
                <textarea type="text"  class="form-control" id="taskDescription" name="alamat_sekolah_asal">{{ $calonsiswa->alamat_sekolah_asal ?? old('alamat_sekolah_asal') }}</textarea>
                @error('alamat_sekolah_asal')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            
            <div class="form-group">
                <label for="nik_ayah">NIK Ayah</label>
                <input type="number" name="nik_ayah" id="nik_ayah" class="form-control" value="{{ $calonsiswa->nik_ayah ?? old('nik_ayah') }}">

                @error('nik_ayah')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama_ayah">Nama ayah</label>
                <input type="text" name="nama_ayah" id="nama_ayah" class="form-control" value="{{ $calonsiswa->nama_ayah ?? old('nama_ayah') }}">

                @error('nama_ayah')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
            <label for="alamat_ayah">Alamat ayah</label>
            <textarea type="text"  class="form-control" id="taskDescription" name="alamat_ayah">{{ $calonsiswa->alamat_ayah ?? old('alamat_ayah') }}</textarea>
            @error('alamat_ayah')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
            <div class="form-group">
                <label for="tanggal_lahir_ayah">Tanggal Lahir ayah</label>
                <input type="date" name="tanggal_lahir_ayah" id="tanggal_lahir_ayah" class="form-control" value="{{ $calonsiswa->tanggal_lahir_ayah ?? old('tanggal_lahir_ayah') }}">
                @error('tanggal_lahir_ayah')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="tempat_lahir_ayah">Tempat Lahir ayah</label>
                <input type="text" name="tempat_lahir_ayah" id="tempat_lahir_ayah" class="form-control" value="{{ $calonsiswa->tempat_lahir_ayah ?? old('tempat_lahir_ayah') }}">

                @error('tempat_lahir_ayah')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="agama_ayah">Agama ayah</label>
                <input type="text" name="agama_ayah" id="agama_ayah" class="form-control" value="{{ $calonsiswa->agama_ayah ?? old('agama_ayah') }}">

                @error('agama_ayah')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="pendidikan_ayah">Pendidikan ayah</label>
                <input type="text" name="pendidikan_ayah" id="pendidikan_ayah" class="form-control" value="{{ $calonsiswa->pendidikan_ayah ?? old('pendidikan_ayah') }}">

                @error('pendidikan_ayah')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="pekerjaan_ayah">Pekerjaan ayah</label>
                <input type="text" name="pekerjaan_ayah" id="pekerjaan_ayah" class="form-control" value="{{ $calonsiswa->pekerjaan_ayah ?? old('pekerjaan_ayah') }}">

                @error('pekerjaan_ayah')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="penghasilan_ayah">Penghasilan ayah</label>
                <input type="number" name="penghasilan_ayah" id="penghasilan_ayah" class="form-control" value="{{ $calonsiswa->penghasilan_ayah ?? old('penghasilan_ayah') }}">

                @error('penghasilan_ayah')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="telp_ayah">No HandPhone ayah</label>
                <input type="number" name="telp_ayah" id="telp_ayah" class="form-control" value="{{ $calonsiswa->telp_ayah ?? old('telp_ayah') }}">

                @error('telp_ayah')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="nik_ibu">NIK Ibu</label>
                <input type="number" name="nik_ibu" id="nik_ibu" class="form-control" value="{{ $calonsiswa->nik_ibu ?? old('nik_ibu') }}">

                @error('nik_ibu')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama_ibu">Nama Ibu</label>
                <input type="text" name="nama_ibu" id="nama_ibu" class="form-control" value="{{ $calonsiswa->nama_ibu ?? old('nama_ibu') }}">

                @error('nama_ibu')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
            <label for="alamat_ibu">Alamat Ibu</label>
            <textarea type="text"  class="form-control" id="taskDescription" name="alamat_ibu">{{ $calonsiswa->alamat_ibu ?? old('alamat_ibu') }}</textarea>
            @error('alamat_ibu')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
            <div class="form-group">
                <label for="tanggal_lahir_ibu">Tanggal Lahir Ibu</label>
                <input type="date" name="tanggal_lahir_ibu" id="tanggal_lahir_ibu" class="form-control" value="{{ $calonsiswa->tanggal_lahir_ibu ?? old('tanggal_lahir_ibu') }}">
                @error('tanggal_lahir_ibu')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="tempat_lahir_ibu">Tempat Lahir Ibu</label>
                <input type="text" name="tempat_lahir_ibu" id="tempat_lahir_ibu" class="form-control" value="{{ $calonsiswa->tempat_lahir_ibu ?? old('tempat_lahir_ibu') }}">

                @error('tempat_lahir_ibu')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="agama_ibu">Agama Ibu</label>
                <input type="text" name="agama_ibu" id="agama_ibu" class="form-control" value="{{ $calonsiswa->agama_ibu ?? old('agama_ibu') }}">

                @error('agama_ibu')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="pendidikan_ibu">Pendidikan Ibu</label>
                <input type="text" name="pendidikan_ibu" id="pendidikan_ibu" class="form-control" value="{{ $calonsiswa->pendidikan_ibu ?? old('pendidikan_ibu') }}">

                @error('pendidikan_ibu')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="pekerjaan_ibu">Pekerjaan Ibu</label>
                <input type="text" name="pekerjaan_ibu" id="pekerjaan_ibu" class="form-control" value="{{ $calonsiswa->pekerjaan_ibu ?? old('pekerjaan_ibu') }}">

                @error('pekerjaan_ibu')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="penghasilan_ibu">Penghasilan Ibu</label>
                <input type="number" name="penghasilan_ibu" id="penghasilan_ibu" class="form-control" value="{{ $calonsiswa->penghasilan_ibu ?? old('penghasilan_ibu') }}">

                @error('penghasilan_ibu')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="telp_ibu">No HandPhone Ibu</label>
                <input type="number" name="telp_ibu" id="telp_ibu" class="form-control" value="{{ $calonsiswa->telp_ibu ?? old('telp_ibu') }}">

                @error('telp_ibu')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="nik_wali">NIK Wali</label>
                <input type="number" name="nik_wali" id="nik_wali" class="form-control" value="{{ $calonsiswa->nik_wali ?? old('nik_wali') }}">

                @error('nik_wali')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="nama_wali">Nama Wali</label>
                <input type="text" name="nama_wali" id="nama_wali" class="form-control" value="{{ $calonsiswa->nama_wali ?? old('nama_wali') }}">

                @error('nama_wali')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
            <label for="alamat_wali">Alamat Wali</label>
            <textarea type="text"  class="form-control" id="taskDescription" name="alamat_wali">{{ $calonsiswa->alamat_wali ?? old('alamat_wali') }}</textarea>
            @error('alamat_wali')
            <small class="text-danger">{{ $message }}</small>
            @enderror
        </div>
            <div class="form-group">
                <label for="tanggal_lahir_wali">Tanggal Lahir Wali</label>
                <input type="date" name="tanggal_lahir_wali" id="tanggal_lahir_wali" class="form-control" value="{{ $calonsiswa->tanggal_lahir_wali ?? old('tanggal_lahir_wali') }}">
                @error('tanggal_lahir_wali')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="tempat_lahir_wali">Tempat Lahir Wali</label>
                <input type="text" name="tempat_lahir_wali" id="tempat_lahir_wali" class="form-control" value="{{ $calonsiswa->tempat_lahir_wali ?? old('tempat_lahir_wali') }}">

                @error('tempat_lahir_wali')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="agama_wali">Agama Wali</label>
                <input type="text" name="agama_wali" id="agama_wali" class="form-control" value="{{ $calonsiswa->agama_wali ?? old('agama_wali') }}">

                @error('agama_wali')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="pendidikan_wali">Pendidikan Wali</label>
                <input type="text" name="pendidikan_wali" id="pendidikan_wali" class="form-control" value="{{ $calonsiswa->pendidikan_wali ?? old('pendidikan_wali') }}">

                @error('pendidikan_wali')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="pekerjaan_wali">Pekerjaan Wali</label>
                <input type="text" name="pekerjaan_wali" id="pekerjaan_wali" class="form-control" value="{{ $calonsiswa->pekerjaan_wali ?? old('pekerjaan_wali') }}">

                @error('pekerjaan_wali')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="penghasilan_wali">Penghasilan Wali</label>
                <input type="number" name="penghasilan_wali" id="penghasilan_wali" class="form-control" value="{{ $calonsiswa->penghasilan_wali ?? old('penghasilan_wali') }}">

                @error('penghasilan_wali')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="telp_wali">No HandPhone Wali</label>
                <input type="text" name="telp_wali" id="telp_wali" class="form-control" value="{{ $calonsiswa->telp_wali ?? old('telp_wali') }}">

                @error('telp_wali')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Kirim</button>
            </div>
        </form>
    </div>
</div>
@stop