<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        .cf:before, .cf:after {
            content: " ";
            display: table;
        }

        .cf:after {
            clear: both;
        }
        .cf {
            *zoom: 1;
        }
        /* set */
        .container {
            width: 80%;
            margin: 10px auto;
            font-family: 'Calibri', sans-serif;
        }
        
        .kop {
            display: flex;
            justify-content: space-around;
            margin: auto;
            padding: 20px 0px;
        }

        .logo1 img, .logo2 img{
            max-width:100px;
        }
        hr {
            border-top: 5px solid;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin: 30px auto;
        }
        table, td, th {
            border: 1px solid black;
        }
        td {
            padding: 15px;
        }
        #col1 {
            float: left;
            width: 15%;
            text-align: center;
        }
        #col2 {
            float: left;
            width: 70%;
        }
        #col3 {
            float: right;
            width: 15%;
            text-align: center;
        }
        #clearfix {
            clear: both;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="kop">
            <div id="col1" class="logo1">
                <img src="apple.jpg" alt="">
            </div>
            <div id="col2" class="main">
                <center>
                    <h3>KEMENTRIAN AGAMA REPUBLIK INDONESIA</h3>
                    <h3>MADRASAH ALIYAH NEGERI 1 BITUNG</h3>                        
                    <h3>KECAMATAN MAESA - KOTA BITUNG</h2>
                    <h5>Jl. Resetment Kakenturan II</h5>  
                </center>
            </div>
            <div id="col3" class="logo2">
                <img src="apple.jpg" alt="">
            </div>
            <div class="" id="clearfix"></div>
        </div>
        <hr class="batas">
        <div class="isi">
            <table>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>NISN</th>
                        <th>NAMA</th>
                        <th>JALUR PPDB</th>
                        <th>GELOMBANG PENDAFTARAN</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($calonsiswas as $calonsiswa)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $calonsiswa->nisn }}</td>
                        <td>{{ $calonsiswa->nama }}</td>
                        <td>{{ $calonsiswa->jalur_ppdb }}</td>
                        <td style="text-align: center">{{ $calonsiswa->gelombang_pendaftaran }}</td>
                     </tr>
                     @endforeach
                </tbody>
            </table>
        </div>
    </div>
</body>
</html>


{{-- <!DOCTYPE html> 
<html> 
    <head> 
          
        <!-- CSS property to place div
            side by side -->
        <style> 
            #leftbox {
                float:left; 
                background:Red;
                width:25%;
                height:280px;
            }
            #middlebox{
                float:left; 
                background:Green;
                width:50%;
                height:280px;
            }
            #rightbox{
                float:right;
                background:blue;
                width:25%;
                height:280px;
            }
            h1{
                color:green;
                text-align:center;
            }
        </style> 
    </head> 
      
    <body> 
        <div id = "boxes">
            <h1>GeeksforGeeks</h1>
              
            <div id = "leftbox">
                <h2>Learn:</h2>
                It is a good platform to learn programming.
                It is an educational website. Prepare for
                the Recruitment drive  of product based 
                companies like Microsoft, Amazon, Adobe 
                etc with a free online placement preparation 
                course.
            </div> 
              
            <div id = "middlebox">
                <h2>GeeksforGeeks:</h2>
                The course focuses on various MCQ's & Coding 
                question likely to be asked in the interviews
                & make your upcoming placement season efficient
                and successful.
            </div>
              
            <div id = "rightbox">
                <h2>Contribute:</h2>
                Any geeks can help other geeks by writing
                articles on the GeeksforGeeks, publishing
                articles follow few steps that are Articles
                that need little modification/improvement
                from reviewers are published first.
            </div>
        </div>
    </body> 
</html>          --}}