@extends('layouts.master', ['title' => 'Calon Siswa', 'first' => 'Calon Siswa'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Calon Siswa</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <a href="{{ route('calonsiswa.create') }}" class="btn btn-primary mb-3">Daftarkan Calon Siswa</a>
        <a href="{{ route('pendaftar.ditolak') }}" class="btn btn-danger mb-3">Pendaftar Ditolak</a>
        <a href="{{ route('print.calonsiswa') }}" class="btn btn-info mb-3">Print</a>
        <table id="data-table-default" class="table table-striped table-bordered table-td-valign-middle">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NISN</th>
                    <th>Action</th>
                </tr>
            </thead>

            <tbody>
                @foreach($calonsiswas as $calonsiswa)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $calonsiswa->nama }}</td>
                    <td>{{ $calonsiswa->nisn }}</td>
                    <td class="d-flex">
                        <a href="{{ route('calonsiswa.edit', $calonsiswa->id) }}" class="btn btn-sm btn-success mr-1"><i class="fas fa-edit"></i></a>
                        <a href="{{ route('calonsiswa.show', $calonsiswa->id) }}" class="btn btn-sm btn-info mr-1"><i class="fas fa-eye"></i></a>

                        <a href="{{ route('print', $calonsiswa->id) }}" class="btn btn-sm btn-info mr-1"><i class="fas fa-print"></i></a>

                        <div class="dropdown mr-1">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-exclamation"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <form action="{{ route('calonsiswa.update', $calonsiswa->id) }}" method="post">
                                    @csrf
                                    @method('PATCH')
                                    <input type="hidden" value="Diterima" name="status_siswa">
                                    <button type="submit" class="dropdown-item" value="terima">Terima</button>
                                </form>
                                <form action="{{ route('calonsiswa.update', $calonsiswa->id) }}" method="post">
                                    @csrf
                                    @method('PATCH')
                                    <input type="hidden" value="Ditolak" name="status_siswa">
                                    <button type="submit" class="dropdown-item" value="tolak">Tolak</button>
                                </form>
                            </div>
                        </div>

                        <form action="{{ route('calonsiswa.destroy', $calonsiswa->id) }}" method="post">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger mr-1" onclick="return confirm('Apakah anda yakin ingin menghapus data ini?')"><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop