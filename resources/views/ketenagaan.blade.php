@extends('template.main')

@section('content')

<div class="custom-breadcrumns border-bottom">
    <div class="container">
      <a href="{{ route('home') }}">Beranda</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <span class="current">Ketenagaan</span>
    </div>
  </div>

  <div class="site-section">
    <div class="container">
      <div class="row mb-5 justify-content-center text-center">
        <div class="col-lg-4 mb-5">
          <h2 class="section-title-underline mb-5">
            <span id="tenaga-pendidikan">Tenaga Pendidikan</span>
          </h2>
        </div>
      </div>
      <div class="container">
        <div class="row">
          @foreach (App\Models\Ketenagaan::where('kategori', 'Tenaga Pendidikan')->get() as $tenaga_pendidikan)
          <div class="col-xs-6 col-md-4 mb-5">
            <div class="card">
              <div class="wrapper">
                <img class="card-img-top img-fluid" src="{{ asset('storage/' . $tenaga_pendidikan->gambar) }}" alt="Card image cap">
              </div>
              <div class="card-body">
                <p class="card-text">{{ $tenaga_pendidikan->nama }}</p>
                <p class="card-text">{{ $tenaga_pendidikan->nip ?? '-' }}</p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
  <div class="site-section">
    <div class="container">
      <div class="row mb-5 justify-content-center text-center">
        <div class="col-lg-4 mb-5">
          <h2 class="section-title-underline mb-5">
            <span id="tenaga-kependidikan">Tenaga Kependidikan</span>
          </h2>
        </div>
      </div>
      <div class="container">
        <div class="row">
          @foreach (App\Models\Ketenagaan::where('kategori', 'Tenaga Kependidikan')->get() as $tenaga_kependidikan)
          <div class="col-xs-6 col-md-4 mb-5">
            <div class="card">
              <div class="wrapper">
                <img class="card-img-top img-fluid" src="{{ asset('storage/' . $tenaga_kependidikan->gambar) }}" alt="Card image cap">
              </div>
              <div class="card-body">
                <p class="card-text">{{ $tenaga_kependidikan->nama }}</p>
                <p class="card-text">{{ $tenaga_kependidikan->nip ?? '-' }}</p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>

@endsection