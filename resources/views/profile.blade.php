@extends('template.main')

@section('content')
<head>
    <meta property='og:title' content='{{ App\Models\Logo::find(1)->deskripsi }}' />
    <meta property='og:image' content='{{ asset('storage/' . App\Models\Logo::find(1)->gambar) ?? '' }}' />
    <meta property='og:description' content='{{ asset('storage/' . App\Models\Logo::find(1)->deskripsi) ?? '' }}' />
    <meta property='og:image:width' content='1200' />
    <meta property='og:image:height' content='630' />
    <meta property="og:type" content='websiteswwsyttt' />
</head>

<div class="custom-breadcrumns border-bottom">
    <div class="container">
      <a href="#">Home</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <span class="current">Profile</span>
    </div>
</div>
<div class="site-section" id="sambutan">
    <div class="container">
        <div>
            <h2 class="section-title-underline text-center">
                <span>{{ App\Models\Sambutan::find(1)->judul ?? ''}}</span>
            </h2>
            <img src="{{ asset('storage/' . App\Models\Sambutan::find(1)->gambar) ?? '' }}" alt="" class="media-object rounded" height="300">
            {!! App\Models\Sambutan::find(1)->deskripsi ?? '' !!}
        </div>
    </div>
</div>
<div class="site-section" id="sejarah">
    <div class="container">
        <div>
            <h2 class="section-title-underline text-center">
                <span>{{ App\Models\Sejarah::find(1)->judul ?? ''}}</span>
            </h2>
            <img src="{{ asset('storage/' . App\Models\Sejarah::find(1)->gambar) ?? '' }}" alt="" class="media-object rounded" width="300">
            {!! App\Models\Sejarah::find(1)->deskripsi ?? '' !!}
        </div>
    </div>
</div>

<div class="site-section" id="visi-misi-tujuan">
    <div class="container">
        <div>
            <h2 class="section-title-underline text-center">
                <span>{{ App\Models\TujuanSlogan::find(1)->judul_slogan ?? ''}}</span>
            </h2>
            {!! App\Models\TujuanSlogan::find(1)->slogan ?? '' !!}
        </div>
    </div>
    <div class="container">
        <div>
            <h2 class="section-title-underline text-center">
                <span>{{ App\Models\TujuanSlogan::find(1)->judul_tujuan ?? ''}}</span>
            </h2>
            {!! App\Models\TujuanSlogan::find(1)->tujuan ?? '' !!}
        </div>
    </div>
</div>

  <div class="section-bg style-1">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-6">
                <h2 class="section-title-underline mb-3">
                    <span class="text-white">{{ App\Models\VisiMisi::find(1)->judul_visi }}</span>
                </h2>
            </div>
        </div>
        <div class="row mb-5 d-flex justify-content-center">
            <div class="col-lg-12 col-md-6 mb-lg-0">
                <div class="text-white">{!! App\Models\VisiMisi::find(1)->visi !!}</div>
            </div>
        </div>
        <div class="row justify-content-center text-center">
            <div class="col-lg-6">
                <h2 class="section-title-underline mb-3">
                    <span class="text-white">{{ App\Models\VisiMisi::find(1)->judul_misi }}</span>
                </h2>
            </div>
        </div>
        <div class="row mb-2 d-flex justify-content-center">
            <div class="col-lg-12 col-md-6 mb-lg-0">
                <div class="text-white">{!! App\Models\VisiMisi::find(1)->misi !!}</div>
            </div>
        </div>
    </div>
</div>
    
<div class="site-section" id="budaya">
  <div class="container">
      <div class="row justify-content-center text-center">
          <div class="mb-5">
              <h2 class="section-title-underline mb-5">
                  <span>{{ App\Models\Budaya::find(1)->judul ?? ''  }}</span>
              </h2>
          </div>
      </div>
      <div class="row">
          <div class="col-lg-4 col-md-6 mb-lg-0">
              <div class="feature-1">
                  <div style="max-height:400px; overflow:hidden">
                      <img src="{{ asset('storage/' . App\Models\Budaya::find(1)->gambar) ?? '' }}" alt="" class="media-object rounded" width="300">
                  </div>
              </div>
          </div>
          <div class="col-lg-8 col-md-6 mb-lg-0">
              <div class="feature-1">
                  <div class="feature-1-content">
                      {!! App\Models\Budaya::find(1)->deskripsi !!}
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

<div class="site-section" id="jurusan">
    <div class="container">  
      <div class="row mb-5 justify-content-center text-center">
        <div class="col-lg-6 mb-5">
          <h2 class="section-title-underline mb-3">
            <span>Jurusan</span>
          </h2>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
            <div class="owl-slide-3 owl-carousel ">
                @foreach (App\Models\Jurusan::get() as $jurusan)
                <div class="card h-100">
                    <a href="{{ route('jurusan-detail', $jurusan->id) }}">
                        <img src="{{ asset('storage/' . $jurusan->gambar) }}" class="card-img-top">
                    </a>
                    <div class="card-body">
                        <p class="card-text card-title">{{ Str::limit(strip_tags($jurusan->judul), 50) }}</p>
                        <p><a href="{{ route('jurusan-detail', $jurusan->id) }}" class="btn btn-primary text-white rounded-0 px-8">Buka</a></p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
      </div>
    </div>
</div>
    <div class="site-section" id="sarana-prasarana">
        <div class="container">  
          <div class="row mb-5 justify-content-center text-center">
            <div class="col-lg-6 mb-5">
              <h2 class="section-title-underline mb-3">
                <span>Sarana Prasarana</span>
              </h2>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
                <div class="owl-slide-3 owl-carousel ">
                    @foreach (App\Models\SaranaPrasarana::get() as $saranaprasarana)
                    <div class="card h-100">
                        <a href="{{ route('saranaprasarana-detail', $saranaprasarana->id) }}">
                            <img src="{{ asset('storage/' . $saranaprasarana->gambar) }}" class="card-img-top">
                        </a>
                        <div class="card-body">
                            <p class="card-text card-title">{{ Str::limit(strip_tags($saranaprasarana->judul), 50) }}</p>
                            <p><a href="{{ route('saranaprasarana-detail', $saranaprasarana->id) }}" class="btn btn-primary text-white rounded-0 px-8">Buka</a></p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
          </div>
        </div>
    </div>

    <div class="site-section" id="prestasi">
        <div class="container">  
          <div class="row mb-5 justify-content-center text-center">
            <div class="col-lg-6 mb-5">
              <h2 class="section-title-underline mb-3">
                <span>Prestasi</span>
              </h2>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
                <div class="owl-slide-3 owl-carousel ">
                    @foreach (App\Models\Prestasi::get() as $prestasi)
                    <div class="card h-100">
                        <a href="{{ route('prestasi-detail', $prestasi->id) }}">
                            <img src="{{ asset('storage/' . $prestasi->gambar) }}" class="card-img-top">
                        </a>
                        <div class="card-body">
                            <p class="card-text card-title">{{ Str::limit(strip_tags($prestasi->judul), 50) }}</p>
                            <p><a href="{{ route('prestasi-detail', $prestasi->id) }}" class="btn btn-primary text-white rounded-0 px-8">Buka</a></p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
          </div>
        </div>
    </div>

    <div class="site-section" id="ekstrakurikuler">
        <div class="container">  
          <div class="row mb-5 justify-content-center text-center">
            <div class="col-lg-6 mb-5">
              <h2 class="section-title-underline mb-3">
                <span>Ekstrakurikuler</span>
              </h2>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
                <div class="owl-slide-3 owl-carousel ">
                    @foreach (App\Models\Ekstrakurikuler::get() as $ekstrakurikuler)
                    <div class="card h-100">
                        <img src="{{ asset('storage/' . $ekstrakurikuler->gambar) }}" class="card-img-top">
                        <div class="card-body">
                            <p class="card-text card-title">{{ Str::limit(strip_tags($ekstrakurikuler->judul), 50) }}</p>
                            <p><a href="{{ route('ekstrakurikuler-detail', $ekstrakurikuler->id) }}" class="btn btn-primary text-white rounded-0 px-8">Buka</a></p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
          </div>
        </div>
    </div>

</div>
@endsection