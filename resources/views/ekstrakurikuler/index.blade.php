@extends('layouts.master', ['title' => 'Ekstrakurikuler', 'first' => 'Ekstrakurikuler'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Ekstrakurikuler</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <a href="{{ route('ekstrakurikuler.create') }}" class="btn btn-primary mb-3">Ekstrakurikuler</a>
        <table id="data-table-default" class="table table-striped table-bordered table-td-valign-middle">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Gambar</th>
                    <th>Judul</th>
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>
                @foreach($ekstrakurikulers as $ekstrakurikuler)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>
                        <img src="{{ asset('storage/' . $ekstrakurikuler->gambar) }}" alt="{{ $ekstrakurikuler->gambar }}" width="100">
                    </td>
                    <td>{{ $ekstrakurikuler->judul }}</td>
                    <td class="d-flex">
                        <a href="{{ route('ekstrakurikuler.edit', $ekstrakurikuler->id) }}" class="btn btn-sm btn-success mr-1"><i class="fas fa-edit"></i></a>
                        <a href="{{ route('ekstrakurikuler.show', $ekstrakurikuler->id) }}" class="btn btn-sm btn-info mr-1"><i class="fas fa-eye"></i></a>
                        <form action="{{ route('ekstrakurikuler.destroy', $ekstrakurikuler->id) }}" method="post">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger mr-1" onclick="return confirm('Apakah anda yakin ingin menghapus ini?')"><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop