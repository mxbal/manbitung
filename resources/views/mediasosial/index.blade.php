@extends('layouts.master', ['title' => 'Media Sosial', 'first' => 'Media Sosial'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Media Sosial</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <a href="{{ route('mediasosial.create') }}" class="btn btn-primary mb-3">Buat Media Sosial</a>
        <table id="data-table-default" class="table table-striped table-bordered table-td-valign-middle">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Label</th>
                    <th>Ikon</th>
                    <th>Tautan</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($mediasosials as $mediasosial)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    
                    <td>{{ $mediasosial->label }}</td>
                    <td><span class="{{ $mediasosial->ikon }}"></span></td>
                    <td>{{ $mediasosial->tautan }}</td>
                    <td>{{ $mediasosial->tampil }}</td>
                    <td class="d-flex">
                        <a href="{{ route('mediasosial.edit', $mediasosial->id) }}" class="btn btn-sm btn-success mr-1"><i class="fas fa-edit"></i></a>
                        <div class="dropdown mr-1">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-exclamation"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <form action="{{ route('mediasosial.update', $mediasosial->id) }}" method="post">
                                    @csrf
                                    @method('PATCH')
                                    <input type="hidden" value="Aktif" name="status_tampil">
                                    <button type="submit" class="dropdown-item" value="Aktif">Aktif</button>
                                </form>
                                <form action="{{ route('mediasosial.update', $mediasosial->id) }}" method="post">
                                    @csrf
                                    @method('PATCH')
                                    <input type="hidden" value="Non-Aktif" name="status_tampil">
                                    <button type="submit" class="dropdown-item" value="Non-Aktif">Non-Aktif</button>
                                </form>
                            </div>
                        </div>

                        <form action="{{ route('mediasosial.destroy', $mediasosial->id) }}" method="post">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger mr-1" onclick="return confirm('Apakah anda yakin ingin menghapus data ini ?')"><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop