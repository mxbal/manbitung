@extends('layouts.master', ['title' => 'Media Sosial', 'first' => 'Media Sosial', 'second' => 'Form Media Sosial'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Media Sosial</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <form action="{{ $action }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="{{ $method }}">
            @csrf
            <div class="form-group">
                <label for="label">Label</label>
                <input type="text" name="label" id="label" class="form-control" value="{{ $mediasosial->label ?? old('label') }}">

                @error('label')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="tautan">Tautan</label>
                <input type="text" name="tautan" id="tautan" class="form-control" value="{{ $mediasosial->tautan ?? old('tautan') }}">

                @error('tautan')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="ikon">Ikon</label>
                <input type="text" name="ikon" id="ikon" class="form-control" value="{{ $mediasosial->ikon ?? old('ikon') }}">

                @error('ikon')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Kirim</button>
            </div>
        </form>
    </div>
</div>
@stop