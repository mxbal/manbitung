@extends('template.main')

@section('content')

    <head>
        <meta property='og:title' content='{{ App\Models\Logo::find(1)->deskripsi }}' />
        <meta property='og:image' itemprop="image"
            content='{{ asset('storage/' . App\Models\Logo::find(1)->gambar) ?? '' }}' />
        <meta property='og:description' content='{{ asset('storage/' . App\Models\Logo::find(1)->deskripsi) ?? '' }}' />
        <meta property='og:image:width' content='1200' />
        <meta property='og:image:height' content='630' />
        <meta property="og:type" content='website' />
    </head>

    <link itemprop="{{ asset('storage/' . App\Models\Logo::find(1)->gambar) ?? '' }}" href="{{ asset('storage/' . App\Models\Logo::find(1)->gambar) ?? '' }}">
    <span itemprop="{{ asset('storage/' . App\Models\Logo::find(1)->gambar) ?? '' }}" itemscope itemtype="{{ asset('storage/' . App\Models\Logo::find(1)->gambar) ?? '' }}">
        <link itemprop="url" href="{{ asset('storage/' . App\Models\Logo::find(1)->gambar) ?? '' }}">
    </span>


    <div class="hero-slide owl-carousel site-blocks-cover">
        @foreach (App\Models\Slider::get() as $slider)
            <div class="intro-section"
                style="background-image: url({{ asset('storage/' . $slider->gambar) ?? '' }}); background-position: center center">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-12 mx-auto text-center" data-aos="fade-up">
                            <h2 class="text-white">
                                <b>
                                    {{ $slider->judul ?? '' }}
                                </b>
                            </h2>
                            <center>
                                <p class="text-white"> {{ $slider->deskripsi ?? '' }}</p>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="site-section" id="sambutan">
        <div class="container">
            <div class="row justify-content-center">
                <div class="mb-5">
                    <h2 class="section-title-underline mb-5">
                        <span>{{ App\Models\Sambutan::find(1)->judul ?? '' }}</span>
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6 mb-lg-0">
                    <div class="feature-1">
                        <div style="max-height:400px; overflow:hidden">
                            <img src="{{ asset('storage/' . App\Models\Sambutan::find(1)->gambar) ?? '' }}" alt=""
                                class="media-object rounded" width="300">
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-6 mb-lg-0">
                    <div class="feature-1">
                        <div class="feature-1-content">
                            <div>
                                {!! App\Models\Sambutan::find(1)->deskripsi !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section">
        <div class="container">
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-6">
                    <h2 class="section-title-underline">
                        <span>{{ App\Models\TujuanSlogan::find(1)->judul_slogan ?? '' }}</span>
                    </h2>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-12 col-md-6 mb-lg-0">
                            <div>
                                {!! App\Models\TujuanSlogan::find(1)->slogan ?? '' !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="section-bg style-1">
        <div class="container">
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-6">
                    <h2 class="section-title-underline mb-3">
                        <span class="text-white">{{ App\Models\VisiMisi::find(1)->judul_visi ?? '' }}</span>
                    </h2>
                </div>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-lg-12 col-md-6 mb-lg-0">
                    <h3>{!! App\Models\VisiMisi::find(1)->visi ?? '' !!}</h3>
                </div>
            </div>
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-6">
                    <h2 class="section-title-underline mb-3">
                        <span class="text-white">{{ App\Models\VisiMisi::find(1)->judul_misi ?? '' }}</span>
                    </h2>
                </div>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-lg-12 col-md-6 mb-lg-0">
                    <h3>{!! App\Models\VisiMisi::find(1)->misi ?? '' !!}</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section">
        <div class="container">
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-6 mb-5">
                    <h2 class="section-title-underline mb-3">
                        <span>{{ App\Models\TujuanSlogan::find(1)->judul_tujuan ?? '' }}</span>
                    </h2>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-12 col-md-6 mb-lg-0">
                            {!! App\Models\TujuanSlogan::find(1)->tujuan ?? '' !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="news-updates">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="section-heading">
                        <h2 class="text-black">Berita</h2>
                        <a href="{{ route('blog') }}">Baca Semua Berita</a>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            @if (App\Models\Blog::count() !== 0)
                                @foreach (App\Models\Blog::first()->limit(1)->get()
        as $blog)
                                    <div class="post-entry-big">
                                        <a href="{{ route('blog-detail', App\Models\Blog::first()->id) ?? '' }}"
                                            class="img-link"><img
                                                src="{{ asset('storage/' . App\Models\Blog::first()->gambar ?? '') }}"
                                                alt="Image" class="img-fluid"></a>
                                        <div class="post-content">
                                            <div class="post-meta">
                                                <span
                                                    class="mx-1">{{ App\Models\Blog::first()->created_at->diffForHumans() ?? '' }}</span>
                                            </div>
                                            <h3 class="post-heading"><a
                                                    href="{{ route('blog-detail', App\Models\Blog::first()->id) }}">{{ App\Models\Blog::first()->judul ?? '' }}</a>
                                            </h3>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <div class="col-lg-6">
                            @foreach (App\Models\Blog::skip(1)->limit(3)->get()
        as $blog)
                                <div class="d-flex justify-content-center">
                                    <div class="post-entry-big horizontal d-flex mb-4">
                                        <a href="{{ route('blog-detail', $blog->id) }}" class="img-link mr-4"><img
                                                src="{{ asset('storage/' . $blog->gambar) ?? '' }}" height="72"
                                                alt="Image" class="img-fluid"></a>
                                        <div class="post-content">
                                            <div class="post-meta">
                                                <a href="#">{{ $blog->created_at->diffForHumans() }}</a>
                                                <span class="mx-1">{{ $blog->judul }}</span>
                                            </div>
                                            <h3 class="post-heading"><a
                                                    href="{{ route('blog-detail', $blog->id) }}">{{ $blog->judul }}</a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
