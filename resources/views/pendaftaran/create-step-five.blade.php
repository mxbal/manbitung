@extends('template.main')

@section('content')

<div class="site-section ftco-subscribe-1 site-blocks-cover pb-4" style="background-image: url('images/bg_1.jpg')">
    <div class="container">
      <div class="row align-items-end">
        <div class="col-lg-7">
          <h2 class="mb-0">Pendaftaran Siswa Baru</h2>
        </div>
      </div>
    </div>
  </div> 

<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="{{ route('home') }}">Beranda</a>
    <span class="mx-3 icon-keyboard_arrow_right"></span>
    <span class="current">Pendaftaran Siswa Baru</span>
  </div>
</div>


      <form action="{{ route('daftar.hal.lima') }}" method="POST">
        @csrf
        <div class="site-section">
            <div class="container">
            <div class="card">
            <div class="card-header">Data Orang Tua | Wali</div>

            <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    <div class="form-group">
                        <label for="nama_ayah">Nama Ayah</label>
                        <input type="text" value="{{ $siswa->nama_ayah ?? old('nama_ayah') }}" class="form-control"  name="nama_ayah">
                    </div>
                    <div class="form-group">
                        <label for="nik_ayah">Nik Ayah</label>
                        <input type="number" value="{{ $siswa->nik_ayah ?? old('nik_ayah') }}" class="form-control"  name="nik_ayah">
                    </div>
        
                    <div class="form-group">
                        <label for="alamat_ayah">Alamat Ayah</label>
                        <textarea type="text"  class="form-control" id="taskDescription" name="alamat_ayah">{{ $siswa->alamat_ayah ?? old('alamat_ayah') }}</textarea>
                      </div>
                    <div class="form-group">
                        <label for="tempat_lahir_ayah">Tempat Lahir Ayah</label>
                        <input type="text" value="{{ $siswa->tempat_lahir_ayah ?? old('tempat_lahir_ayah') }}" class="form-control"  name="tempat_lahir_ayah">
                    </div>
                    <div class="form-group">
                        <label for="tanggal_lahir_ayah">Tanggal Lahir Ayah</label>
                        <input type="date" value="{{ $siswa->tanggal_lahir_ayah ?? old('tanggal_lahir_ayah') }}" class="form-control"  name="tanggal_lahir_ayah">
                    </div>
                    <div class="form-group">
                        <label for="agama_ayah">Agama Ayah</label>
                        <input type="text" value="{{ $siswa->agama_ayah ?? old('agama_ayah') }}" class="form-control"  name="agama_ayah">
                    </div>
                    <div class="form-group">
                        <label for="pendidikan_ayah">Pendidikan Ayah</label>
                        <input type="text" value="{{ $siswa->pendidikan_ayah ?? old('pendidikan_ayah') }}" class="form-control"  name="pendidikan_ayah">
                    </div>
                    <div class="form-group">
                        <label for="pekerjaan_ayah">Pekerjaan Ayah</label>
                        <input type="text" value="{{ $siswa->pekerjaan_ayah ?? old('pekerjaan_ayah') }}" class="form-control"  name="pekerjaan_ayah">
                    </div>
                    <div class="form-group">
                        <label for="penghasilan_ayah">Penghasilan Ayah</label>
                        <input type="number" value="{{ $siswa->penghasilan_ayah ?? old('penghasilan_ayah') }}" class="form-control"  name="penghasilan_ayah">
                    </div>
                    <div class="form-group">
                        <label for="telp_ayah">Telepon Ayah</label>
                        <input type="number" value="{{ $siswa->telp_ayah ?? old('telp_ayah') }}" class="form-control"  name="telp_ayah">
                    </div>
                    <div class="form-group">
                        <label for="nama_ibu">Nama Ibu</label>
                        <input type="text" value="{{ $siswa->nama_ibu ?? old('nama_ibu') }}" class="form-control"  name="nama_ibu">
                    </div>
                    <div class="form-group">
                        <label for="nik_ibu">Nik Ibu</label>
                        <input type="number" value="{{ $siswa->nik_ibu ?? old('nik_ibu') }}" class="form-control"  name="nik_ibu">
                    </div>
                    <div class="form-group">
                        <label for="alamat_ibu">Alamat Ibu</label>
                        <textarea type="text"  class="form-control" id="taskDescription" name="alamat_ibu">{{ $siswa->alamat_ibu ?? old('alamat_ibu') }}</textarea>
                      </div>
                    <div class="form-group">
                        <label for="tempat_lahir_ibu">Tempat Lahir Ibu</label>
                        <input type="text" value="{{ $siswa->tempat_lahir_ibu ?? old('tempat_lahir_ibu') }}" class="form-control"  name="tempat_lahir_ibu">
                    </div>
                    <div class="form-group">
                        <label for="tanggal_lahir_ibu">Tanggal Lahir Ibu</label>
                        <input type="date" value="{{ $siswa->tanggal_lahir_ibu ?? old('tanggal_lahir_ibu') }}" class="form-control"  name="tanggal_lahir_ibu">
                    </div>
                    <div class="form-group">
                        <label for="agama_ibu">Agama Ibu</label>
                        <input type="text" value="{{ $siswa->agama_ibu ?? old('agama_ibu') }}" class="form-control"  name="agama_ibu">
                    </div>
                    <div class="form-group">
                        <label for="pendidikan_ibu">Pendidikan Ibu</label>
                        <input type="text" value="{{ $siswa->pendidikan_ibu ?? old('pendidikan_ibu') }}" class="form-control"  name="pendidikan_ibu">
                    </div>
                    <div class="form-group">
                        <label for="pekerjaan_ibu">Pekerjaan Ibu</label>
                        <input type="text" value="{{ $siswa->pekerjaan_ibu ?? old('pekerjaan_ibu') }}" class="form-control"  name="pekerjaan_ibu">
                    </div>
                    <div class="form-group">
                        <label for="penghasilan_ibu">Penghasilan Ibu</label>
                        <input type="number" value="{{ $siswa->penghasilan_ibu ?? old('penghasilan_ibu') }}" class="form-control"  name="penghasilan_ibu">
                    </div>
                    <div class="form-group">
                        <label for="telp_ibu">Telepon Ibu</label>
                        <input type="number" value="{{ $siswa->telp_ibu ?? old('telp_ibu') }}" class="form-control"  name="telp_ibu">
                    </div>
                    <div class="form-group">
                        <label for="nama_wali">Nama Wali</label>
                        <input type="text" value="{{ $siswa->nama_wali ?? old('nama_wali') }}" class="form-control"  name="nama_wali">
                    </div>
                    <div class="form-group">
                        <label for="nik_wali">Nik Wali</label>
                        <input type="number" value="{{ $siswa->nik_wali ?? old('nik_wali') }}" class="form-control"  name="nik_wali">
                    </div>
                    <div class="form-group">
                        <label for="alamat_wali">Alamat Wali</label>
                        <textarea type="text"  class="form-control" id="taskDescription" name="alamat_wali">{{ $siswa->alamat_wali ?? old('alamat_wali') }}</textarea>
                      </div>
                    <div class="form-group">
                        <label for="tempat_lahir_wali">Tempat Lahir Wali</label>
                        <input type="text" value="{{ $siswa->tempat_lahir_wali ?? old('tempat_lahir_wali') }}" class="form-control"  name="tempat_lahir_wali">
                    </div>
                    <div class="form-group">
                        <label for="tanggal_lahir_wali">Tanggal Lahir Wali</label>
                        <input type="date" value="{{ $siswa->tanggal_lahir_wali ?? old('tanggal_lahir_wali') }}" class="form-control"  name="tanggal_lahir_wali">
                    </div>
                    <div class="form-group">
                        <label for="agama_wali">Agama Wali</label>
                        <input type="text" value="{{ $siswa->agama_wali ?? old('agama_wali') }}" class="form-control"  name="agama_wali">
                    </div>
                    <div class="form-group">
                        <label for="pendidikan_wali">Pendidikan Wali</label>
                        <input type="text" value="{{ $siswa->pendidikan_wali ?? old('pendidikan_wali') }}" class="form-control"  name="pendidikan_wali">
                    </div>
                    <div class="form-group">
                        <label for="pekerjaan_wali">Pekerjaan Wali</label>
                        <input type="text" value="{{ $siswa->pekerjaan_wali ?? old('pekerjaan_wali') }}" class="form-control"  name="pekerjaan_wali">
                    </div>
                    <div class="form-group">
                        <label for="penghasilan_wali">Penghasilan Wali</label>
                        <input type="number" value="{{ $siswa->penghasilan_wali ?? old('penghasilan_wali') }}" class="form-control"  name="penghasilan_wali">
                    </div>
                    <div class="form-group">
                        <label for="telp_wali">Telepon Wali</label>
                        <input type="number" value="{{ $siswa->telp_wali ?? old('telp_wali') }}" class="form-control"  name="telp_wali">
                    </div>

            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-md-12  d-flex justify-content-between">
                        <a href="{{ route('daftar.hal.empat') }}" class="btn btn-danger">Kembali</a>
                        <button type="submit" class="btn btn-primary">Selanjutnya</button>
                    </div>
                </div>
            </div>
        </div>
            </div>
        </div>
    </form>

    </div>
</div>

@endsection