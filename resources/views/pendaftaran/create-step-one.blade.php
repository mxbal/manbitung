@extends('template.main')

@section('content')

<div class="site-section ftco-subscribe-1 site-blocks-cover pb-4" style="background-image: url('images/bg_1.jpg')">
    <div class="container">
      <div class="row align-items-end">
        <div class="col-lg-7">
          <h2 class="mb-0">Pendaftaran Siswa Baru</h2>
        </div>
      </div>
    </div>
  </div> 

    <div class="custom-breadcrumns border-bottom">
      <div class="container">
        <a href="{{ route('home') }}">Beranda</a>
        <span class="mx-3 icon-keyboard_arrow_right"></span>
        <span class="current">Pendaftaran Siswa Baru</span>
      </div>
    </div>

    <div class="container">
      <div class="row align-items-end">
        <div class="col-lg-10">
        <a href="{{ route('siswa.diterima') }}" class="btn btn-lg mt-5 btn-warning">Siswa Diterima</a>
        </div>
      </div>
    </div>
  </div> 
    
      <form action="{{ route('daftar.hal.satu.post') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="site-section">
        <div class="container">
        <div class="card">
            <div class="card-header">Data Diri</div>
                  <div class="card-body">
                    <div class="form-group">
                      <label for="jalur_ppdb">Jalur PPDB</label>
                      <select  class="form-control"  name="jalur_ppdb" id="">
                        <option value="" disabled selected>-- Pilih Jalur PPDB --</option>
                        <option value="Afirmasi" @if ($siswa->jalur_ppdb ?? old('jalur_ppdb') =='Afirmasi'){{ 'selected' }}@endif>Afirmasi / Keluarga Ekonomi Tidak Mampu</option>
                        <option value="Afirmasi Disabilitas"  @if ($siswa->jalur_ppdb ?? old('jalur_ppdb') =='Afirmasi Disabilitas') {{ 'selected' }}@endif>Afirmasi Disabilitas</option>
                        <option value="Perpindahan Tugas Orangtua" @if ($siswa->jalur_ppdb ?? old('jalur_ppdb') =='Perpindahan Tugas Orangtua') {{ 'selected' }}@endif>Perpindahan Tugas Orangtua</option>
                        <option value="Prestasi" @if ($siswa->jalur_ppdb ?? old('jalur_ppdb') =='Prestasi') {{ 'selected' }}@endif>Prestasi</option>
                        <option value="Zonasi" @if ($siswa->jalur_ppdb ?? old('jalur_ppdb') =='Zonasi') {{ 'selected' }}@endif>Zonasi</option>
                      </select>
                      @error('jalur_ppdb')
                      <small class="text-danger">{{ $message }}</small>
                      @enderror
                  </div>
                  <div class="form-group">
                    <label for="penerima_pkh">Penerima PKH</label>
                    <select  class="form-control"  name="penerima_pkh" id="">
                      <option value="" disabled selected>-- Penerima PKH --</option>
                      <option value="Ya" @if($siswa->penerima_pkh ?? old('penerima_pkh') == 'Ya') {{ 'selected' }}@endif>Ya</option>
                      <option value="Tidak" @if($siswa->penerima_pkh ?? old('penerima_pkh') == 'Tidak') {{ 'selected' }}@endif>Tidak</option>
                    </select>
                    @error('penerima_pkh')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                    <div class="form-group">
                      <label for="gelombang_pendaftaran">Gelombang Pendaftaran</label>
                      <select  class="form-control"  name="gelombang_pendaftaran" id="">
                        <option value="" disabled selected>-- Pilih Gelombang Pendaftaran --</option>
                        <option value="1" @if($siswa->gelombang_pendaftaran ?? old('gelombang_pendaftaran') =='1') {{ 'selected' }}@endif>1</option>
                        <option value="2" @if($siswa->gelombang_pendaftaran ?? old('gelombang_pendaftaran') =='2') {{ 'selected' }}@endif>2</option>
                        <option value="3" @if($siswa->gelombang_pendaftaran ?? old('gelombang_pendaftaran') =='3') {{ 'selected' }}@endif>3</option>
                      </select>
                      @error('gelombang_pendaftaran')
                      <small class="text-danger">{{ $message }}</small>
                      @enderror
                  </div>

                    <div class="form-group">
                        <label for="asal_sekolah">Asal Sekolah</label>
                        <input type="text"  value="{{ $siswa->asal_sekolah ?? old('asal_sekolah') }}" class="form-control" id="taskNama"  name="asal_sekolah">
                        @error('asal_sekolah')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                      <label for="tahun_lulus">Tahun Lulus</label>
                      <select  class="form-control"  name="tahun_lulus" id="">
                        <option value="" disabled selected>-- Tahun Lulus --</option>
                        @foreach ($years as $year)
                        <option value="{{ $year }}" @if($siswa->tahun_lulus ?? old('tahun_lulus') == $year) {{ 'selected' }}@endif>{{ $year }}</option>
                        @endforeach
                      </select>
                      @error('tahun_lulus')
                      <small class="text-danger">{{ $message }}</small>
                      @enderror
                  </div>
                  <div class="form-group">
                    <label for="tanggal_daftar">Tanggal Daftar</label>
                    <input type="date" value="{{ $siswa->tanggal_daftar ?? old('tanggal_daftar') }}" class="form-control" name="tanggal_daftar">
                    @error('tanggal_daftar')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                    <div class="form-group">
                        <label for="nama">Nama Lengkap</label>
                        <input type="text"  value="{{ $siswa->nama ?? old('nama') }}" class="form-control" id="taskNama"  name="nama">
                        @error('nama')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="nama_panggilan">Nama Panggilan</label>
                        <input type="text"  value="{{ $siswa->nama_panggilan ?? old('nama_panggilan') }}" class="form-control" id="taskNama"  name="nama_panggilan">
                        @error('nama_panggilan')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="nomor_kk">Nomor KK</label>
                        <input type="number" value="{{ $siswa->nomor_kk ?? old('nomor_kk') }}" class="form-control" name="nomor_kk">
                        @error('nomor_kk')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="nisn">NISN</label>
                        <input type="number" value="{{ $siswa->nisn ?? old('nisn') }}" class="form-control" name="nisn">
                        @error('nisn')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" value="{{ $siswa->email ?? old('email') }}" class="form-control" name="email">
                        @error('email')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="no_telepon">No Telepon</label>
                        <input type="number" value="{{ $siswa->no_telepon ?? old('no_telepon') }}" class="form-control" name="no_telepon">
                        @error('no_telepon')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="kewarganegaraan">Kewarganegaraan</label>
                        <input type="text" value="{{ $siswa->kewarganegaraan ?? old('kewarganegaraan') }}" class="form-control" name="kewarganegaraan">
                        @error('kewarganegaraan')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="bahasa">Bahasa</label>
                        <input type="text" value="{{ $siswa->bahasa ?? old('bahasa') }}" class="form-control" name="bahasa">
                        @error('bahasa')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="tempat_lahir">Tempat Lahir</label>
                        <input type="text" value="{{ $siswa->tempat_lahir ?? old('tempat_lahir') }}" class="form-control" name="tempat_lahir">
                        @error('tempat_lahir')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="tanggal_lahir">Tanggal Lahir :</label>
                        <input type="date" value="{{ $siswa->tanggal_lahir ?? old('tanggal_lahir') }}" class="form-control" name="tanggal_lahir">
                        @error('tanggal_lahir')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                      <label for="description">Jenis Kelamin</label><br/>
                      <label class="radio-inline"><input type="radio" name="jenis_kelamin" value="L" @if($siswa->jenis_kelamin ?? old('jenis_kelamin') =='L') {{ 'checked' }}@endif> Laki - Laki</label>
                      <label class="radio-inline"><input type="radio" name="jenis_kelamin" value="P" @if($siswa->jenis_kelamin ?? old('jenis_kelamin') =='P') {{ 'checked' }}@endif> Perempuan</label>
                      @error('jenis_kelamin')
                      <small class="text-danger">{{ $message }}</small>
                      @enderror
                    </div>

                    <div class="form-group">
                      <label for="agama">Agama</label>
                      <input type="text" value="{{ $siswa->agama ?? old('agama') }}" class="form-control" id="taskAgama"  name="agama">
                      @error('agama')
                      <small class="text-danger">{{ $message }}</small>
                      @enderror
                  </div>

                  <div class="form-group">
                    <label for="jurusan1">Jurusan Pertama</label>
                    <select  class="form-control"  name="jurusan1" id="jurusan1">
                      <option value="" disabled selected>Pilih Jurusan Pertama</option>
                        @foreach(App\Models\Jurusan::get() as $jurusan)
                          <option value="{{ $jurusan->judul }}"  @if(old('jurusan1') == $jurusan->judul) {{ 'selected' }}@endif>{{ $jurusan->judul }}</option>
                        @endforeach
                    </select>
                    @error('jurusan1')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                    <label for="jurusan2">Jurusan Kedua</label>
                    <select  class="form-control"  name="jurusan2" id="jurusan2">
                      <option value="" disabled selected>Pilih Jurusan Kedua</option>
                      @foreach(App\Models\Jurusan::get() as $jurusan)
                      <option value="{{ $jurusan->judul }}" @if(old('jurusan2') == $jurusan->judul) {{ 'selected' }}@endif>{{ $jurusan->judul }}</option>
                      @endforeach
                    </select>
                    @error('jurusan2')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                    <label for="jurusan3">Jurusan Ketiga</label>
                    <select  class="form-control"  name="jurusan3" id="jurusan3">
                      <option value="" disabled selected>Pilih Jurusan Ketiga</option>
                      @foreach(App\Models\Jurusan::get() as $jurusan)
                      <option value="{{ $jurusan->judul }}" @if( old('jurusan3') == $jurusan->judul) {{ 'selected' }}@endif>{{ $jurusan->judul }}</option>
                      @endforeach
                    </select>
                    @error('jurusan3')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="alasan">Alasan</label>
                    <input type="text" value="{{ $siswa->alasan ?? old('alasan') }}" class="form-control" id="taskAgama"  name="alasan">
                    @error('alasan')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                  <label for="hobi">Hobi</label>
                  <input type="text" value="{{ $siswa->hobi ?? old('hobi') }}" class="form-control" id="taskAgama"  name="hobi">
                  @error('hobi')
                  <small class="text-danger">{{ $message }}</small>
                  @enderror
              </div>
              <div class="form-group">
                <label for="cita_cita">Cita - Cita</label>
                <input type="text" value="{{ $siswa->cita_cita ?? old('cita_cita') }}" class="form-control" id="taskAgama"  name="cita_cita">
                @error('cita_cita')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary">Selanjutnya</button>
            </div>
        </div>
        </div>
        </div>
      </form>
    </div>
</div>

@endsection