@extends('template.main')

@section('content')

<div class="site-section ftco-subscribe-1 site-blocks-cover pb-4" style="background-image: url('images/bg_1.jpg')">
    <div class="container">
      <div class="row align-items-end">
        <div class="col-lg-7">
          <h2 class="mb-0">Pendaftaran Siswa Baru</h2>
        </div>
      </div>
    </div>
  </div> 

<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="{{ route('home') }}">Beranda</a>
    <span class="mx-3 icon-keyboard_arrow_right"></span>
    <span class="current">Pendaftaran Siswa Baru</span>
  </div>
</div>

      <form action="{{ route('daftar.hal.dua') }}" method="POST">
        @csrf
        <div class="site-section">
            <div class="container">
            <div class="card">
            <div class="card-header">Data Keluarga</div>

            <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="status_anak">Status Anak</label>
                        <input type="text"  value="{{ $siswa->status_anak ?? old('status_anak') }}" class="form-control"  name="status_anak">
                        @error('status_anak')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="anak_ke">Anak ke</label>
                        <input type="number"  value="{{ $siswa->anak_ke ?? old('anak_ke') }}" class="form-control"  name="anak_ke">
                        @error('anak_ke')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="jumlah_saudara">Jumlah Saudara</label>
                        <input type="number"  value="{{ $siswa->jumlah_saudara ?? old('jumlah_saudara') }}"class="form-control"  name="jumlah_saudara">
                        @error('jumlah_saudara')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="jumlah_saudara_kandung">Jumlah Saudara Kandung</label>
                        <input type="number"  value="{{ $siswa->jumlah_saudara_kandung ?? old('jumlah_saudara_kandung') }}"class="form-control"  name="jumlah_saudara_kandung">
                        @error('jumlah_saudara_kandung')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="jumlah_saudara_tiri">Jumlah Saudara Tiri</label>
                        <input type="number"  value="{{ $siswa->jumlah_saudara_tiri ?? old('jumlah_saudara_tiri') }}"class="form-control"  name="jumlah_saudara_tiri">
                        @error('jumlah_saudara_tiri')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="jumlah_saudara_angkat">Jumlah Saudara Angkat</label>
                        <input type="number"  value="{{ $siswa->jumlah_saudara_angkat ?? old('jumlah_saudara_angkat') }}"class="form-control"  name="jumlah_saudara_angkat">
                        @error('jumlah_saudara_angkat')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-md-12  d-flex justify-content-between">
                        <a href="{{ route('daftar.hal.dua') }}" class="btn btn-danger">Kembali</a>
                        <button type="submit" class="btn btn-primary">Selanjutnya</button>
                    </div>
                </div>
            </div>
        </div>
            </div>
        </div>
    </form>

    </div>
</div>

@endsection