@extends('template.main')

@section('content')

<div class="site-section ftco-subscribe-1 site-blocks-cover pb-4" style="background-image: url('images/bg_1.jpg')">
    <div class="container">
      <div class="row align-items-end">
        <div class="col-lg-7">
          <h2 class="mb-0">Pendaftaran Siswa Baru</h2>
        </div>
      </div>
    </div>
  </div> 

<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="{{ route('home') }}">Beranda</a>
    <span class="mx-3 icon-keyboard_arrow_right"></span>
    <span class="current">Pendaftaran Siswa Baru</span>
  </div>
</div>

      <form action="{{ route('daftar.hal.empat') }}" method="POST">
        @csrf
        <div class="site-section">
            <div class="container">
            <div class="card">
            <div class="card-header">Data Alamat</div>

            <div class="card-body">
                   
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea type="text"  class="form-control" id="taskDescription" name="alamat">{{ $siswa->alamat ?? old('alamat') }}</textarea>
                        @error('alamat')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                   
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="rw">RW</label>
                                <input type="number" value="{{ $siswa->rw ?? old('rw') }}" class="form-control"  name="rw">
                                @error('rw')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="col">
                                <label for="rt">RT</label>
                                <input type="number" value="{{ $siswa->rt ?? old('rt') }}" class="form-control"  name="rt">
                                @error('rt')
                                <small class="text-danger">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="provinsi">Provinsi</label>
                        <select  class="form-control"  name="provinsi" id="provinsi">
                          <option value="" disabled selected>-- Provinsi --</option>
                          @foreach ($provinsis as $provinsi)
                          <option value="{{ $provinsi->province }}-{{ $provinsi->province_id }}">{{ $provinsi->province }}</option>
                          @endforeach
                        </select>
                        @error('provinsi')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="domisili">Domisili</label>
                        <select  class="form-control"  name="domisili" id="domisili">
                          <option value="" disabled>-- Domisili --</option>
                        </select>
                        @error('domisili')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="kode_pos">Kode Pos</label>
                        <input type="number" value="{{ $siswa->kode_pos ?? old('kode_pos') }}" id='kode_pos' class="form-control"  name="kode_pos">
                        @error('kode_pos')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="kecamatan">Kecamatan</label>
                        <input type="text" value="{{ $siswa->kecamatan ?? old('kecamatan') }}" id='kecamatan' class="form-control"  name="kecamatan">
                        @error('kecamatan')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="kelurahan">Kelurahan</label>
                        <input type="text" value="{{ $siswa->kelurahan ?? old('kelurahan') }}" id='kelurahan' class="form-control"  name="kelurahan">
                        @error('kelurahan')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="jarak_kesekolah">Jarak Kesekolah (KM)</label>
                        <input type="number" value="{{ $siswa->jarak_kesekolah ?? old('jarak_kesekolah') }}" class="form-control"  name="jarak_kesekolah">
                        @error('jarak_kesekolah')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="transportasi_kesekolah">Transportasi</label>
                        <input type="text" value="{{ $siswa->transportasi_kesekolah ?? old('transportasi_kesekolah') }}" class="form-control"  name="transportasi_kesekolah">
                        @error('transportasi_kesekolah')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="alamat_sekolah_asal">Alamat Sekolah Asal</label>
                        <textarea type="text"  class="form-control" id="taskDescription" name="alamat_sekolah_asal">{{ $siswa->alamat_sekolah_asal ?? old('alamat_sekolah_asal') }}</textarea>
                        @error('alamat_sekolah_asal')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>     
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-md-12  d-flex justify-content-between">
                        <a href="{{ route('daftar.hal.tiga') }}" class="btn btn-danger">Kembali</a>
                        <button type="submit" class="btn btn-primary">Selajutnya</button>
                    </div>
                </div>
            </div>
        </div>
            </div>
        </div>
    </form>
    </div>
</div>
<script>
    $(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
    })
        $(function(){
            $('#provinsi').on('change', function(){
                let provinsi = $('#provinsi').val()
                $.ajax({
                    type: 'POST',
                    url: "{{ route('api.provinsi') }}",
                    data: {provinsi: provinsi},
                    cache: false,
                    success: function(msg){
                        $('#domisili').html(msg)
                    },
                    error: function(data){
                        console.log('error : ', data)
                    }
                })
            })
        })
        $(function(){
            $('#domisili').on('change', function(){
                let provinsi = $('#provinsi').val()
                let id_district = $('#domisili').val()
                $.ajax({
                    type: 'POST',
                    url: "{{ route('api.domisili') }}",
                    data: {provinsi: provinsi, id_district: id_district },
                    cache: false,
                    success: function(msg){
                        console.log(msg)
                    },
                    error: function(data){
                        console.log('error : ', data)
                    }
                })
            })
        })
</script>

@endsection