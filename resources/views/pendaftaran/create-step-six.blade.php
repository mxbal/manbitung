@extends('template.main')

@section('content')

<div class="site-section ftco-subscribe-1 site-blocks-cover pb-4" style="background-image: url('images/bg_1.jpg')">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-lg-7">
                <h2 class="mb-0">Pendaftaran Siswa Baru</h2>
            </div>
        </div>
    </div>
</div>

<div class="custom-breadcrumns border-bottom">
    <div class="container">
        <a href="{{ route('home') }}">Beranda</a>
        <span class="mx-3 icon-keyboard_arrow_right"></span>
        <span class="current">Pendaftaran Siswa Baru</span>
    </div>
</div>

<form action="{{ route('daftar.hal.enam') }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="site-section">
        <div class="container">
            <div class="card">
                <div class="card-header">Review Detail</div>

                <div class="card-body">
                    <div class="form-group">
                        <label for="image">Foto</label>
                        <input type="file" value="{{ $siswa->image ?? old('image') }}" class="form-control" name="image" id="image" onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])">
                        <br>
                        <img src="" alt="" class="media-object rounded mb-3" id="preview" width="100">
                        @error('image')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <table class="table">
                        <tr>
                            <td>Jalur PPDB</td>
                            <td><strong>{{$siswa->jalur_ppdb}}</strong></td>
                        </tr>
                        <tr>
                            <td>Gelombang Pendaftaaran</td>
                            <td><strong>{{$siswa->gelombang_pendaftaran}}</strong></td>
                        </tr>
                        <tr>
                            <td>Tanggal Daftar</td>
                            <td><strong>{{$siswa->tanggal_daftar}}</strong></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td><strong>{{$siswa->nama}}</strong></td>
                        </tr>
                        <tr>
                            <td>Nama Panggilan</td>
                            <td><strong>{{$siswa->nama_panggilan}}</strong></td>
                        </tr>
                        <tr>
                            <td>Kewarganegaraan</td>
                            <td><strong>{{$siswa->kewarganegaraan}}</strong></td>
                        </tr>
                        <tr>
                            <td>Bahasa</td>
                            <td><strong>{{$siswa->bahasa}}</strong></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td><strong>{{$siswa->email}}</strong></td>
                        </tr>
                        <tr>
                            <td>No Telepon</td>
                            <td><strong>{{$siswa->no_telepon}}</strong></td>
                        </tr>
                        <tr>
                            <td>No KK</td>
                            <td><strong>{{$siswa->nomor_kk}}</strong></td>
                        </tr>
                        <tr>
                            <td>Jurusan</td>
                            <td><strong>{{$siswa->jurusan}}</strong></td>
                        </tr>
                        <tr>
                            <td>Alasan</td>
                            <td><strong>{{$siswa->alasan}}</strong></td>
                        </tr>
                        <tr>
                            <td>Hobi</td>
                            <td><strong>{{$siswa->hobi}}</strong></td>
                        </tr>
                        <tr>
                            <td>Cita - Cita</td>
                            <td><strong>{{$siswa->cita_cita}}</strong></td>
                        </tr>
                        <tr>
                            <td>Tempat Lahir</td>
                            <td><strong>{{$siswa->tempat_lahir}}</strong></td>
                        </tr>
                        <tr>
                            <td>Tanggal Lahir</td>
                            <td><strong>{{$siswa->tanggal_lahir}}</strong></td>
                        </tr>
                        <tr>
                            <td>Jenis Kelamin</td>
                            <td><strong>{{$siswa->jenis_kelamin}}</strong></td>
                        </tr>
                        <tr>
                            <td>Hobi</td>
                            <td><strong>{{$siswa->hobi}}</strong></td>
                        </tr>
                        <tr>
                            <td>Penerima PKH</td>
                            <td><strong>{{$siswa->penerima_pkh}}</strong></td>
                        </tr>
                        <tr>
                            <td>Cita - cita</td>
                            <td><strong>{{$siswa->cita_cita}}</strong></td>
                        </tr>
                        <tr>
                            <td>NISN</td>
                            <td><strong>{{$siswa->nisn}}</strong></td>
                        </tr>
                        <tr>
                            <td>Agama</td>
                            <td><strong>{{$siswa->agama}}</strong></td>
                        </tr>
                        <tr>
                            <td>Status Anak</td>
                            <td><strong>{{$siswa->status_anak}}</strong></td>
                        </tr>
                        <tr>
                            <td>Anak Ke</td>
                            <td><strong>{{$siswa->anak_ke}}</strong></td>
                        </tr>
                        <tr>
                            <td>Jumlah Saudara</td>
                            <td><strong>{{$siswa->jumlah_saudara}}</strong></td>
                        </tr>
                            <tr>
                                <td>Jumlah Saudara Kandung</td>
                                <td><strong>{{$siswa->jumlah_saudara_kandung}}</strong></td>
                            </tr>
                            <tr>
                                <td>Jumlah Saudara Angkat</td>
                                <td><strong>{{$siswa->jumlah_saudara_angkat}}</strong></td>
                            </tr>
                            <tr>
                                <td>Jumlah Saudara Tiri</td>
                                <td><strong>{{$siswa->jumlah_saudara_tiri}}</strong></td>
                            </tr>
                        <tr>
                            <td>Tinggi Badan</td>
                            <td><strong>{{$siswa->tinggi_badan}}</strong></td>
                        </tr>
                        <tr>
                            <td>Asal Sekolah</td>
                            <td><strong>{{$siswa->asal_sekolah}}</strong></td>
                        </tr>
                        <tr>
                            <td>Tahun Lulus</td>
                            <td><strong>{{$siswa->tahun_lulus}}</strong></td>
                        </tr>
                        <tr>
                            <td>Berat Badan</td>
                            <td><strong>{{$siswa->berat_badan}}</strong></td>
                        </tr>
                        <tr>
                            <td>Cacat Badan</td>
                            <td><strong>{{$siswa->cacat_badan}}</strong></td>
                        </tr>
                        <tr>
                            <td>Golongan Darah</td>
                            <td><strong>{{$siswa->golongan_darah}}</strong></td>
                        </tr>
                        <tr>
                            <td>Penyakit Bawaan</td>
                            <td><strong>{{$siswa->penyakit_bawaan}}</strong></td>
                        </tr>
                        <tr>
                            <td>Pernah Sakit</td>
                            <td><strong>{{$siswa->pernah_sakit}}</strong></td>
                        </tr>
                        <tr>
                            <td>Nama Penyakit</td>
                            <td><strong>{{$siswa->nama_penyakit}}</strong></td>
                        </tr>
                        <tr>
                            <td>Tanggal Sakit</td>
                            <td><strong>{{$siswa->tanggal_sakit}}</strong></td>
                        </tr>
                        <tr>
                            <td>Lama sakit</td>
                            <td><strong>{{$siswa->lama_sakit}}</strong></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td><strong>{{$siswa->alamat}}</strong></td>
                        </tr>
                        <tr>
                            <td>RT</td>
                            <td><strong>{{$siswa->rt}}</strong></td>
                        </tr>
                        <tr>
                            <td>RW</td>
                            <td><strong>{{$siswa->rw}}</strong></td>
                        </tr>
                        <tr>
                            <td>Kelurahan</td>
                            <td><strong>{{$siswa->kelurahan}}</strong></td>
                        </tr>
                        <tr>
                            <td>Kecamatan</td>
                            <td><strong>{{$siswa->kecamatan}}</strong></td>
                        </tr>
                        <tr>
                            <td>Domisili</td>
                            <td><strong>{{$siswa->domisili}}</strong></td>
                        </tr>
                        <tr>
                            <td>Provinsi</td>
                            <td><strong>{{$siswa->provinsi}}</strong></td>
                        </tr>
                        <tr>
                            <td>Kode Pos</td>
                            <td><strong>{{$siswa->kode_pos}}</strong></td>
                        </tr>
                        <tr>
                            <td>Jarak Kesekolah</td>
                            <td><strong>{{$siswa->jarak_kesekolah}}</strong></td>
                        </tr>
                        <tr>
                            <td>Transportasi Kesekolah</td>
                            <td><strong>{{$siswa->transportasi_kesekolah}}</strong></td>
                        </tr>
                        <tr>
                            <td>Alamat Sekolah Asal</td>
                            <td><strong>{{$siswa->alamat_sekolah_asal}}</strong></td>
                        </tr>
                        <tr>
                            <td>Nama Ayah</td>
                            <td><strong>{{$siswa->nama_ayah}}</strong></td>
                        </tr>
                        <tr>
                            <td>NIK Ayah</td>
                            <td><strong>{{$siswa->nik_ayah}}</strong></td>
                        </tr>
                        <tr>
                            <td>Alamat Ayah</td>
                            <td><strong>{{$siswa->alamat_ayah}}</strong></td>
                        </tr>
                        <tr>
                            <td>Tempat Lahir Ayah</td>
                            <td><strong>{{$siswa->tempat_lahir_ayah}}</strong></td>
                        </tr>
                        <tr>
                            <td>Tanggal Lahir Ayah</td>
                            <td><strong>{{$siswa->tanggal_lahir_ayah}}</strong></td>
                        </tr>
                        <tr>
                            <td>Agama Ayah</td>
                            <td><strong>{{$siswa->agama_ayah}}</strong></td>
                        </tr>
                        <tr>
                            <td>Pendidikan Ayah</td>
                            <td><strong>{{$siswa->pendidikan_ayah}}</strong></td>
                        </tr>
                        <tr>
                            <td>Pekerjaan Ayah</td>
                            <td><strong>{{$siswa->pekerjaan_ayah}}</strong></td>
                        </tr>
                        <tr>
                            <td>Penghasilan Ayah</td>
                            <td><strong>{{$siswa->penghasilan_ayah}}</strong></td>
                        </tr>
                        <tr>
                            <td>Telepon Ayah</td>
                            <td><strong>{{$siswa->telp_ayah}}</strong></td>
                        </tr>
                        <tr>
                            <td>Nama Ibu</td>
                            <td><strong>{{$siswa->nama_ibu}}</strong></td>
                        </tr>
                        <tr>
                            <td>NIK Ibu</td>
                            <td><strong>{{$siswa->nik_ibu}}</strong></td>
                        </tr>
                        <tr>
                            <td>Alamat Ibu</td>
                            <td><strong>{{$siswa->alamat_ibu}}</strong></td>
                        </tr>
                        <tr>
                            <td>Tempat Lahir Ibu</td>
                            <td><strong>{{$siswa->tempat_lahir_ibu}}</strong></td>
                        </tr>
                        <tr>
                            <td>Tanggal Lahir Ibu</td>
                            <td><strong>{{$siswa->tanggal_lahir_ibu}}</strong></td>
                        </tr>
                        <tr>
                            <td>Agama Ibu</td>
                            <td><strong>{{$siswa->agama_ibu}}</strong></td>
                        </tr>
                        <tr>
                            <td>Pendidikan Ibu</td>
                            <td><strong>{{$siswa->pendidikan_ibu}}</strong></td>
                        </tr>
                        <tr>
                            <td>Pekerjaan Ibu</td>
                            <td><strong>{{$siswa->pekerjaan_ibu}}</strong></td>
                        </tr>
                        <tr>
                            <td>Penghasilan Ibu</td>
                            <td><strong>{{$siswa->penghasilan_ibu}}</strong></td>
                        </tr>
                        <tr>
                            <td>Telepon Ibu</td>
                            <td><strong>{{$siswa->telp_ibu}}</strong></td>
                        </tr>
                        <tr>
                            <td>Nama Wali</td>
                            <td><strong>{{$siswa->nama_wali}}</strong></td>
                        </tr>
                        <tr>
                            <td>NIK Wali</td>
                            <td><strong>{{$siswa->nik_wali}}</strong></td>
                        </tr>
                        <tr>
                            <td>Alamat Wali</td>
                            <td><strong>{{$siswa->alamat_wali}}</strong></td>
                        </tr>
                        <tr>
                            <td>Tempat Lahir Wali</td>
                            <td><strong>{{$siswa->tempat_lahir_wali}}</strong></td>
                        </tr>
                        <tr>
                            <td>Tanggal Lahir Wali</td>
                            <td><strong>{{$siswa->tanggal_lahir_wali}}</strong></td>
                        </tr>
                        <tr>
                            <td>Agama Wali</td>
                            <td><strong>{{$siswa->agama_wali}}</strong></td>
                        </tr>
                        <tr>
                            <td>Pendidikan Wali</td>
                            <td><strong>{{$siswa->pendidikan_wali}}</strong></td>
                        </tr>
                        <tr>
                            <td>Pekerjaan Wali</td>
                            <td><strong>{{$siswa->pekerjaan_wali}}</strong></td>
                        </tr>
                        <tr>
                            <td>Penghasilan Wali</td>
                            <td><strong>{{$siswa->penghasilan_wali}}</strong></td>
                        </tr>
                        <tr>
                            <td>Telepon Wali</td>
                            <td><strong>{{$siswa->telp_wali}}</strong></td>
                        </tr>

                    </table>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-12  d-flex justify-content-between">
                            <a href="{{ route('daftar.hal.lima') }}" class="btn btn-danger">Kembali</a>
                            <button type="submit" class="btn btn-primary" onclick="return confirm('Apakah anda yakin bahwa data yang anda kirim sudah benar?')">Kirim</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="ModalConfirm" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalConfirm">Konfirmasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah Yakin Data Sudah Benar ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Kirim</button>
            </div>
        </div>
    </div>
</div>
@endsection