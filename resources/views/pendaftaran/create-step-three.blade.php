@extends('template.main')

@section('content')

<div class="site-section ftco-subscribe-1 site-blocks-cover pb-4" style="background-image: url('images/bg_1.jpg')">
    <div class="container">
      <div class="row align-items-end">
        <div class="col-lg-7">
          <h2 class="mb-0">Pendaftaran Siswa Baru</h2>
        </div>
      </div>
    </div>
  </div> 

  <div class="custom-breadcrumns border-bottom">
    <div class="container">
      <a href="{{ route('home') }}">Beranda</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <span class="current">Pendaftaran Siswa Baru</span>
    </div>
  </div>
        <form action="{{ route('daftar.hal.tiga') }}" method="POST">
          @csrf
          <div class="site-section">
              <div class="container">
              <div class="card">
              <div class="card-header">Data Kesehatan</div>
              <div class="card-body">
                <div class="form-group">
                  <label for="golongan_darah">Golongan Darah</label>
                  <select  class="form-control"  name="golongan_darah" id="">
                    <option value="" disabled selected>-- Pilih Golongan Darah --</option>
                    <option value="A" @if($calonsiswa->golongan_darah ?? old('golongan_darah') =='A') {{ 'selected' }}@endif>A</option>
                    <option value="B" @if($calonsiswa->golongan_darah ?? old('golongan_darah') =='B') {{ 'selected' }}@endif>B</option>
                    <option value="AB" @if($calonsiswa->golongan_darah ?? old('golongan_darah') =='AB') {{ 'selected' }}@endif>AB</option>
                    <option value="O" @if($calonsiswa->golongan_darah ?? old('golongan_darah')=='O') {{ 'selected' }}@endif>O</option>
                    <option value="lainnya">lainnya</option>
                  </select>
                  @error('golongan_darah')
                  <small class="text-danger">{{ $message }}</small>
                  @enderror
              </div>
                      <div class="form-group">
                          <label for="tinggi_badan">Tinggi Badan (cm)</label>
                          <input type="number" value="{{ $siswa->tinggi_badan ?? old('tinggi_badan') }}" class="form-control"  name="tinggi_badan">
                          @error('tinggi_badan')
                          <small class="text-danger">{{ $message }}</small>
                          @enderror
                      </div>
                      <div class="form-group">
                          <label for="berat_badan">Berat Badan (kg)</label>
                          <input type="number" value="{{ $siswa->berat_badan ?? old('berat_badan') }}" class="form-control"  name="berat_badan">
                          @error('berat_badan')
                          <small class="text-danger">{{ $message }}</small>
                          @enderror
                      </div>
                      <div class="form-group">
                          <label for="cacat_badan">Cacat Badan</label>
                          <input type="text" value="{{ $siswa->cacat_badan ?? old('cacat_badan') }}" class="form-control"  name="cacat_badan">
                          @error('cacat_badan')
                          <small class="text-danger">{{ $message }}</small>
                          @enderror
                      </div>
                      <div class="form-group">
                          <label for="penyakit_bawaan">Penyakit Bawaan</label>
                          <input type="text" value="{{ $siswa->penyakit_bawaan ?? old('penyakit_bawaan') }}" class="form-control"  name="penyakit_bawaan">
                          @error('penyakit_bawaan')
                          <small class="text-danger">{{ $message }}</small>
                          @enderror
                      </div>
                      <div class="form-group">
                        <label for="pernah_sakit">Pernah Sakit</label>
                        <input type="text" value="{{ $siswa->pernah_sakit ?? old('pernah_sakit') }}" class="form-control"  name="pernah_sakit">
                        @error('pernah_sakit')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                      <div class="form-group">
                        <label for="nama_penyakit">Nama Penyakit</label>
                        <input type="text" value="{{ $siswa->nama_penyakit ?? old('nama_penyakit') }}" class="form-control"  name="nama_penyakit">
                        @error('nama_penyakit')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                      <label for="tanggal_sakit">Tanggal Sakit</label>
                      <input type="date" value="{{ $siswa->tanggal_sakit ?? old('tanggal_sakit') }}" class="form-control"  name="tanggal_sakit">
                      @error('tanggal_sakit')
                      <small class="text-danger">{{ $message }}</small>
                      @enderror
                  </div>
                  <div class="form-group">
                    <label for="lama_sakit">Lama Sakit</label>
                    <input type="text" value="{{ $siswa->lama_sakit ?? old('lama_sakit') }}" class="form-control"  name="lama_sakit">
                    @error('lama_sakit')
                    <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                  
                      <div class="card-footer">
                        <div class="row">
                            <div class="col-md-12  d-flex justify-content-between">
                                <a href="{{ route('daftar.hal.dua') }}" class="btn btn-danger">Kembali</a>
                                <button type="submit" class="btn btn-primary">Selanjutnya</button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
          </div>
      </form>

    </div>
</div>

@endsection