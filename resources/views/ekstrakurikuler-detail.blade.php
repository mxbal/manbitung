@extends('template.main')

@section('content')

<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="{{ route('home') }}">Beranda</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <span class="current">Ekstrakurikuler</span>
  </div>
  <div class="py-5 text-center">
    <img class="d-block mx-auto mb-4" style="height: 400px" alt="{{ $ekstrakurikuler->gambar }}" src="{{ asset('storage/' . $ekstrakurikuler->gambar ?? ''  ) }}">
  </div>

  <div class="site-section">
    <div class="container">
      <div class="blog-post">
        <h2 class="blog-post-title">{{ $ekstrakurikuler->judul }}</h2>
          {!! $ekstrakurikuler->deskripsi !!}
      </div>

    </div>
</div>
</div>
@endsection