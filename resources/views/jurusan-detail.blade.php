@extends('template.main')

@section('content')

<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="{{ route('home') }}">Beranda</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <span class="current">Jurusan</span>
  </div>
  <div class="py-5 text-center">
    <img class="d-block mx-auto mb-4" style="height: 400px" alt="{{ $jurusan->gambar }}" src="{{ asset('storage/' . $jurusan->gambar ?? ''  ) }}">
  </div>

  <div class="site-section">
    <div class="container">
      <div class="blog-post">
        <h2 class="blog-post-title">{{ $jurusan->judul }}</h2>
          {!! $jurusan->deskripsi !!}
      </div>

    </div>
</div>
</div>
@endsection