@extends('layouts.master', ['title' => 'Tujuan | Slogan', 'first' => 'Tujuan | Slogan'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Tujuan | Slogan</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <form action="{{ route('tujuan-slogan.update', $tujuanslogan->id) }}" method="post" enctype="multipart/form-data">
            @method('PATCH')
            @csrf
            <div class="media">
                <div class="media-body">
                    <div class="form-group">
                        <label for="judul_slogan">Judul Slogan</label>
                        <input type="text" name="judul_slogan" id="judul_slogan" class="form-control" value="{{ $tujuanslogan->judul_slogan ?? old('judul_slogan') }}">

                        @error('judul_slogan')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="slogan">Slogan</label>
                        <textarea id="editor" name="slogan">{{ $tujuanslogan->slogan ?? old('slogan') }}</textarea>
        
                        @error('slogan')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="media-body">
                        <div class="form-group">
                            <label for="judul_tujuan">Judul Tujuan</label>
                            <input type="text" name="judul_tujuan" id="judul_tujuan" class="form-control" value="{{ $tujuanslogan->judul_tujuan ?? old('judul_tujuan') }}">
    
                            @error('judul_tujuan')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="tujuan">Tujuan</label>
                            <textarea id="editor" name="tujuan">{{ $tujuanslogan->tujuan ?? old('tujuan') }}</textarea>
            
                            @error('tujuan')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop