@extends('layouts.master', ['title' => 'Ketenagaan', 'first' => 'Ketenagaan'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Ketenagaan</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <a href="{{ route('ketenagaan.create') }}" class="btn btn-primary mb-3">Buat Ketenagaan</a>
        <table id="data-table-default" class="table table-striped table-bordered table-td-valign-middle">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIP</th>
                    <th>Kategori</th>
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>
                @foreach($ketenagaans as $ketenagaan)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $ketenagaan->nama }}</td>
                    <td>{{ $ketenagaan->nip }}</td>
                    <td>{{ $ketenagaan->kategori }}</td>
                    <td class="d-flex">
                        <a href="{{ route('ketenagaan.edit', $ketenagaan->id) }}" class="btn btn-sm btn-success mr-1"><i class="fas fa-edit"></i></a>
                        <a href="{{ route('ketenagaan.show', $ketenagaan->id) }}" class="btn btn-sm btn-info mr-1"><i class="fas fa-eye"></i></a>
                        <form action="{{ route('ketenagaan.destroy', $ketenagaan->id) }}" method="post">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger mr-1" onclick="return confirm('Apakah anda yakin ingin menghapus data ini?')"><i class="fas fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop