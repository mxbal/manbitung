@extends('layouts.master', ['title' => 'Ketenagaan', 'first' => 'Ketenagaan', 'second' => 'Buat Ketenagaan'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Ketenagaan</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
        <form action="{{ $action }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="{{ $method }}">
            @csrf
            <div class="form-group">
                <label for="gambar">Foto</label>
                <input type="file" name="gambar" id="gambar" class="form-control" onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])">
                <br>
                <img id="preview" alt="your gambar" width="100" src="{{ asset('storage/' . $ketenagaan->gambar) ?? '' }}" />

                @error('gambar')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" name="nama" id="nama" class="form-control" value="{{ $ketenagaan->nama ?? old('nama') }}">

                @error('nama')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="nip">NIP</label>
                <input type="text" name="nip" id="nip" class="form-control" value="{{ $ketenagaan->nip ?? old('nip') }}">

                @error('nip')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="nuptk">NUPTK</label>
                <input type="text" name="nuptk" id="nuptk" class="form-control" value="{{ $ketenagaan->nuptk ?? old('nuptk') }}">

                @error('nuptk')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <input type="text" name="status" id="status" class="form-control" value="{{ $ketenagaan->status ?? old('status') }}">

                @error('status')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="pendidikan">Pendidikan</label>
                <input type="text" name="pendidikan" id="pendidikan" class="form-control" value="{{ $ketenagaan->pendidikan ?? old('pendidikan') }}">

                @error('pendidikan')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="jabatan">Jabatan</label>
                <input type="text" name="jabatan" id="jabatan" class="form-control" value="{{ $ketenagaan->jabatan ?? old('jabatan') }}">

                @error('jabatan')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="jenis_kelamin">Jenis Kelamin</label>
                <input type="text" name="jenis_kelamin" id="jenis_kelamin" class="form-control" value="{{ $ketenagaan->jenis_kelamin ?? old('jenis_kelamin') }}">

                @error('jenis_kelamin')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="agama">Agama</label>
                <input type="text" name="agama" id="agama" class="form-control" value="{{ $ketenagaan->agama ?? old('agama') }}">

                @error('agama')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="tempat_lahir">Tempat Lahir</label>
                <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" value="{{ $ketenagaan->tempat_lahir ?? old('tempat_lahir') }}">

                @error('tempat_lahir')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="tanggal_lahir">Tanggal Lahir</label>
                <input type="date" name="tanggal_lahir" id="tanggal_lahir" class="form-control" value="{{ $ketenagaan->tanggal_lahir ?? old('tanggal_lahir') }}">

                @error('tanggal_lahir')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="kode_pos">Kode Pos</label>
                <input type="text" name="kode_pos" id="kode_pos" class="form-control" value="{{ $ketenagaan->kode_pos ?? old('kode_pos') }}">

                @error('kode_pos')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="telepon">Telepon</label>
                <input type="text" name="telepon" id="telepon" class="form-control" value="{{ $ketenagaan->telepon ?? old('telepon') }}">

                @error('telepon')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="instansi">Instansi</label>
                <input type="text" name="instansi" id="instansi" class="form-control" value="{{ $ketenagaan->instansi ?? old('instansi') }}">

                @error('instansi')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="keterangan">Keterangan</label>
                <input type="text" name="keterangan" id="keterangan" class="form-control" value="{{ $ketenagaan->keterangan ?? old('keterangan') }}">

                @error('keterangan')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="kategori">Kategori Ketenagaan</label>
                <select  class="form-control"  name="kategori" id="">
                    <option value="" disabled selected>-- Kategori Ketenagaan --</option>
                    <option value="Tenaga Pendidikan">Tenaga Pendidikan</option>
                    <option value="Tenaga Kependidikan">Tenaga Kependidikan</option>
                </select>
                @error('kategori')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>

            <div class="form-group">
                <label for="alamat">Alamat</label>
                <textarea name="alamat" id="alamat" rows="4" class="form-control">{{ $ketenagaan->alamat ?? old('alamat') }}</textarea>

                @error('alamat')
                <small class="text-danger">{{ $message }}</small>
                @enderror
            </div>

            
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Kirim</button>
            </div>
        </form>
    </div>
</div>
@stop