<div class="site-mobile-menu site-navbar-target">
    <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
        </div>
    </div>
    <div class="site-mobile-menu-body"></div>
</div>
<div class="py-2 bg-light">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-9 d-none d-lg-block">
                <a href="{{ route('chat') }}" class="small mr-3"><span class="icon-phone2 mr-2"></span>+62{{ App\Models\LokasiKontak::find(1)->telepon ?? '-' }}</a>
                <a href="#" class="small mr-3"><span class="icon-envelope-o mr-2"></span>{{ App\Models\LokasiKontak::find(1)->email ?? '-' }}</a>
                <a href="{{ route('lokasi') }}" class="small mr-3"><span class="fas fa-map-marker-alt mr-2"></span>Alamat</a>
                <a href="{{ route('faq') }}" class="small mr-3"><span class="fa fa-question mr-2"></span>FaQ</a>
            </div>
            <div class="col-lg-3 text-right">
                <a href="/login" class="small mr-3"><span class="icon-unlock-alt"></span> Log In</a>
                <a href="https://absensi.man1bitung.sch.id/" class="small btn btn-primary px-4 py-2 rounded-0"><span class="icon-users"></span>Absensi</a>
            </div>
        </div>
    </div>
</div>

<nav class="navbar navbar-expand-lg navbar-light sticky-top" style="background-color: #f4f4f4;">
    <img class="navbar-brand" src="{{ asset('storage/' . App\Models\Logo::find(1)->gambar ?? '') }}" width="50" class="img-fluid rounded m-3" id="preview"><br>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbarToggler10"
        aria-controls="myNavbarToggler10" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="myNavbarToggler10">
        <ul class="navbar-nav mx-auto">
            <li class="nav-item {{ request()->is('/') || request()->is('home') ? 'active' : '' }}">
                <a class="nav-link " href="{{ route('home') }}">Beranda</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle {{ request()->is('profile*') ? 'active' : '' }}" href="{{ route('profile') }}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Profile
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ route('profile') }}#sambutan">Sambutan</a>
                  <a class="dropdown-item" href="{{ route('profile') }}#sejarah">Sejarah</a>
                  <a class="dropdown-item" href="{{ route('profile') }}#visi-misi-tujuan">Visi, Misi dan Tujuan</a>
                  <a class="dropdown-item" href="{{ route('profile') }}#budaya">Budaya</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('profile') }}#jurusan">Jurusan</a>
                  <a class="dropdown-item" href="{{ route('profile') }}#sarana-prasarana">Sarana dan Prasarana</a>
                  <a class="dropdown-item" href="{{ route('profile') }}#prestasi">Prestasi</a>
                  <a class="dropdown-item" href="{{ route('profile') }}#ekstrakurikuler">Ekstrakurikuler</a>
                </div>
              </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle {{ request()->is('ketenagaan*') ? 'active' : '' }}" href="{{ route('ketenagaan') }}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Ketenagaan
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ route('ketenagaan') }}#tenaga-pendidikan">Tenaga Pendidikan</a>
                  <a class="dropdown-item" href="{{ route('ketenagaan') }}#tenaga-kependidikan">Tenaga Kependidikan</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle {{ request()->is('akademik*') ? 'active' : '' }}" href="{{ route('akademik') }}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Akademik
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ route('akademik') }}#struktur-kurikulum">Struktur Kurikulum</a>
                </div>
            </li>
            <li class="nav-item {{ request()->is('blog*') ? 'active' : '' }}">
                <a class="nav-link " href="{{ route('blog') }}">Berita</a>
            </li>
            <li class="nav-item {{ request()->is('daftar*') ? 'active' : '' }}">
                <a class="nav-link " href="{{ route('daftar.hal.satu') }}">Pendaftaran</a>
            </li>
        </ul>
        <ul class="navbar-nav sm-icons mr-0">
            @foreach(App\Models\MediaSosial::where('tampil', 'Aktif')->get() as $mediasosial)
                <li class="nav-item"><a class="nav-link" href="{{ $mediasosial->tautan }}"><i class="{{ $mediasosial->ikon }}"></i></a></li>
            @endforeach
        </ul>
    </div>
</nav>