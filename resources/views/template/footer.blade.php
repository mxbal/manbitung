<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <p class="mb-4"> 
                    <img src="{{ asset('storage/' . App\Models\Logo::find(1)->gambar ?? '') }}" width="100" class="img-fluid rounded m-3" id="preview"><br>
                </p>
                {{-- <p>{{  App\Models\Logo::find(1)->deskripsi ?? ''}}</p> --}}
            </div>
            <div class="col-lg-3">
                <h3 class="footer-heading"><span>Halaman</span></h3>
                <ul class="list-unstyled">
                    <li><a href="{{ route('home') }}">Beranda</a></li>
                    <li><a href="{{ route('profile') }}">Profile</a></li>
                    <li><a href="{{ route('blog') }}">Berita</a></li>
                </ul>
            </div>
          
            <div class="col-lg-3">
                <h3 class="footer-heading"><span>Kontak</span></h3>
                <ul class="list-unstyled">
                    <li><a href="{{ route('chat') }}">+62{{ App\Models\LokasiKontak::find(1)->telepon ?? '-' }}</a></li>
                    <li><a href="#">{{ App\Models\LokasiKontak::find(1)->email ?? '-' }}</a></li>
                    <li><a href="{{ route('lokasi') }}">Alamat</a></li>
                    <li><a href="{{ route('faq') }}">FaQ</a></li>
                </ul>
              
            </div>

            <div class="col-lg-3">
                <h3 class="footer-heading"><span>Sosial Media</span></h3>
                <ul class="list-unstyled">
                    @foreach(App\Models\MediaSosial::get() as $mediasosial)
                    <li>
                        <a href="{{ $mediasosial->tautan }}"><span class="{{ $mediasosial->ikon }}"></span></a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="copyright">
                    <p>
                        Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved | MAN 1 Bitung
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>