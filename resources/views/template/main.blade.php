<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{ App\Models\Logo::find(1)->label }}</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Madrasah Aliyah Negeri 1 Bitung MAN Bitung MAN 1 KOTA sekolah MA">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/') }}front/fonts/icomoon/style.css">

    <link rel="stylesheet" href="{{ asset('/') }}front/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}front/css/jquery-ui.css">
    <link rel="stylesheet" href="{{ asset('/') }}front/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}front/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}front/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}front/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}front/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="{{ asset('/') }}front/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="{{ asset('/') }}front/css/aos.css">

    {{-- Thumbnail --}}
  

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('/') }}front/css/jquery.mb.YTPlayer.min.css" media="all" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('/') }}front/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>

</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
    <div id="myDiv"></div>
    <div id="myButton"></div>
    <div class="site-wrap">
        @include('template.navbar')
        @yield('content')
        @include('template.footer')
    </div>
    <!-- .site-wrap -->

    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#51be78" />
        </svg>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="{{ asset('/') }}front/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('/') }}front/js/jquery-migrate-3.0.1.min.js"></script>
    <script src="{{ asset('/') }}front/js/jquery-ui.js"></script>
    <script src="{{ asset('/') }}front/js/popper.min.js"></script>
    <script src="{{ asset('/') }}front/js/bootstrap.min.js"></script>
    <script src="{{ asset('/') }}front/js/owl.carousel.min.js"></script>
    <script src="{{ asset('/') }}front/js/jquery.stellar.min.js"></script>
    <script src="{{ asset('/') }}front/js/jquery.countdown.min.js"></script>
    <script src="{{ asset('/') }}front/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ asset('/') }}front/js/jquery.easing.1.3.js"></script>
    <script src="{{ asset('/') }}front/js/aos.js"></script>
    <script src="{{ asset('/') }}front/js/jquery.fancybox.min.js"></script>
    <script src="{{ asset('/') }}front/js/jquery.sticky.js"></script>
    <script src="{{ asset('/') }}front/js/jquery.mb.YTPlayer.min.js"></script>
    <script src="{{ asset('/') }}front/js/main.js"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>

    <script type="text/javascript" src="{{ asset('/') }}front/js/jquery-3.3.1.min.js"></script>
    {{-- <script type="text/javascript" src="{{ asset('/') }}floating-wpp.min.js"></script>
    <script type="text/javascript" src="{{ asset('/') }}venom-button.min.js"></script> --}}
    <script type="text/javascript">
        // $(function () {
        //     $('#myDiv').floatingWhatsApp({
        //         phone: {{ App\Models\LokasiKontak::find(1)->telepon ?? '-' }},
        //         popupMessage: 'Bagaimana Kami dapat Membantumu?',
        //         showPopup: true
        //     });
        // });
        //
    </script>
    <script type="text/javascript">
        //     $('#myDiv').venomButton({
        //     phone: '5521990000000',
        //     message: "I'd like to order a pizza",
        //     chatMessage: 'Hi there 👋<br><br>How can I help you?',
        //     showPopup: true,
        //     avatar: './assets/avatar.jpg',
        //     position: "right",
        //     headerColor: '#033E8C',
        //     buttonColor: '#033E8C',
        //     linkButton: false,
        //     showOnIE: false,
        //     nameClient: "Joe Dutra",
        //     headerTitle: 'gitCommercial Sales',
        // });
        //
    </script>

</body>

</html>
