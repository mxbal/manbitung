@extends('template.main')

@section('content')

<div class="site-section ftco-subscribe-1 site-blocks-cover pb-4">
    <div class="container">
      <div class="row align-items-end">
        <div class="col-lg-7">
          <h2 class="mb-0">Daftar Siswa Diterima</h2>
        </div>
      </div>
    </div>
  </div> 

<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="index.html">Home</a>
    <span class="mx-3 icon-keyboard_arrow_right"></span>
    <span class="current">Daftar Siswa Diterima</span>
  </div>
</div>

<div class="site-section">
  <div class="container">
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">NISN</th>
          <th scope="col">Nama</th>
          <th scope="col">Asal Sekolah</th>
        </tr>
      </thead>
      <tbody>
        @foreach (App\Models\CalonSiswa::where('status_siswa', 'Diterima')->get() as $siswa)
        <tr>
          <th scope="row">{{ $loop->iteration }}</th>
          <td>{{ $siswa->nisn }}</td>
          <td>{{ $siswa->nama }}</td>
          <td>{{ $siswa->asal_sekolah }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>

</div>



@endsection