@extends('template.main')

@section('content')

<div class="site-section ftco-subscribe-1 site-blocks-cover pb-4">
    <div class="container">
      <div class="row align-items-end">
        <div class="col-lg-7">
          <h2 class="mb-0">Frequently Asked Questions</h2>
        </div>
      </div>
    </div>
  </div> 

<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="index.html">Home</a>
    <span class="mx-3 icon-keyboard_arrow_right"></span>
    <span class="current">Frequently Asked Questions</span>
  </div>
</div>
<div class="site-section">
    <div class="container">
        <div class="card">
            <div class="card-body">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
         @foreach(App\Models\Pertanyaan::get() as $pertanyaan)
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
              <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                {{ $pertanyaan->pertanyaan }}
              </a>
            </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                {!! $pertanyaan->jawaban !!}
              </div>
            </div>
          </div>
          @endforeach
         
        </div>
      </div>
        </div>
    </div>
</div>
@endsection