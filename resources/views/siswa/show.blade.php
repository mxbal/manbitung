@extends('layouts.master', ['title' => 'Siswa', 'first' => 'Siswa', 'second' => 'Detail Siswa'])

@section('content')
<div class="panel panel-inverse">
    <div class="panel-heading">
        <h4 class="panel-title">Siswa</h4>
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
    </div>
    <div class="panel-body">
    <table>
        <tr>
            <td>Jalur PPDB</td>
            <td>:</td>
            <td>{{ $siswa->jalur_ppdb }}</td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{ $siswa->nama }}</td>
        </tr>
        <tr>
            <td>Kewarganegaraan</td>
            <td>:</td>
            <td>{{ $siswa->kewarganegaraan }}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>:</td>
            <td>{{ $siswa->email }}</td>
        </tr>
        <tr>
            <td>Nomor KK</td>
            <td>:</td>
            <td>{{ $siswa->nomor_kk }}</td>
        </tr>
        <tr>
            <td>Jurusan 1</td>
            <td>:</td>
            <td>{{ explode('-',$siswa->jurusan)[0] }}</td>
        </tr>
        <tr>
            <td>Jurusan 2</td>
            <td>:</td>
            <td>{{ explode('-',$siswa->jurusan)[1] }}</td>
        </tr>
        <tr>
            <td>Tempat Lahir</td>
            <td>:</td>
            <td>{{ $siswa->tempat_lahir }}</td>
        </tr>
        <tr>
            <td>Tanggal Lahir</td>
            <td>:</td>
            <td>{{ $siswa->tanggal_lahir }}</td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td>:</td>
            <td>{{ $siswa->jenis_kelamin }}</td>
        </tr>
        <tr>
            <td>Agama</td>
            <td>:</td>
            <td>{{ $siswa->agama }}</td>
        </tr>
        <tr>
            <td>Status Anak</td>
            <td>:</td>
            <td>{{ $siswa->status_anak }}</td>
        </tr>
        <tr>
            <td>Anak Ke</td>
            <td>:</td>
            <td>{{ $siswa->anak_ke }}</td>
        </tr>
        <tr>
            <td>Jumlah Saudara</td>
            <td>:</td>
            <td>{{ $siswa->jumlah_saudara }}</td>
        </tr>
        <tr>
            <td>Tinggi Badan</td>
            <td>:</td>
            <td>{{ $siswa->tinggi_badan }}</td>
        </tr>
        <tr>
            <td>Berat badan</td>
            <td>:</td>
            <td>{{ $siswa->berat_badan }}</td>
        </tr>
        <tr>
            <td>Golongan darah</td>
            <td>:</td>
            <td>{{ $siswa->golongan_darah }}</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td>{{ $siswa->alamat }}</td>
        </tr>
        <tr>
            <td>RT</td>
            <td>:</td>
            <td>{{ $siswa->rt }}</td>
        </tr>
        <tr>
            <td>RW</td>
            <td>:</td>
            <td>{{ $siswa->rw }}</td>
        </tr>
        <tr>
            <td>Kelurahan</td>
            <td>:</td>
            <td>{{ $siswa->kelurahan }}</td>
        </tr>
        <tr>
            <td>Kecamatan</td>
            <td>:</td>
            <td>{{ $siswa->kecamatan }}</td>
        </tr>
        <tr>
            <td>Domisili</td>
            <td>:</td>
            <td>{{ $siswa->domisili }}</td>
        </tr>
        <tr>
            <td>Provinsi</td>
            <td>:</td>
            <td>{{ $siswa->provinsi }}</td>
        </tr>
        <tr>
            <td>Kode Pos</td>
            <td>:</td>
            <td>{{ $siswa->kode_pos }}</td>
        </tr>
        <tr>
            <td>Jarak Kesekolah</td>
            <td>:</td>
            <td>{{ $siswa->jarak_kesekolah }}</td>
        </tr>
        <tr>
            <td>Transportasi Kesekolah</td>
            <td>:</td>
            <td>{{ $siswa->transportasi_kesekolah }}</td>
        </tr>
        <tr>
            <td>Handphone</td>
            <td>:</td>
            <td>{{ $siswa->telp }}</td>
        </tr>
        <tr>
            <td>Penerima PKH</td>
            <td>:</td>
            <td>{{ $siswa->penerima_pkh }}</td>
        </tr>
        <tr>
            <td>Asal Sekolah</td>
            <td>:</td>
            <td>{{ $siswa->asal_sekolah }}</td>
        </tr>
        <tr>
            <td>Tahun Lulus</td>
            <td>:</td>
            <td>{{ $siswa->tahun_lulus }}</td>
        </tr>
        <tr>
            <td>NISN</td>
            <td>:</td>
            <td>{{ $siswa->nisn }}</td>
        </tr>
        <tr>
            <td>Nama Ayah</td>
            <td>:</td>
            <td>{{ $siswa->nama_ayah }}</td>
        </tr>
        <tr>
            <td>NIK Ayah</td>
            <td>:</td>
            <td>{{ $siswa->nik_ayah }}</td>
        </tr>
        <tr>
            <td>Alamat Ayah</td>
            <td>:</td>
            <td>{{ $siswa->nik_ayah }}</td>
        </tr>
        <tr>
            <td>NIK Ayah</td>
            <td>:</td>
            <td>{{ $siswa->alamat_ayah }}</td>
        </tr>
        <tr>
            <td>Tempat Lahir Ayah</td>
            <td>:</td>
            <td>{{ $siswa->tempat_lahir_ayah }}</td>
        </tr>
        <tr>
            <td>Tanggal Lahir Ayah</td>
            <td>:</td>
            <td>{{ $siswa->tanggal_lahir_ayah }}</td>
        </tr>
        <tr>
            <td>Agama Ayah</td>
            <td>:</td>
            <td>{{ $siswa->agama_ayah }}</td>
        </tr>
        <tr>
            <td>Pendidikan Ayah</td>
            <td>:</td>
            <td>{{ $siswa->pendidikan_ayah }}</td>
        </tr>
        <tr>
            <td>Pekerjaan Ayah</td>
            <td>:</td>
            <td>{{ $siswa->pekerjaan_ayah }}</td>
        </tr>
        <tr>
            <td>Penghasilan Ayah</td>
            <td>:</td>
            <td>{{ $siswa->penghasilan_ayah }}</td>
        </tr>
        <tr>
            <td>Telepon Ayah</td>
            <td>:</td>
            <td>{{ $siswa->telp_ayah }}</td>
        </tr>
        <tr>
            <td>Nama Ibu</td>
            <td>:</td>
            <td>{{ $siswa->nama_ibu }}</td>
        </tr>
        <tr>
            <td>NIK Ibu</td>
            <td>:</td>
            <td>{{ $siswa->nik_ibu }}</td>
        </tr>
        <tr>
            <td>Alamat Ibu</td>
            <td>:</td>
            <td>{{ $siswa->nik_ibu }}</td>
        </tr>
        <tr>
            <td>NIK Ibu</td>
            <td>:</td>
            <td>{{ $siswa->alamat_ibu }}</td>
        </tr>
        <tr>
            <td>Tempat Lahir Ibu</td>
            <td>:</td>
            <td>{{ $siswa->tempat_lahir_ibu }}</td>
        </tr>
        <tr>
            <td>Tanggal Lahir Ibu</td>
            <td>:</td>
            <td>{{ $siswa->tanggal_lahir_ibu }}</td>
        </tr>
        <tr>
            <td>Agama Ibu</td>
            <td>:</td>
            <td>{{ $siswa->agama_ibu }}</td>
        </tr>
        <tr>
            <td>Pendidikan Ibu</td>
            <td>:</td>
            <td>{{ $siswa->pendidikan_ibu }}</td>
        </tr>
        <tr>
            <td>Pekerjaan Ibu</td>
            <td>:</td>
            <td>{{ $siswa->pekerjaan_ibu }}</td>
        </tr>
        <tr>
            <td>Penghasilan Ibu</td>
            <td>:</td>
            <td>{{ $siswa->penghasilan_ibu }}</td>
        </tr>
        <tr>
            <td>Telepon Ibu</td>
            <td>:</td>
            <td>{{ $siswa->telp_ibu }}</td>
        </tr>
        <tr>
            <td>Nama Wali</td>
            <td>:</td>
            <td>{{ $siswa->nama_wali }}</td>
        </tr>
        <tr>
            <td>NIK Wali</td>
            <td>:</td>
            <td>{{ $siswa->nik_wali }}</td>
        </tr>
        <tr>
            <td>Alamat Wali</td>
            <td>:</td>
            <td>{{ $siswa->nik_wali }}</td>
        </tr>
        <tr>
            <td>NIK Wali</td>
            <td>:</td>
            <td>{{ $siswa->alamat_wali }}</td>
        </tr>
        <tr>
            <td>Tempat Lahir Wali</td>
            <td>:</td>
            <td>{{ $siswa->tempat_lahir_wali }}</td>
        </tr>
        <tr>
            <td>Tanggal Lahir Wali</td>
            <td>:</td>
            <td>{{ $siswa->tanggal_lahir_wali }}</td>
        </tr>
        <tr>
            <td>Agama Wali</td>
            <td>:</td>
            <td>{{ $siswa->agama_wali }}</td>
        </tr>
        <tr>
            <td>Pendidikan Wali</td>
            <td>:</td>
            <td>{{ $siswa->pendidikan_wali }}</td>
        </tr>
        <tr>
            <td>Pekerjaan Wali</td>
            <td>:</td>
            <td>{{ $siswa->pekerjaan_wali }}</td>
        </tr>
        <tr>
            <td>Penghasilan Wali</td>
            <td>:</td>
            <td>{{ $siswa->penghasilan_wali }}</td>
        </tr>
        <tr>
            <td>Telepon Wali</td>
            <td>:</td>
            <td>{{ $siswa->telp_wali }}</td>
        </tr>
     </table>
    </div>
</div>
@stop