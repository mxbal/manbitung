@extends('template.main')

@section('content')

<div class="site-section ftco-subscribe-1 site-blocks-cover pb-4">
    <div class="container">
      <div class="row align-items-end">
        <div class="col-lg-7">
          <h2 class="mb-0">Alamat</h2>
        </div>
      </div>
    </div>
  </div> 

<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="{{ route('home') }}">Beranda</a>
    <span class="mx-3 icon-keyboard_arrow_right"></span>
    <span class="current">Alamat</span>
  </div>
</div>
     
<div class="site-section">
  <div class="container">
    <div class="row">
      <div class="col-md-6 mb-5 mb-md-0">
        <h2 class="h3 mb-3 text-black">Alamat</h2>
        {!! App\Models\LokasiKontak::find(1)->alamat ?? '-' !!}
      </div>
    </div>
    <div class="row">
      <iframe width="100%" height="300" src="https://maps.google.com/maps?q={{  App\Models\LokasiKontak::find(1)->lokasi_map ?? '' }}&output=embed" class="media-left"></iframe>
    </div>
  </div>
</div>
        
</div>

@endsection