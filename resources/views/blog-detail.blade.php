@extends('template.main')

@section('content')

<head>
  <meta property='og:title' content='{{ $blog->judul }}' />
  <meta property='og:image' content='{{ asset('storage/' . $blog->gambar) ?? '' }}' />
  <meta property='og:description' content='{{ $blog->judul }}' />
  <meta property='og:image:width' content='1200' />
  <meta property='og:image:height' content='630' />
  <meta property="og:type" content='websiteswwsyttt' />
</head>

<link itemprop="{{ asset('storage/' . $blog->gambar) ?? '' }}" href="{{ asset('storage/' . $blog->gambar) ?? '' }}">
<span itemprop="{{ asset('storage/' . $blog->gambar) ?? '' }}" itemscope itemtype="{{ asset('storage/' . $blog->gambar) ?? '' }}">
    <link itemprop="url" href="{{ asset('storage/' . $blog->gambar) ?? '' }}">
</span>

<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="{{ route('home') }}">Beranda</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <span class="current">Blog</span>
  </div>
  <div class="py-5 text-center" >
    <img class="d-block mx-auto mb-4 img-fluid" height="350px" alt="{{ $blog->gambar }}" src="{{ asset('storage/' . $blog->gambar ?? ''  ) }}">
  </div>

  <div class="site-section">
    <div class="container">
      <div class="blog-post">
        <h2 class="blog-post-title">{{ $blog->judul }}</h2>
        <p class="blog-post-meta">{{ $blog->created_at->diffForHumans() }}</p>
          {!! $blog->deskripsi !!}
      </div>

    </div>
</div>
</div>
@endsection