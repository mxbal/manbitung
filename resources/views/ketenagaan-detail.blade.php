@extends('template.main')

@section('content')

<div class="custom-breadcrumns border-bottom">
    <div class="container">
      <a href="{{ route('home') }}">Beranda</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <span class="current">Ketenagaan</span>
    </div>
  </div>
<div class="site-section">
  <div class="container" style="width: 80%;">
    <div class="card text-center" >
      <img src="{{ asset('storage/' . $ketenagaan->gambar) }}" class="card-img-top" alt="...">
      <div class="card-body">
        <p class="card-text">
          <table>
            <tr>
              <td>Nama</td>
              <td> : </td>
              <td>{{ $ketenagaan->nama }}</td>
            </tr>
            <tr>
              <td>NIP</td>
              <td> : </td>
              <td>{{ $ketenagaan->nip }}</td>
            </tr>
            <tr>
              <td>NUPTK</td>
              <td> : </td>
              <td>{{ $ketenagaan->nuptk }}</td>
            </tr>
            <tr>
              <td>Status</td>
              <td> : </td>
              <td>{{ $ketenagaan->status }}</td>
            </tr>
            <tr>
              <td>Pendidikan</td>
              <td> : </td>
              <td>{{ $ketenagaan->pendidikan }}</td>
            </tr>
            <tr>
              <td>Jabatan</td>
              <td> : </td>
              <td>{{ $ketenagaan->jabatan }}</td>
            </tr>
            <tr>
              <td>Jenis Kelamin</td>
              <td> : </td>
              <td>{{ $ketenagaan->jenis_kelamin }}</td>
            </tr>
            <tr>
              <td>Tempat Lahir</td>
              <td> : </td>
              <td>{{ $ketenagaan->tempat_lahir }}</td>
            </tr>
            <tr>
              <td>Tanggal Lahir</td>
              <td> : </td>
              <td>{{ $ketenagaan->tanggal_lahir }}</td>
            </tr>
            <tr>
              <td>Kode Pos</td>
              <td> : </td>
              <td>{{ $ketenagaan->kode_pos }}</td>
            </tr>
            <tr>
              <td>Telepon</td>
              <td> : </td>
              <td>{{ $ketenagaan->telepon }}</td>
            </tr>
            <tr>
              <td>Instansi</td>
              <td> : </td>
              <td>{{ $ketenagaan->instansi }}</td>
            </tr>
            <tr>
              <td>Keterangan</td>
              <td> : </td>
              <td>{{ $ketenagaan->keterangan }}</td>
            </tr>
            <tr>
              <td>Alamat</td>
              <td> : </td>
              <td>{{ $ketenagaan->alamat }}</td>
            </tr>
          </table>
        </p>
      </div>
     
    </div>
  </div>
</div>
@endsection