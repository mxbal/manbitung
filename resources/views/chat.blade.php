@extends('template.main')

@section('content')

<div class="site-section ftco-subscribe-1 site-blocks-cover pb-4">
    <div class="container">
      <div class="row align-items-end">
        <div class="col-lg-7">
          <h2 class="mb-0">Chat Admin</h2>
        </div>
      </div>
    </div>
  </div> 

<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="index.html">Home</a>
    <span class="mx-3 icon-keyboard_arrow_right"></span>
    <span class="current">Ajukan Pertanyaan</span>
  </div>
</div>
      <form action="{{ route('chat.post') }}" method="POST">
        @csrf
        <div class="site-section">
        <div class="container">
        <div class="card">
            <div class="card-header">Ajukan Pertanyaan</div>

            <div class="card-body">
              <div class="form-group">
                  <label for="nama">Nama</label>
                    <input type="text"  value="{{ old('nama') }}" class="form-control" name="nama">
                    @error('nama')
                      <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
                <div class="form-group">
                  <label for="pesan">Pesan</label>
                  <textarea type="text"  class="form-control" name="pesan">{{ old('pesan') }}</textarea>
                </div>

            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary">Kirim</button>
            </div>
            
        </div>
        </div>
        </div>
    </form>

    </div>
</div>



@endsection