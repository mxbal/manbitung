@extends('template.main')

@section('content')

<div class="custom-breadcrumns border-bottom">
  <div class="container">
    <a href="#">Home</a>
      <span class="mx-3 icon-keyboard_arrow_right"></span>
      <span class="current">Blog</span>
  </div>
  <div class="py-5 text-center">
    <img class="d-block mx-auto mb-4" style="height: 400px" alt="{{ $blog->title }}" src="{{ asset('storage/' . $blog->gambar ?? ''  ) }}">
  </div>

  <div class="site-section">
    <div class="container">
      <div class="blog-post">
        <h2 class="blog-post-title">{{ $blog->judul }}</h2>
        <p class="blog-post-meta">{{ $blog->created_at->diffForHumans() }}</p>
          {!! $blog->deskripsi !!}
      </div>

    </div>
</div>
</div>
@endsection