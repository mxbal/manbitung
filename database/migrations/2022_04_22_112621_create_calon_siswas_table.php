<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalonSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calon_siswas', function (Blueprint $table) {
            $table->id();
            $table->string('jalur_ppdb');
            $table->string('gelombang_pendaftaran');
            $table->date('tanggal_daftar');
            $table->string('nama');
            $table->string('nama_panggilan');
            $table->string('kewarganegaraan');
            $table->string('bahasa')->nullable();
            $table->string('alasan')->nullable();
            $table->string('hobi')->nullable();
            $table->string('cita_cita')->nullable();
            $table->string('email');
            $table->string('no_telepon');
            $table->integer('nomor_kk');
            $table->string('jurusan');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('jenis_kelamin');
            $table->string('penerima_pkh');
            $table->integer('nisn');
            $table->string('agama');
            $table->string('status_anak');
            $table->integer('anak_ke');
            $table->integer('jumlah_saudara');
            $table->integer('jumlah_saudara_kandung');
            $table->integer('jumlah_saudara_tiri');
            $table->integer('jumlah_saudara_angkat');
            $table->string('tinggi_badan');
            $table->string('asal_sekolah');
            $table->year('tahun_lulus');
            $table->string('berat_badan');
            $table->string('cacat_badan')->nullable();
            $table->string('penyakit_bawaan')->nullable();
            $table->string('pernah_sakit')->nullable();
            $table->string('nama_penyakit')->nullable();
            $table->date('tanggal_sakit')->nullable();
            $table->string('lama_sakit')->nullable();
            $table->string('golongan_darah');
            $table->string('alamat');
            $table->string('rt');
            $table->string('rw');
            $table->string('kecamatan');
            $table->string('kelurahan');
            $table->string('provinsi');
            $table->string('kode_pos');
            $table->string('domisili');
            $table->string('jarak_kesekolah');
            $table->string('transportasi_kesekolah')->nullable();
            $table->string('alamat_sekolah_asal');
            $table->string('nama_ayah')->nullable();
            $table->integer('nik_ayah')->nullable();
            $table->string('alamat_ayah')->nullable();
            $table->date('tanggal_lahir_ayah')->nullable();
            $table->string('tempat_lahir_ayah')->nullable();
            $table->string('agama_ayah')->nullable();
            $table->string('pendidikan_ayah')->nullable();
            $table->string('pekerjaan_ayah')->nullable();
            $table->string('penghasilan_ayah')->nullable();
            $table->string('telp_ayah')->nullable();
            $table->string('nama_ibu')->nullable();
            $table->integer('nik_ibu')->nullable();
            $table->string('alamat_ibu')->nullable();
            $table->string('tempat_lahir_ibu')->nullable();
            $table->string('agama_ibu')->nullable();
            $table->string('pendidikan_ibu')->nullable();
            $table->string('pekerjaan_ibu')->nullable();
            $table->string('penghasilan_ibu')->nullable();
            $table->date('tanggal_lahir_ibu')->nullable();
            $table->string('telp_ibu')->nullable();
            $table->string('nama_wali')->nullable();
            $table->integer('nik_wali')->nullable();
            $table->string('alamat_wali')->nullable();
            $table->string('tempat_lahir_wali')->nullable();
            $table->string('agama_wali')->nullable();
            $table->string('pendidikan_wali')->nullable();
            $table->date('tanggal_lahir_wali')->nullable();
            $table->string('pekerjaan_wali')->nullable();
            $table->string('penghasilan_wali')->nullable();
            $table->string('telp_wali')->nullable();
            $table->string('status_siswa');
            $table->string('image')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calon_siswas');
    }
}
