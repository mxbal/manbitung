<?php

namespace Database\Seeders;

use App\Models\Logo;
use App\Models\User;
use App\Models\About;
use App\Models\LokasiKontak;
use App\Models\Sejarah;
use App\Models\Budaya;
use App\Models\Sambutan;
use App\Models\StrukturKurikulum;
use App\Models\VisiMisi;
use App\Models\TujuanSlogan;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'admin',
            'name' => 'Admin Man 1 Bitung',
            'password' => bcrypt('secret'),
            'level' => 'Admin',
        ]);

        Sambutan::create([
            'judul' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam, asperiores.',
            'deskripsi' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus, dolore! Cupiditate dignissimos quidem sed, eligendi eaque veritatis assumenda ipsam aliquam dolorum omnis. Modi consectetur, tenetur excepturi nesciunt pariatur repudiandae quod fugiat distinctio exercitationem nisi in cum quaerat, quos numquam praesentium nobis adipisci omnis reiciendis animi? Earum deserunt rem natus quis.',
        ]);

        Budaya::create([
            'judul' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam, asperiores.',
            'deskripsi' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus, dolore! Cupiditate dignissimos quidem sed, eligendi eaque veritatis assumenda ipsam aliquam dolorum omnis. Modi consectetur, tenetur excepturi nesciunt pariatur repudiandae quod fugiat distinctio exercitationem nisi in cum quaerat, quos numquam praesentium nobis adipisci omnis reiciendis animi? Earum deserunt rem natus quis.',
        ]);

        // About::create([
        //     'judul' => 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis ipsum beatae temporibus necessitatibus quaerat veritatis cumque harum alias sint, molestiae deleniti, consectetur cupiditate animi! Hic recusandae numquam assumenda maxime esse.',
        //     'deskripsi' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga, fugiat ipsa temporibus omnis nulla ipsum, libero accusamus sit sequi unde nihil beatae excepturi quaerat id deleniti quas ea dolore dolor laboriosam blanditiis, illo architecto? Eos alias temporibus commodi, eaque ratione sint maiores nisi dolore. Vel modi culpa delectus ipsam, ab alias sequi dicta cum atque, ad porro similique sed laudantium esse soluta, iusto fugiat necessitatibus ducimus ut saepe facere? Id error illo ipsa praesentium deleniti. Minus veniam temporibus delectus dolores sapiente necessitatibus, sint est repellat tempore id. Suscipit natus ipsa iusto saepe. Excepturi optio eos doloremque, voluptas amet, iste placeat vel dolore, possimus nihil incidunt totam ipsum? Maxime fuga, dolores perspiciatis temporibus nesciunt doloribus ipsum sint! Maxime non ullam temporibus rerum et voluptatem minima veniam esse modi ex. Molestias ut eveniet, ad velit officiis est perspiciatis impedit accusantium dolor. Voluptatum quas modi perspiciatis vel, magni aliquid. Molestiae saepe corporis est officiis dolorem! Repellat voluptatem mollitia non alias dolor voluptatum, tempore ut, atque impedit voluptas repellendus quos perferendis, provident ab modi reiciendis reprehenderit. Recusandae, esse debitis harum inventore alias modi. Quod nam sapiente magnam. Modi perferendis, et exercitationem repudiandae praesentium sint, quisquam enim cumque ab animi, voluptates odit consequatur incidunt vel.',
        // ]);

        TujuanSlogan::create([
            'judul_slogan' => 'Lorem ipsum',
            'judul_tujuan' => 'Lorem ipsum dolor sit',
            'tujuan' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga, fugiat ipsa temporibus omnis nulla ipsum, libero accusamus sit sequi unde nihil beatae excepturi quaerat id deleniti quas ea dolore dolor laboriosam blanditiis, illo architecto? Eos alias temporibus commodi, eaque ratione sint maiores nisi dolore. Vel modi culpa delectus ipsam, ab alias sequi dicta cum atque, ad porro similique sed laudantium esse soluta, iusto fugiat necessitatibus ducimus ut saepe facere? Id error illo ipsa praesentium deleniti. Minus veniam temporibus delectus dolores sapiente necessitatibus, sint est repellat tempore id. Suscipit natus ipsa iusto saepe. Excepturi optio eos doloremque, voluptas amet, iste placeat vel dolore, possimus nihil incidunt totam ipsum? Maxime fuga, dolores perspiciatis temporibus nesciunt doloribus ipsum sint! Maxime non ullam temporibus rerum et voluptatem minima veniam esse modi ex. Molestias ut eveniet, ad velit officiis est perspiciatis impedit accusantium dolor. Voluptatum quas modi perspiciatis vel, magni aliquid. Molestiae saepe corporis est officiis dolorem! Repellat voluptatem mollitia non alias dolor voluptatum, tempore ut, atque impedit voluptas repellendus quos perferendis, provident ab modi reiciendis reprehenderit. Recusandae, esse debitis harum inventore alias modi. Quod nam sapiente magnam. Modi perferendis, et exercitationem repudiandae praesentium sint, quisquam enim cumque ab animi, voluptates odit consequatur incidunt vel.',
            'slogan' => 'Lorem ipsum dolor sit amet consectetur',
            // 'gambar' => 'Lorem ipsum dolor sit amet consectetur',
        ]);

        Sejarah::create([
            'judul' => 'Lorem ipsum dolor sit, Perferendis ipsum beatae temporibus necessitatibus quaerat veritatis cumque harum alias sint, molestiae deleniti, consectetur cupiditate animi! Hic recusandae numquam assumenda maxime esse.',
            'deskripsi' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga, fugiat ipsa temporibus omnis nulla ipsum, libero accusamus sit sequi unde nihil beatae excepturi quaerat id deleniti quas ea dolore dolor laboriosam blanditiis, illo architecto? Eos alias temporibus commodi, eaque ratione sint maiores nisi dolore. Vel modi culpa delectus ipsam, ab alias sequi dicta cum atque, ad porro similique sed laudantium esse soluta, iusto fugiat necessitatibus ducimus ut saepe facere? Id error illo ipsa praesentium deleniti. Minus veniam temporibus delectus dolores sapiente necessitatibus, sint est repellat tempore id. Suscipit natus ipsa iusto saepe. Excepturi optio eos doloremque, voluptas amet, iste placeat vel dolore, possimus nihil incidunt totam ipsum? Maxime fuga, dolores perspiciatis temporibus nesciunt doloribus ipsum sint! Maxime non ullam temporibus rerum et voluptatem minima veniam esse modi ex. Molestias ut eveniet, ad velit officiis est perspiciatis impedit accusantium dolor. Voluptatum quas modi perspiciatis vel, magni aliquid. Molestiae saepe corporis est officiis dolorem! Repellat voluptatem mollitia non alias dolor voluptatum, tempore ut, atque impedit voluptas repellendus quos perferendis, provident ab modi reiciendis reprehenderit. Recusandae, esse debitis harum inventore alias modi. Quod nam sapiente magnam. Modi perferendis, et exercitationem repudiandae praesentium sint, quisquam enim cumque ab animi, voluptates odit consequatur incidunt vel.',
            'gambar' => 'default.jpg',
        ]);

        StrukturKurikulum::create([
            'judul' => 'Lorem ipsum dolor sit, Perferendis ipsum beatae temporibus necessitatibus quaerat veritatis cumque harum alias sint, molestiae deleniti, consectetur cupiditate animi! Hic recusandae numquam assumenda maxime esse.',
            'deskripsi' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga, fugiat ipsa temporibus omnis nulla ipsum, libero accusamus sit sequi unde nihil beatae excepturi quaerat id deleniti quas ea dolore dolor laboriosam blanditiis, illo architecto? Eos alias temporibus commodi, eaque ratione sint maiores nisi dolore. Vel modi culpa delectus ipsam, ab alias sequi dicta cum atque, ad porro similique sed laudantium esse soluta, iusto fugiat necessitatibus ducimus ut saepe facere? Id error illo ipsa praesentium deleniti. Minus veniam temporibus delectus dolores sapiente necessitatibus, sint est repellat tempore id. Suscipit natus ipsa iusto saepe. Excepturi optio eos doloremque, voluptas amet, iste placeat vel dolore, possimus nihil incidunt totam ipsum? Maxime fuga, dolores perspiciatis temporibus nesciunt doloribus ipsum sint! Maxime non ullam temporibus rerum et voluptatem minima veniam esse modi ex. Molestias ut eveniet, ad velit officiis est perspiciatis impedit accusantium dolor. Voluptatum quas modi perspiciatis vel, magni aliquid. Molestiae saepe corporis est officiis dolorem! Repellat voluptatem mollitia non alias dolor voluptatum, tempore ut, atque impedit voluptas repellendus quos perferendis, provident ab modi reiciendis reprehenderit. Recusandae, esse debitis harum inventore alias modi. Quod nam sapiente magnam. Modi perferendis, et exercitationem repudiandae praesentium sint, quisquam enim cumque ab animi, voluptates odit consequatur incidunt vel.',
            'gambar' => 'default.jpg',
        ]);

        VisiMisi::create([
            'judul_visi' => 'Lorem ipsum dolor sit amet.',
            'judul_misi' => 'Lorem ipsum dolor.',
            'visi' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Illum, iusto.',
            'misi' => 'Lorem ipsum dolor.',
            // 'gambar' => 'default.jpg',
        ]);

        Logo::create([
            'label' => 'Lorem ipsum dolor..',
            'deskripsi' => 'Lorem ipsum dolor.',
            'gambar' => 'Lorem ipsum dolor.',
        ]);

        LokasiKontak::create([
            'telepon' => 'Lorem ipsum dolor.',
            'email' => 'Lorem ipsum dolor.',
            'alamat' => 'Lorem ipsum dolor.',
            'lokasi_map' => 'Lorem ipsum dolor.',
        ]);
    }
}
